{
    "name"          : "Execute Query",
    'version'       : '10.0.1.1.0',
    'author'        : 'IES Team',
    'company'       : 'IES',
    "category"      : "Extra Tools",
    "summary"       : "Execute query from database",
    "depends"       : [
        "base",
        "mail",
    ],
    "data"          : [
        "views/ms_query_view.xml",
        "security/ir.model.access.csv",
    ],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}