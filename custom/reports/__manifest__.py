# -*- coding: utf-8 -*-

{
    "name": "CReport",
    "version": "10.0.1.1.1",
    "category": "Accounting & Finance",
    "author": "IES ",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "account",
        "date_range",
        'report_xlsx',
        'report',
    ],
    "data": [
        "wizard/open_tax_balances_view.xml",
        "views/account_move_view.xml",
        "views/account_tax_view.xml",
        # 333
        'wizard/aged_partner_balance_wizard_view.xml',
        'wizard/general_ledger_wizard_view.xml',
        'wizard/journal_report_wizard.xml',
        'wizard/open_items_wizard_view.xml',
        'wizard/trial_balance_wizard_view.xml',
        'menuitems.xml',
        'reports.xml',
        'report/templates/aged_partner_balance.xml',
        'report/templates/general_ledger.xml',
        'report/templates/journal.xml',
        'report/templates/layouts.xml',
        'report/templates/open_items.xml',
        'report/templates/trial_balance.xml',
        'views/account_view.xml',
        'views/report_template.xml',
        'views/report_general_ledger.xml',
        'views/report_journal_ledger.xml',
        'views/report_trial_balance.xml',
        'views/report_open_items.xml',
        'views/report_aged_partner_balance.xml',
    ],
    "images": [
        'images/tax_balance.png',
    ]
}
