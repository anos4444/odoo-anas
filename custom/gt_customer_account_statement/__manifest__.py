# -*- coding: utf-8 -*-


{
    'name': 'Account Customer statement & Supplier statement & overdue statements',
    'version': '1.0',

    'category': 'Base',
    'summary': 'Account Customers statement & Supplier statement & overdue statements',
    'description': "This module will help you to Print and email Customers & Suppliers with Due Account Statements",
    'author': 'IES',
    'depends': ['base','sale','purchase','report'],
    'data': [
        'wizard/account_statements.xml',
        'report/acc_statemnt_view.xml',
        'report/report_view.xml'
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True

}

