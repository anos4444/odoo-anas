# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ManualNumberReportWizard(models.TransientModel):
    _name = "manual_number_report.wizard"
    _description = "Manual Number Report Wizard"

    range_from = fields.Integer('From')
    range_to = fields.Integer('To')
    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    type = fields.Selection(
        [('all', 'All'), ('vendor_invoice', 'Vendor Invoice'), ('purchase', 'Purchase Orders'),
         ('sale', 'Sales Orders'), ('delivery', 'Delivery Orders'), ('receipts', 'Receipts Orders'),
         ('receipt', 'Receipt'), ('pay', 'Pay'), ('customer', 'Customer Invoices'), ('vendor', 'Vendor Bills')],
        string='Type', default='all')

    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['date_from', 'date_to', 'range_from', 'range_to', 'type'])[0]
        return self.env['report'].get_action(self, 'invoice_number.manual_number_report_template', data=data)
