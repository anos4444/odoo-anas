# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class InvoiceNumbersUpdateWiz(models.TransientModel):
	_name = 'invoice.number.update.wiz'


	date_from  = fields.Date(string=' Date From')
	date_to = fields.Date(string=' Date To')
	range_from = fields.Integer('From')
	range_to = fields.Integer('To')
	sales_man_id = fields.Many2one('hr.employee', string='Sales Man', domain=[('is_sales_man', '=', True)])
	delegate_id = fields.Many2one('hr.employee', 'Delegate')


	@api.multi
	def get_data(self):
		note_lines_list = []
		line_ids = []
		domain = [('manual_invoice_number','=',False),('transmission_number','!=',False)]
		if self.sales_man_id:
			domain.append(('sales_man_id','=',self.sales_man_id.id))
		if self.date_from:
			domain.append(('invoice_date','>=',self.date_from))
		if self.date_to:
			domain.append(('invoice_date','<=',self.date_to))
		inv_lst= []
		print " DDDDDDD   ", domain
		inv_num_upd_obj= self.env['invoices.update.number']
		inv_ids = self.env['vendors.invoice'].sudo().search(domain)
		o_ids= inv_num_upd_obj.search([])
		o_ids.unlink()
		for rec in inv_ids:
			inv= inv_num_upd_obj.create({
				'vendor_invoice_id':rec.id,
				'date':rec.invoice_date,
				'state':rec.state,
				'sales_man_id':rec.sales_man_id.id,
				'vendor_invoice_id':rec.id,
				'number':rec.transmission_number,
				'invoice_number':rec.name,
				'amount':rec.net_total,
			})
			inv_lst.append(inv.id)
		action = self.env.ref('invoice_number.manual_invoice_update_number_action').read()[0]
		if inv_lst:
			action.update({'domain':[('id','in',inv_lst)]})
		else:
			action.update({'domain':[('id','=',-1)]})
		return action



