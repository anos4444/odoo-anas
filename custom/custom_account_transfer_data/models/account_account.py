# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import time

class AccountMove(models.Model):
    _inherit = 'account.move'

    transfer_done= fields.Boolean('Done')

class Account_payment(models.Model):
    _inherit = 'account.payment'

    transfer_done= fields.Boolean('Done')

class TransferOldData(models.Model):
    _name = 'transfer.old.data'

    custom_view= fields.Selection([('custom_payment_cash','Payment Cash'),
				   ('custom_payment_bank','Payment Bank'),
				   ('custom_receive_cash','Receive Cash'),
				   ('custom_receive_bank','Receive Bank'),
				   ('receipt_bond','Receipt Bond'),
				   ('pay_bond','Pay Bond')], string="Type")


    def transfer_data(self):

	move_obj= self.env['account.move']
	partner_obj= self.env['res.partner']
	payment_obj= self.env['account.payment']
	if self.custom_view == 'custom_receive_cash':
		#for pp in payment_obj.search([('payment_type', '=', 'inbound'),('multi_supplier','=',True),('journal_id.type', '=', 'cash')]):
			##pp.state= 'draft'
			#pp.unlink()
		move_ids= move_obj.search([('transfer_done','!=',True),('custom_view','=','custom_receive_cash')])
		for move in move_ids:
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			for line in move.line_ids:
				if line.credit > 0.000:
					p_ids= partner_obj.search([('property_account_receivable_id','=',line.account_id.id)])
					partner_id= p_ids and p_ids[0].id or False
					if not partner_id:
						account_id = line.account_id.id
			if partner_id:
				llst.append((0,0,{
				'amount':move.amount or 0.00,
				'partner_id':partner_id or False,
				'payment_to':'customer',
				'payment_type':'inbound',
					}))
				dct.update({
					'payment_to':'customer',
					'line_ids':llst,
					'sec_line_ids':[],
					})
			else:

				llst.append((0,0,{
				'amount':move.amount or 0.00,
				'account_id':account_id or False,
				'payment_to':'account',
				'payment_type':'inbound',
					}))
				dct.update({
					'sec_line_ids':llst,
					'payment_to':'account',
					'line_ids':[],
					})
			dct.update({
				'notee':move.ref,
				'payment_date':move.date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'inbound',
				'partner_type':'customer',
				'journal_type':'cash',
				'state':'posted',
				'payment_method_id':move.journal_id.inbound_payment_method_ids and move.journal_id.inbound_payment_method_ids[0].id or False,

				})
			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= move.name or  self.env['ir.sequence'].next_by_code('receive.cash')
				move.transfer_done= True
			for line in move.line_ids:
				line.payment_id= new_payment_id.id
		print " TTTEEESSSST   ",move_ids
	elif self.custom_view == 'custom_payment_cash':
		#for pp in payment_obj.search([('payment_type', '=', 'outbound'),('multi_supplier','=',True),('journal_id.type', '=', 'cash')]):
			##pp.state= 'draft'
			#pp.unlink()
		move_ids= move_obj.search([('transfer_done','!=',True),('custom_view','=','custom_payment_cash')])
		for move in move_ids:
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			for line in move.line_ids:
				if line.debit > 0.000:
					p_ids= partner_obj.search([('property_account_payable_id','=',line.account_id.id)])
					partner_id= p_ids and p_ids[0].id or False
					if not partner_id:
						account_id = line.account_id.id
			if partner_id:
				llst.append((0,0,{
				'amount':move.amount or 0.00,
				'partner_id':partner_id or False,
				'payment_to':'supplier',
				'payment_type':'outbound',
					}))
				dct.update({
					'payment_to':'supplier',
					'line_ids':llst,
					'sec_line_ids':[],
					})
			else:

				llst.append((0,0,{
				'amount':move.amount or 0.00,
				'account_id':account_id or False,
				'payment_to':'account',
				'payment_type':'outbound',
					}))
				dct.update({
					'sec_line_ids':llst,
					'payment_to':'account',
					'line_ids':[],
					})
			dct.update({
				'notee':move.ref,
				'payment_date':move.date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'outbound',
				'partner_type':'supplier',
				'journal_type':'cash',
				'state':'posted',
				'payment_method_id':move.journal_id.outbound_payment_method_ids and move.journal_id.outbound_payment_method_ids[0].id or False,

				})
			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= move.name or  self.env['ir.sequence'].next_by_code('payment.cash')
				move.transfer_done= True
			for line in move.line_ids:
				line.payment_id= new_payment_id.id
		print " TTTEEESSSST   ",move_ids
	elif self.custom_view == 'custom_receive_bank':
		#for pp in payment_obj.search([('payment_type', '=', 'inbound'),('multi_supplier','=',True),('journal_id.type', '=', 'cash')]):
			##pp.state= 'draft'
			#pp.unlink()
		move_ids= move_obj.search([('transfer_done','!=',True),('custom_view','=','custom_receive_bank')])
		for move in move_ids:
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			for line in move.line_ids:
				if line.credit > 0.000:
					p_ids= partner_obj.search([('property_account_receivable_id','=',line.account_id.id)])
					partner_id= p_ids and p_ids[0].id or False
					if not partner_id:
						account_id = line.account_id.id
			if partner_id:
				llst.append((0,0,{
				'amount':move.amount or 0.00,
				'partner_id':partner_id or False,
				'payment_to':'customer',
				'payment_type':'inbound',
					}))
				dct.update({
					'payment_to':'customer',
					'line_ids':llst,
					'sec_line_ids':[],
					})
			else:

				llst.append((0,0,{
				'amount':move.amount or 0.00,
				'account_id':account_id or False,
				'payment_to':'account',
				'payment_type':'inbound',
					}))
				dct.update({
					'sec_line_ids':llst,
					'payment_to':'account',
					'line_ids':[],
					})
			dct.update({
				'notee':move.ref,
				'payment_date':move.date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'inbound',
				'partner_type':'customer',
				'journal_type':'bank',
				'state':'posted',
				'payment_method_id':move.journal_id.inbound_payment_method_ids and move.journal_id.inbound_payment_method_ids[0].id or False,

				})
			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= move.name or  self.env['ir.sequence'].next_by_code('receive.bank')
				move.transfer_done= True
			for line in move.line_ids:
				line.payment_id= new_payment_id.id
		print " TTTEEESSSST   ",move_ids
	elif self.custom_view == 'custom_payment_bank':
		#for pp in payment_obj.search([('payment_type', '=', 'outbound'),('multi_supplier','=',True),('journal_id.type', '=', 'cash')]):
			##pp.state= 'draft'
			#pp.unlink()
		move_ids= move_obj.search([('transfer_done','!=',True),('custom_view','=','custom_payment_bank')])
		for move in move_ids:
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			for line in move.line_ids:
				if line.debit > 0.000:
					p_ids= partner_obj.search([('property_account_payable_id','=',line.account_id.id)])
					partner_id= p_ids and p_ids[0].id or False
					if not partner_id:
						account_id = line.account_id.id
			if partner_id:
				llst.append((0,0,{
				'amount':move.amount or 0.00,
				'partner_id':partner_id or False,
				'payment_to':'supplier',
				'payment_type':'outbound',
					}))
				dct.update({
					'payment_to':'supplier',
					'line_ids':llst,
					'sec_line_ids':[],
					})
			else:

				llst.append((0,0,{
				'amount':move.amount or 0.00,
				'account_id':account_id or False,
				'payment_to':'account',
				'payment_type':'outbound',
					}))
				dct.update({
					'sec_line_ids':llst,
					'payment_to':'account',
					'line_ids':[],
					})
			dct.update({
				'notee':move.ref,
				'payment_date':move.date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'outbound',
				'partner_type':'supplier',
				'journal_type':'bank',
				'state':'posted',
				'payment_method_id':move.journal_id.outbound_payment_method_ids and move.journal_id.outbound_payment_method_ids[0].id or False,

				})
			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= move.name or self.env['ir.sequence'].next_by_code('payment.bank')
				move.transfer_done= True
			for line in move.line_ids:
				line.payment_id= new_payment_id.id
		print " TTTEEESSSST   ",move_ids


######
	elif self.custom_view == 'receipt_bond':
		#for pp in payment_obj.search([('payment_type', '=', 'inbound'),('multi_supplier','=',True),('journal_id.type', '=', 'cash')]):
			##pp.state= 'draft'
			#pp.unlink()
		#p_ids= pay_obj.search_count([('payment_type', '=', 'inbound'),('multi_supplier','=',False),('journal_id.type','=','bank')])
		#print "  RRRRRRRRRR   ",p_ids

######################
		p_ids= payment_obj.search([('transfer_done','!=',True),('payment_type', '=', 'inbound'),('multi_supplier','=',False),('journal_id.type','=','cash')])
		print "  RRRRRRRRRR   ",p_ids
		for move in p_ids:
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			llst.append((0,0,{
			'amount':move.amount or 0.00,
			'partner_id':move.partner_id.id or False,
			'payment_to':'customer',
			'payment_type':'inbound',
			'label':move.communication or '/',
				}))
			dct.update({
				'payment_to':'customer',
				'line_ids':llst,
				'sec_line_ids':[],
				'notee':move.communication or '/',
				'payment_date':move.payment_date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'inbound',
				'partner_type':'customer',
				'journal_type':'cash',
				'state':'posted',
				'payment_method_id':move.journal_id.inbound_payment_method_ids and move.journal_id.inbound_payment_method_ids[0].id or False,
				})



			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= self.env['ir.sequence'].next_by_code('receive.cash')
				move.transfer_done= True
			for line in move.move_line_ids:
				line.payment_id= new_payment_id.id

		p_ids= payment_obj.search([('transfer_done','!=',True),('payment_type', '=', 'inbound'),('multi_supplier','=',False),('journal_id.type','=','bank')])
		print "  RRRRRRRRRR   ",p_ids
		for move in p_ids:
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			llst.append((0,0,{
			'amount':move.amount or 0.00,
			'partner_id':move.partner_id.id or False,
			'payment_to':'customer',
			'payment_type':'inbound',
			'label':move.communication or '/',
				}))
			dct.update({
				'payment_to':'customer',
				'line_ids':llst,
				'sec_line_ids':[],
				'notee':move.communication or '/',
				'payment_date':move.payment_date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'inbound',
				'partner_type':'customer',
				'journal_type':'bank',
				'state':'posted',
				'payment_method_id':move.journal_id.inbound_payment_method_ids and move.journal_id.inbound_payment_method_ids[0].id or False,
				})



			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= self.env['ir.sequence'].next_by_code('receive.bank')
				move.transfer_done= True
			for line in move.move_line_ids:
				line.payment_id= new_payment_id.id
######
	elif self.custom_view == 'pay_bond':
		p_ids= payment_obj.search([('payment_to','!=','account'),('transfer_done','!=',True),('payment_type', '=', 'outbound'),('multi_supplier','=',False),('journal_id.type','=','cash')])
		print "  RRRRRRRRRR   ",p_ids

		for move in p_ids:
			print "7777777777777   ",move.payment_to
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			p_t= move.payment_to or 'supplier'
			if p_t== 'partner':
				p_t= 'supplier'
			llst.append((0,0,{
			'amount':move.amount or 0.00,
			'partner_id':move.partner_id.id or False,
			'payment_to':p_t,
			'payment_type':'outbound',
			'label':move.communication or '/',
			'check_date':move.payment_date,
				}))
			dct.update({
				'payment_to':p_t or 'supplier',
				'line_ids':llst,
				'notee':move.communication or '/',
				'payment_date':move.payment_date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'outbound',
				'partner_type':'supplier',
				'journal_type':'cash',
				'state':'posted',
				'payment_method_id':move.journal_id.outbound_payment_method_ids and move.journal_id.outbound_payment_method_ids[0].id or False,
				'sec_line_ids':[],
				})



			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= self.env['ir.sequence'].next_by_code('payment.cash')
				move.transfer_done= True
			for line in move.move_line_ids:
				line.payment_id= new_payment_id.id
		p_ids= payment_obj.search([('payment_to','=','account'),('transfer_done','!=',True),('payment_type', '=', 'outbound'),('multi_supplier','=',False),('journal_id.type','=','cash')])
		print "  RRRRRRRRRR   ",p_ids
		for move in p_ids:
			print "8888888888888   ",move.payment_to
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			llst.append((0,0,{
			'amount':move.amount or 0.00,
			'account_id':move.payment_account_id.id or False,
			'payment_to':'account',
			'payment_type':'outbound',
			'label':move.communication or '/',
			'check_date':move.payment_date,
				}))
			dct.update({
				'payment_to':'account',
				'notee':move.communication or '/',
				'payment_date':move.payment_date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'outbound',
				'partner_type':'supplier',
				'journal_type':'cash',
				'state':'posted',
				'payment_method_id':move.journal_id.outbound_payment_method_ids and move.journal_id.outbound_payment_method_ids[0].id or False,
				'sec_line_ids':llst,
				'payment_to':'account',
				'line_ids':[],
				})



			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= self.env['ir.sequence'].next_by_code('payment.cash')
				move.transfer_done= True
			for line in move.move_line_ids:
				line.payment_id= new_payment_id.id
		p_ids= payment_obj.search([('payment_to','!=','account'),('transfer_done','!=',True),('payment_type', '=', 'outbound'),('multi_supplier','=',False),('journal_id.type','=','bank')])
		print "  RRRRRRRRRR   ",p_ids
		for move in p_ids:
			print "9999999   ",move.payment_to
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			p_t= move.payment_to or 'supplier'
			if p_t== 'partner':
				p_t= 'supplier'
			llst.append((0,0,{
			'amount':move.amount or 0.00,
			'partner_id':move.partner_id.id or False,
			'payment_to':p_t or 'supplier',
			'payment_type':'outbound',
			'label':move.communication or '/',
			'check_date':move.payment_date,
				}))
			dct.update({
				'payment_to':p_t,
				'line_ids':llst,
				'notee':move.communication or '/',
				'payment_date':move.payment_date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'outbound',
				'partner_type':'supplier',
				'journal_type':'bank',
				'state':'posted',
				'payment_method_id':move.journal_id.outbound_payment_method_ids and move.journal_id.outbound_payment_method_ids[0].id or False,
				'sec_line_ids':[],
				})



			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= self.env['ir.sequence'].next_by_code('payment.bank')
				move.transfer_done= True
			for line in move.move_line_ids:
				line.payment_id= new_payment_id.id
		p_ids= payment_obj.search([('payment_to','=','account'),('transfer_done','!=',True),('payment_type', '=', 'outbound'),('multi_supplier','=',False),('journal_id.type','=','bank')])
		print "  RRRRRRRRRR   ",p_ids
		for move in p_ids:
			print "101010010101010   ",move.payment_to
			dct= {}
			llst= []
			partner_id=False
			account_id=False
			llst.append((0,0,{
			'amount':move.amount or 0.00,
			'account_id':move.payment_account_id.id or False,
			'payment_to':'account',
			'payment_type':'outbound',
			'label':move.communication or '/',
			'check_date':move.payment_date,
				}))
			dct.update({
				'payment_to':'account',
				'notee':move.communication or '/',
				'payment_date':move.payment_date,
				'journal_id':move.journal_id.id,
				'amount':move.amount or 0.00,
				'multi_supplier':True,
				'payment_type':'outbound',
				'partner_type':'supplier',
				'journal_type':'bank',
				'state':'posted',
				'payment_method_id':move.journal_id.outbound_payment_method_ids and move.journal_id.outbound_payment_method_ids[0].id or False,
				'sec_line_ids':llst,
				'payment_to':'account',
				'line_ids':[],
				})



			new_payment_id= payment_obj.create(dct)
			if new_payment_id:
				new_payment_id.name= self.env['ir.sequence'].next_by_code('payment.bank')
				move.transfer_done= True
			for line in move.move_line_ids:
				line.payment_id= new_payment_id.id
	return True





