from odoo import SUPERUSER_ID
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class partner_moves_reportWizard(models.TransientModel):
    _inherit = 'partner_moves_report.wizard'

    @api.multi 
    def mass_invoice_email_send222(self):
        print '*********************** ',self

class wiz_mass_invoice_vendor(models.TransientModel):
    _name = 'wiz.mass.move.partner'
    header_footer_id = fields.Many2one('header.footer.text', string='Template name', required=True)
    text1 = fields.Text('Begin Text',translate=True)
    text2 = fields.Text('End Text',translate=True)

    partner_ids = fields.Many2many('res.partner', string='Partner', required=False)
    partner_tags = fields.Many2many('res.partner.category', string='Partners Tags', required=False)
    date_from = fields.Date(string='Start Date',default=datetime.today(),required=True)
    date_to = fields.Date(string='End Date',default=datetime.today(),required=True)


    @api.onchange('header_footer_id')
    def set_front_end(self):
        res = []
        if self.header_footer_id:
            
            self.text1=self.header_footer_id.text1
            self.text2=self.header_footer_id.text2


    @api.multi 
    def mass_invoice_email_send(self):
        docs = self
        print "QQQQQQQQQQQQQQQQQ   ",self.env.context.get('active_id')
        ss = self.env['wiz.mass.move.partner'].browse(self.env.context.get('active_id'))
        print "WWWWWWWWWWWWW ", ss
        def Remove(duplicate): 
            final_list = [] 
            for num in duplicate: 
                if num not in final_list: 
                    final_list.append(num) 
            return final_list 

        partners_tag_obj = self.env['res.partner']
        getObj= self.env['res.partner']

        partners_ids = []
        if docs.partner_ids:
            partners_ids += docs.partner_ids
        if docs.partner_tags:
            for pa in docs.partner_tags:
                partners_ids += partners_tag_obj.search([('category_id', '=', pa.id)])
        if not partners_ids:
            raise UserError("Please Select Partners or tags")

        partners_ids = (Remove(partners_ids))
        
        for i in partners_ids:
            partner_email = i.email
            if not partner_email:
                raise UserError(_('partner %s has no email id please enter email address')
                        % (i.name)) 
            
            email_receipts=""+i.email
            
            getAllIds = getObj.search([('parent_id','=',i.id)])
            for purchase_line_id in getAllIds :
                wanted_vendor_id=getObj.browse([purchase_line_id])
                email_receipts=email_receipts+" , "+wanted_vendor_id.id.email
            
            print "###################################@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
            
            #account_invoice_brw.send_by_email = True
            template_id = self.env['ir.model.data'].get_object_reference(
                                                                'send_by_email', 
                                                                'partner_move_email_template')[1]
            print '11111111111111111111111111111111111111111111111111'
            email_template_obj = self.env['mail.template'].browse(template_id)
            if template_id:
                print '22222222222222222222222222222222222222',email_template_obj
                #idid = self.env['vendors.invoice'].browse(846)
                values = email_template_obj.generate_email(ss.id , fields=None)
                print "///////////////////////////////*"
                values['email_to'] = email_receipts
                values['res_id'] = ss.id
                ir_attachment_obj = self.env['ir.attachment']
                get_body=""+values['body']
                get_html=""+values['body_html']
                token_list1=get_body.split('@',2)
                token_list2=get_html.split('@',2)
                add_text=''+self.text1
                print "<MMMMMMMMMMMMMMMMMMMMMMMMM"
                new_text_body=token_list1[0]+add_text+"<br />"+token_list1[1]+"<br />"+self.text2+token_list1[2]
                new_text_html=token_list2[0]+add_text+"<br />"+token_list2[1]+"<br />"+self.text2+token_list2[2]
                values['body']=new_text_body
                values['body_html']=new_text_html
                vals = {
                        'name' : 'nnn',
                        'type' : 'binary',
                        'datas' : values['attachments'][0][1],
                        'datas_fname' : values['attachments'][0][0],
                        'res_id' : i,
                        'res_model': 'partner_moves_report.wizard',
                }
                attachment_id = ir_attachment_obj.create(vals)
                print "::::::33333333333333333333333333333333", attachment_id
                mail_mail_obj = self.env['mail.mail']
                msg_id = mail_mail_obj.create(values)
                msg_id.attachment_ids=[(6,0,[attachment_id.id])]
                xxx=[(6,0,[attachment_id.id])]
                if msg_id:
                    print'msg_id..............................',msg_id
                    msg_id.send([msg_id])

                               
        return True