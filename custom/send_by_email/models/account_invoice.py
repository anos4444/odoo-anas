# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) BrowseInfo (http://browseinfo.in)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import SUPERUSER_ID
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class customer_invoice_custom(models.Model):
    
    _inherit = 'account.invoice'

    send_by_email = fields.Boolean(string='Sent By Email', store=True, default=False)
    send_date = fields.Date(string='Sent Date', store=True)

class header_footer_text(models.Model):
    
    _name = 'header.footer.text'
    
    name = fields.Char('name of template',required=True,translate=True)
    text1 = fields.Text('Begin Text',translate=True)
    text2 = fields.Text('End Text',translate=True)

class wiz_mass_invoice(models.TransientModel):
    _name = 'wiz.mass.invoice'
    header_footer_id = fields.Many2one('header.footer.text', string='Template name', required=True)
    text1 = fields.Text('Begin Text',translate=True)
    text2 = fields.Text('End Text',translate=True)

    @api.onchange('header_footer_id')
    def set_front_end(self):
        res = []
        if self.header_footer_id:
            self.text1=self.header_footer_id.text1
            self.text2=self.header_footer_id.text2

        #return res


    @api.multi 
    def mass_invoice_email_send(self):
        print '...................... end value',self.text2
        context = self._context
        active_ids = context.get('active_ids')
        super_user = self.env['res.users'].browse(SUPERUSER_ID)
        for a_id in active_ids:
            account_invoice_brw = self.env['account.invoice'].browse(a_id)
            for partner in account_invoice_brw.partner_id:
                ################## v8 hoz ##############
                partner_email = partner.email
                self.env.cr.execute("UPDATE vendors_invoice SET send_by_email = 't', send_date = '%s'  WHERE id = %s;" %(fields.Date.today(),a_id))
                if not partner_email:
                    raise UserError(_('%s customer has no email id please enter email address')
                            % (account_invoice_brw.partner_id.name))

                email_receipts=""+partner.email
                list_receipts=[]
                getObj= self.env['res.partner']
                getAllIds = getObj.search([('parent_id','=',partner.id)])
                print '************************',getAllIds
                for purchase_line_id in getAllIds :
                    wanted_partner_id=getObj.browse([purchase_line_id])
                    email_receipts=email_receipts+" , "+wanted_partner_id.id.email
                ################## v8 hoz ##############

                if not partner_email:
                    raise UserError(_('%s customer has no email id please enter email address')
                            % (account_invoice_brw.partner_id.name)) 
                else:
                    template_id = self.env['ir.model.data'].get_object_reference(
                                                                      'send_by_email', 
                                                                      'custom_email_template_edi_invoice')[1]

                    #account_invoice_brw.email_status  = 'sent'
                    self.env.cr.execute("UPDATE account_invoice SET send_by_email = 't', send_date = '%s'  WHERE id = %s;" %(fields.Date.today(),a_id))
                    email_template_obj = self.env['mail.template'].browse(template_id)
                    if template_id:
                        values = email_template_obj.generate_email(a_id, fields=None)
                        values['email_to'] = email_receipts # 'huzifaabdo@yahoo.com , hozifaabdo333@hotmail.com'#partner.email
                        values['res_id'] = a_id
                        ir_attachment_obj = self.env['ir.attachment']
                        get_body=""+values['body']
                        get_html=""+values['body_html']
                        token_list1=get_body.split('@',2)
                        token_list2=get_html.split('@',2)
                        add_text=''+self.text1
                        new_text_body=token_list1[0]+add_text+"<br />"+token_list1[1]+"<br />"+self.text2+token_list1[2]
                        new_text_html=token_list2[0]+add_text+"<br />"+token_list2[1]+"<br />"+self.text2+token_list2[2]
                        values['body']=new_text_body
                        values['body_html']=new_text_html
                        vals = {
                                'name' : account_invoice_brw.number,
                                'type' : 'binary',
                                'datas' : values['attachments'][0][1],
                                'datas_fname' : values['attachments'][0][0],
                                'res_id' : a_id,
                                'res_model' : 'account.invoice',
                        }
                        attachment_id = ir_attachment_obj.create(vals)
                        mail_mail_obj = self.env['mail.mail']
                        msg_id = mail_mail_obj.create(values)
                        msg_id.attachment_ids=[(6,0,[attachment_id.id])]
                        xxx=[(6,0,[attachment_id.id])]
                        if msg_id:

                            msg_id.send([msg_id])  

                                                     
        return True
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:






