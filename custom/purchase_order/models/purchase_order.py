# -*- coding: utf-8 -*-

from odoo import fields, api, models
from odoo import _
from odoo.exceptions import ValidationError
import dateutil.parser


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    @api.model
    def _get_default_location(self):
        if self.env.user.user_default_warehouse:
            return self.env['stock.location'].search(
                [('location_id', '=', self.env.user.user_default_warehouse.view_location_id.id)], limit=1).id
        else:
            return False

    @api.one
    def fetch_products(self):
        current_products = []
        current_products = [x.product_id.id for x in self.order_line]
        vals = []
        if self.warehouse_id:
            if self.partner_id:
                if self.partner_id.product_location_ids:
                    conf_warehouses = filter(lambda x: x.location_id.id == self.warehouse_id.id,
                                             self.partner_id.product_location_ids) or []
                    for wh in conf_warehouses:
                        if wh.products_ids:
                            wh_products_ids = sorted(wh.products_ids, key=lambda l: l.sequence)
                            for line in wh_products_ids:
                                if line.product_id.id not in current_products:
                                    vals.append((0, False, {'product_id': line.product_id.id,
                                                            'product_uom': line.product_id.uom_id.id,
                                                            'price_unit': 0.0,
                                                            'product_qty': 0.0,
                                                            'name': line.product_id.name,
                                                            'date_planned': self.date_planned}))
        if vals:
            ctx = self.env.context.copy()
            ctx.update({
                'no_delete_line': True
            })
            self.env.context = ctx
            self.order_line = vals

    @api.model
    def create(self, vals):
        res = super(PurchaseOrder, self).create(vals)
        ctx = self.env.context.copy()
        if 'no_delete_line' not in ctx:
            for line in res.order_line:
                if line.new_quantity == 0:
                    line.unlink()
        return res

    @api.multi
    def write(self, vals):
        res = super(PurchaseOrder, self).write(vals)
        ctx = self.env.context.copy()
        if 'no_delete_line' not in ctx:
            for rec in self:
                for line in rec.order_line:
                    if line.new_quantity == 0:
                        line.unlink()
        return res

    @api.constrains('order_line')
    def _check_quantity_and_price(self):
        ctx = self.env.context.copy()
        if 'no_delete_line' not in ctx:
            for line in self.order_line:
                if line.new_price <= 0 or line.new_quantity <= 0:
                    raise ValidationError(_('Quantity and price must be great than zero.'))

    @api.multi
    @api.depends('date_order')
    def _get_computed_date_order(self):
        for rec in self:
            if rec.date_order:
                rec.computed_date_order = dateutil.parser.parse(rec.date_order).date()

    computed_date_order = fields.Date(string='Computed Order Date', compute="_get_computed_date_order",
                                      store=True)

    # todo:invisible when several locations in setting
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse')
    location_id = fields.Many2one('stock.location', string='Location', default=_get_default_location)

    manual_inv_num = fields.Char('Manual Invoice Number')

    @api.constrains('manual_inv_num')
    def _check_manual_inv_num(self):

        invoice_numbers = self.env['manual.invoice.number'].sudo().search([('type', '=', 'purchase')])

        if self.manual_inv_num and len(invoice_numbers) > 0:

            if len(self.env['purchase.order'].sudo().search(
                    [('manual_inv_num', '=', self.manual_inv_num), ('state', '!=', 'cancel')])) > 1:
                raise ValidationError(_('This number is reserved in another purchase order..!'))

            invoice_number_found = False

            for invoice in invoice_numbers:
                if invoice.range_from <= int(self.manual_inv_num) <= invoice.range_to:
                    invoice_number_found = True
                    break
            if not invoice_number_found:
                raise ValidationError(
                    _('Invalid invoice number, this number not found in purchase orders numbers range.'))

    @api.onchange('manual_inv_num')
    def _onchange_manual_inv_num(self):
        if self.manual_inv_num:
            for rec in self.order_line:
                rec.manual_inv_num = self.manual_inv_num

    @api.multi
    @api.onchange('warehouse_id')
    def filter_warehouse_locations(self):
        res = dict()
        location_ids = self.env['stock.location'].search(
            [('location_id', '=', self.warehouse_id.view_location_id.id)]).ids

        res['domain'] = {'location_id': [('id', 'in', location_ids)]}
        return res

    @api.multi
    def button_confirm(self):
        ctx = self.env.context.copy()
        ctx.update({
            'cr_location_id': self.location_id,
            'warehouse_id': self.warehouse_id
        })

        self.env.context = ctx
        super(PurchaseOrder, self).button_confirm()
        self.picking_ids.do_transfer()
        self.picking_ids.sudo().action_done()


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    @api.depends('invoice_lines.invoice_id.state')
    def _compute_new_qty_invoiced(self):
        for line in self:
            qty = 0.0
            for inv_line in line.invoice_lines:
                if inv_line.invoice_id.state not in ['cancel']:
                    if inv_line.invoice_id.type == 'in_invoice':
                        qty += inv_line.new_quantity
                    elif inv_line.invoice_id.type == 'in_refund':
                        qty -= inv_line.new_quantity
            line.new_qty_invoiced = qty

    new_quantity = fields.Float('Quantity')
    new_qty_invoiced = fields.Float(string='Qty Inv', copy=False, compute=_compute_new_qty_invoiced)
    new_price = fields.Float('Price')
    new_price_total = fields.Float('Total', compute='_compute_total_price')
    tax_amount = fields.Float('Tax Amount', compute='_compute_tax_amount')
    new_weight = fields.Float('Weight')
    free_quantity = fields.Float('Free')
    manual_inv_num = fields.Char('Manual Number')

    @api.depends('new_price', 'new_quantity')
    def _compute_total_price(self):
        for rec in self:
            rec.new_price_total = rec.new_quantity * rec.new_price + \
                                  (rec.new_quantity * rec.new_price) * rec.taxes_id.amount / 100

    @api.depends('taxes_id', 'product_qty', 'price_unit')
    def _compute_tax_amount(self):
        for rec in self:
            if rec.taxes_id:
                rec.tax_amount = (rec.product_qty * rec.price_unit) * rec.taxes_id.amount / 100

    @api.multi
    @api.onchange('new_quantity', 'new_price', 'new_weight', 'free_quantity')
    def _onchange_new_quantity(self):
        product_quantity = (self.new_quantity + self.free_quantity) * self.new_weight

        if product_quantity > 0:
            price_unit = float((self.new_quantity * self.new_price) / product_quantity)
        else:
            price_unit = 0
        self.product_qty = product_quantity
        self.price_unit = price_unit


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.model
    def create(self, vals):
        if vals.get('purchase_line_id'):
            purchase_line_id = self.env['purchase.order.line'].sudo().browse(vals.get('purchase_line_id'))
            vals['new_quantity'] = purchase_line_id.new_quantity
            vals['new_price'] = purchase_line_id.new_price
            vals['new_weight'] = purchase_line_id.new_weight
            vals['free_quantity'] = purchase_line_id.free_quantity
        return super(StockMove, self).create(vals)