# -*- coding: utf-8 -*-

{
    'name': 'VE.18.05 Custom Purchase Order',
    'version': '10.0.1.0.0',
    'sequence': 3,
    'summary': '',
    'depends': ['purchase', 'stock', 'product', 'account_accountant', 'custom_account', 'custom_inventory'],
    'data': [
        'wizard/generate_vendor_invoice.xml',
        'wizard/purchase_order_confirm.xml',
        'views/purchase_order_line_view.xml',
        'views/purchase_order_view.xml',
    ],
    
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': True,
}
