# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "VE.18.05 Product Pckaging",
    'version': '1.0',
    'summary': '',
    'description': '',
    'category': 'Custom',
    'website': '',
    'depends': ['base', 'product', 'stock','sale_stock','sale_order'],

    'data': [
        'views/product_packaging_view.xml',
	'views/product_template_view.xml',
	'views/partner_view.xml',
	'views/sale_order_view.xml',
	'views/purchase_order_view.xml',
	'views/general_price_list_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
