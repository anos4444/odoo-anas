# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.osv import expression
from odoo.exceptions import ValidationError, UserError, Warning


class PartnerGeneralPricelist(models.Model):
    _name = 'partner.general.pricelist'
    _order = 'date desc'

    partner_ids = fields.Many2many('res.partner','general_partners_rel', 'partner_id', 'general_id', string="Partners")
    active = fields.Boolean(string='Active')
    date = fields.Date(string='Date', default=fields.Date.context_today)
    name= fields.Char(string='Name', required=True)

