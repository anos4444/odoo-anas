# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.osv import expression
from odoo.exceptions import ValidationError, UserError, Warning


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    packaging_ids = fields.One2many('product.packaging.line', 'product_id', string="Packaging")


    @api.constrains('packaging_ids','packaging_ids.min_price','packaging_ids.max_price')
    def _check_min_max_price(self):
	for rec in self:
		for line in rec.packaging_ids:
			if line.max_price < line.min_price:
                		raise ValidationError(_('Error, The Maximum Price Must be Greater or Equal than Minimum Price.'))
