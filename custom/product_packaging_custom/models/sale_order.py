# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
import dateutil.parser
from odoo.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from odoo.addons.check_followups.models.money_to_text_en import amount_to_text
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare, float_round
from dateutil.relativedelta import relativedelta


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.one
    def fetch_products(self):
	p_obj= self.env['product.packaging.line']
        current_products = [x.product_id.id for x in self.order_line]
	product_obj = self.env['product.product']
        vals = []
        if self.warehouse_id:
            if self.partner_id:
                if self.partner_id.prices_warehouse_ids:
                    conf_warehouses = filter(lambda x: x.warehouse_id.id == self.warehouse_id.id,
                                             self.partner_id.prices_warehouse_ids) or []
                    for wh in conf_warehouses:
                        if wh.priceslist_ids:
                            #wh_products_ids = sorted(wh.priceslist_ids, key=lambda l: l.sequence)
			    wh_products_ids= sorted(wh.priceslist_ids, key=lambda l: l.sequence)
                            for line in wh_products_ids:
			        product_idss= product_obj.search([('product_tmpl_id','=',line.product_id.id)])
				product_id= product_idss and product_idss[0].id or False
                                if product_id not in current_products:
				    min_new_price= 0.00
				    price_ids = p_obj.search([('product_id','=',product_idss and product_idss[0].product_tmpl_id.id or False),('packaging_type_id','=',line.packaging_type_id.id)])
				    for p in price_ids:
				    	if ((self.warehouse_id.id in (p.warehouses_ids and p.warehouses_ids.ids or [])) or (p.all_warehouses)):
				    		min_new_price =  p.min_price or 0.00
                                    vals.append((0, False, {'product_id': product_id,'packaging_type_id': line.packaging_type_id.id,'price_unit':  line.price or 0.00,'new_price': line.price or 0.00,'min_new_price':min_new_price}))
        if vals:
            ctx = self.env.context.copy()
            ctx.update({
                'no_delete_line': True
            })
            self.env.context = ctx
            self.order_line = vals


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    packaging_type_id = fields.Many2one('product.packaging.type', string='Packaging Type')
    package_ids= fields.Many2many('product.packaging.type', 'package_sale_lines_rel', 'package_id', 'oline_id', compute='get_packages', string='Packages')
    min_new_price = fields.Float('Min Price')

    @api.multi
    @api.onchange('product_id')
    def onchange_product_id(self):
	for rec in self:
		rec.packaging_type_id = False

    @api.multi
    @api.depends('product_id', 'order_id.partner_id','order_id.warehouse_id')
    def get_packages(self):
	price_obj = self.env['customer.packaging.pricelist.line']
	for rec in self:
		p_ids= []
		if rec.product_id and rec.order_id.partner_id and rec.order_id.warehouse_id:
			price_ids = price_obj.search([('product_id','=',rec.product_id.product_tmpl_id.id),('warehouse_id','=',rec.order_id.warehouse_id.id),('customer_warehouse_id.partner_id','=',rec.order_id.partner_id.id)])
			for line in price_ids:
				if line.packaging_type_id.id not in p_ids:
					p_ids.append(line.packaging_type_id.id)
					
			rec.package_ids = [(6,0,p_ids)]

		else:
			rec.package_ids = [(6,0,[])]

    @api.multi
    @api.onchange('product_id', 'order_id.partner_id','order_id.warehouse_id','packaging_type_id')
    def onchange_packages(self):
	price_obj = self.env['customer.packaging.pricelist.line']
        print ":::::::::::::::           11111111111"
	for rec in self:
		if rec.product_id and rec.order_id.partner_id and rec.order_id.warehouse_id and rec.packaging_type_id:
			price_ids = price_obj.search([('product_id','=',rec.product_id.product_tmpl_id.id),('warehouse_id','=',rec.order_id.warehouse_id.id),('customer_warehouse_id.partner_id','=',rec.order_id.partner_id.id),('packaging_type_id','=',rec.packaging_type_id.id)], limit=1)
			rec.new_price= price_ids and price_ids[0].price or 0.00
        		print ":::::::::::::::           22222222222", price_ids and price_ids[0].price or 0.00
			if not price_ids:
				rec.new_price = 0.00
		else:
			rec.new_price= 0.00


    @api.multi
    @api.onchange('product_id','order_id.warehouse_id','packaging_type_id')
    def onchange_get_price(self):
	p_obj= self.env['product.packaging.line']
	for rec in self:
		price_ids = p_obj.search([('product_id','=',rec.product_id.product_tmpl_id.id),('packaging_type_id','=',rec.packaging_type_id.id)])
		for p in price_ids:
			if ((rec.order_id.warehouse_id.id in (p.warehouses_ids and p.warehouses_ids.ids or [])) or (p.all_warehouses)):
				rec.min_new_price =  p.min_price or 0.00
			else:
				rec.min_new_price =  0.00
    @api.multi
    @api.constrains('product_id','order_id.warehouse_id','packaging_type_id','new_price')
    def _constrains_check_product_price(self):
	p_obj= self.env['product.packaging.line']
	for rec in self:
		price_ids = p_obj.search([('product_id','=',rec.product_id.product_tmpl_id.id),('packaging_type_id','=',rec.packaging_type_id.id)])
		for p in price_ids:
			if ((rec.order_id.warehouse_id.id in (p.warehouses_ids and p.warehouses_ids.ids or [])) or (p.all_warehouses)) and rec.new_price < p.min_price:
				raise ValidationError(_('Error, The Price must be greater or equal than minimum price.'))








