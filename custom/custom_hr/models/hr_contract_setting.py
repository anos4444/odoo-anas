import time
import datetime
from dateutil.relativedelta import relativedelta

import odoo
from odoo import SUPERUSER_ID
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class HRContractSettings(models.Model):
    _name = 'hr.contract.settings'

    employee_id = fields.Many2many('hr.employee', string="Employee")
    name = fields.Char(string='Name')
    code = fields.Char(string='Code')

    hr_contract_lines = fields.One2many('hr.contract.line', 'hr_contract_settings_id', string="Fields Names", index=True)

    contract_text = fields.Html('Contract')

    
class HRContractLine(models.Model):
    _name = "hr.contract.line"

    
    name = fields.Char(string="Name")
    
    fields_name = fields.Selection([
        ('contract_no', 'Contract No.'),
        ('emp_name', 'Employee Name'),
        ('emp_code', 'Employee Code'),
        ('emp_mobile', 'Employee Phone'),
        ('emp_department', 'Employee Department'),
        ('emp_job', 'Employee Job'),
        ('emp_email', 'Employee Email'),
        ('emp_id_no', 'Employee Identification No.'),
        ('emp_passport_no', 'Employee Passport No'),
        ('emp_nationality', 'Employee Nationality'),
        ('emp_gender', 'Employee Gender'),
        ('emp_marital_status', 'Employee Marital Status'),
        ('emp_date_of_birth', 'Employee Date of Birth'),
        ('emp_place_of_birth', 'Employee Place of Birth'),
        ('contract_type', 'Contract Type'),
        ('basic_sallary', 'Basic Sallary'),
        ('contract_start_date', 'Contract Start Date'),
        ('contract_end_date', 'Contract End Date'),
        ('scheduled_pay', 'Scheduled Pay'),
        ('housing_allowance', 'Housing Allowance'),
        ('cost_of_living_allowance', 'Cost Of Living Allowance'),
        ('telephone_allowance', 'Telephone Allowance'),
        ('transfer_allowance', 'Transfer Allowance'),
        ('nature_of_work_allowance', 'Nature Of Work Allowance'),
        ('supervision_allowance', 'Supervision Allowance'),
        ('externally_paid_allowance', 'Externally paid Allowance'),
        ('other_allowance', 'Other Allowance'),
    ], string="System name", track_visibility='onchange', copy=False)
    note = fields.Char(string="Note")

    hr_contract_settings_id = fields.Many2one('hr.contract.settings', string="con Ref.")
