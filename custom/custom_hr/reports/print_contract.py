# -*- coding: utf-8 -*-

import time
from odoo import api, models, _
from dateutil.parser import parse
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from num2words import num2words
from operator import itemgetter
from odoo.tools import ustr
import time
from datetime import datetime, date, time, timedelta
import dateutil.parser
import math


class PrintContract(models.AbstractModel):
    _name = 'report.custom_hr.print_contract'

    
    def _get_field_value(self, field, emp_id):
        value = ''

        contract_obj = self.env['hr.contract'].search([('employee_id', '=', emp_id)])
        emp_obj = self.env['hr.employee'].search([('id', '=', emp_id)])

        if field == 'contract_no':
            value = contract_obj.name or ""
        elif field == 'emp_name':
                value = emp_obj.name or ""
        elif field == 'emp_code':
                value = emp_obj.employee_id or ""
        elif field == 'emp_mobile':
                value = emp_obj.mobile_phone or ""
        elif field == 'emp_department':
                value = emp_obj.department_id.name or ""
        elif field == 'emp_job':
                value = emp_obj.job_id.name or ""
        elif field == 'emp_email':
                value = emp_obj.work_email or ""
        elif field == 'emp_id_no':
                value = emp_obj.identification_id or ""
        elif field == 'emp_passport_no':
                value = emp_obj.passport_id or ""
        elif field == 'emp_nationality':
                value = emp_obj.country_id.name or ""
        elif field == 'emp_gender':
                value = dict(emp_obj._fields['gender'].selection).get(emp_obj.gender) or ""
        elif field == 'emp_marital_status':
                value = dict(emp_obj._fields['marital'].selection).get(emp_obj.marital) or ""
        elif field == 'emp_date_of_birth':
                value = emp_obj.birthday or ""
        elif field == 'contract_type':
                value = contract_obj.type_id.name or ""
        elif field == 'basic_sallary':
                value = str(contract_obj.wage)
        elif field == 'contract_start_date':
                value = str(contract_obj.date_start)
        elif field == 'contract_end_date':
                value = str(contract_obj.date_end)
        elif field == 'scheduled_pay':
                value = dict(contract_obj._fields['schedule_pay'].selection).get(contract_obj.schedule_pay) or "" 
        elif field == 'housing_allowance':
                value = str(contract_obj.housing_allowance)
        elif field == 'cost_of_living_allowance':
                value = str(contract_obj.cost_of_living_allowance)
        elif field == 'telephone_allowance':
                value = str(contract_obj.telephone_allowance)
        elif field == 'transfer_allowance':
                value = str(contract_obj.Transfer_allowance)
        elif field == 'nature_of_work_allowance':
                value = str(contract_obj.nature_of_work_allowance)
        elif field == 'supervision_allowance':
                value = contract_obj.supervision_allowance
        elif field == 'externally_paid_allowance':
                value = str(contract_obj.externally_paid_allowance)
        elif field == 'other_allowance':
                value = str(contract_obj.other_allowance)
       



        return value

    @api.model
    def render_html(self, docids, data=None):
        
        docs = self.env['hr.contract'].browse(docids[0])

        
        hr_contracts_obj = self.env['hr.contract.settings']
        contracts = []
        for rec in docs:
            txt_of_contract = ''
            contract = hr_contracts_obj.search([('code', '=', rec.hr_contract_code.code)])
            
            txt_of_contract = contract.contract_text
            for line in contract.hr_contract_lines:
                if line.name in txt_of_contract:
                    txt_of_contract = txt_of_contract.replace(line.name, self._get_field_value(line.fields_name,rec.employee_id.id))
        
            contracts.append({"contract": txt_of_contract})
            


        docargs = {
            'docs': contracts,
        }


        return self.env['report'].render('custom_hr.print_contract', docargs)
