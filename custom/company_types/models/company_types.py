from odoo import api, fields, models

class company_types(models.Model):

    _name = "company.types"

    name = fields.Char(string='company Type', required=True)
