# -*- coding: utf-8 -*-

{
    'name' : "Company Types",
    'version' : '1.0',
    'description':
"""
Defines the types of market in every company
============================================

Fiexed types that are written in the python file

""",
    'category': 'Custom',
    'website': '',
    'data': [
        'data/data_company_type.xml'
        ],
    'installable': True,
    'auto_install': False,
}

