# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError


class detailedSalesmanReport(models.AbstractModel):
    _name = 'report.sales_report.detailed_salesman_report'

    @api.model
    def render_html(self, docids, data=None):
        print "LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"
        final_data =[]
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        sales_records = []
        if (not docs.date_from) or (not docs.date_to):
            raise UserError("Please enter duration")
        #orders = self.env['vendors.invoice'].search(
        #    [('sales_man_id', '=', docs.salesperson_id.id)])
	if docs.vendor_id:
		orders = self.env['vendors.invoice'].search(
		    [('vendor_id', '=', docs.vendor_id.id),('sales_man_id', '=', docs.salesperson_id.id),('invoice_date','>=',docs.date_from),('invoice_date','<=',docs.date_to)])
	else:
		orders = self.env['vendors.invoice'].search(
		    [('sales_man_id', '=', docs.salesperson_id.id),('invoice_date','>=',docs.date_from),('invoice_date','<=',docs.date_to)])
        # if vendor_id.id:
        #     print "***********************************************************************"
        #     orders = self.env['vendors.invoice'].search(
        #         [('sales_man_id', '=', docs.salesperson_id.id), ('vendor_id', '=', docs.vendor_id.id),
        #          ('invoice_date', '>=', docs.date_from), ('invoice_date', '<=', docs.date_to)])
        # else:
        #     print "---------------------------------------------------------------------------"
        #     orders = self.env['vendors.invoice'].search(
        #         [('sales_man_id', '=', docs.salesperson_id.id),
        #          ('invoice_date', '>=', docs.date_from), ('invoice_date', '<=', docs.date_to)])
	total_tax1 = 0.00
        for order in orders:
            number_of_cards=tax=0
            customer_inv=self.env['account.invoice.line'].search(
            [('vendor_invoice_id.name','=',order.name),('partner_id','=',docs.salesperson_id.expense_partner.id)])
            for inv in customer_inv:
                tax+=inv.tax_amount
	        #print inv.partner_id.name,"    --         ", inv.vendor_invoice_id.name,"  PPP            OOO        ",inv.tax_amount
	    total_tax1 += (tax or 0.00)

            for custody in order.custody_lines:
                if custody.is_complex and custody.is_card :
                    number_of_cards =int((custody.amount)/ 51.0)                            
                    
       


            final_data.append({'vendor':order.vendor_id.name,
                               'name': order.name,
                               'transmission_number':order.transmission_number,
                               'main_customer_total_untaxed':round(order.main_customer_total_untaxed, 2),
                               'credit_sales':round(order.credit_sales, 2),
                               'cash_custody':round(order.cash_custody, 2),
                               'commision_total_amount':round(order.commision_total_amount, 2),
                               'cash_custody':round(order.cash_custody, 2),
                               'credit_custody':round(order.credit_custody, 2),
                               'amount_total':round(order.amount_total, 2),
                               'number_of_cards':round(number_of_cards, 2),
                               'tax':round(tax, 2)
                               
                               })


        
        total = {'amount_total': 0.0, 'total_taxed_amount': 0.0, 'credit_custody': 0.0, 'cash_custody': 0.0,
                 'commision_total_amount': 0.0, 'credit_sales': 0.0, 'main_customer_total_untaxed': 0.0, }
        number_of_cards = {}

        if orders.ids:
            self._cr.execute("select sum(round(cast(vendors_invoice.amount_total as numeric), 2)) as amount_total," \
                            "sum(round(cast(vendors_invoice.main_customer_total_amount-vendors_invoice.main_customer_total_untaxed as numeric), 2)) as total_taxed_amount," \
                            "sum(round(cast(vendors_invoice.credit_custody as numeric), 2)) as credit_custody, " \
                            "sum(round(cast(vendors_invoice.cash_custody as numeric), 2)) as cash_custody," \
                            "sum(round(cast(vendors_invoice.commision_total_amount as numeric), 2)) as commision_total_amount, " \
                            "sum(round(cast(vendors_invoice.credit_sales as numeric), 2)) as credit_sales," \
                            "sum(round(cast(vendors_invoice.main_customer_total_untaxed as numeric), 2)) as main_customer_total_untaxed " \
                            "from vendors_invoice "\
                            "left join vendors_custody_line on (vendors_custody_line.vendor_invoice_id = vendors_invoice.id and vendors_custody_line.is_complex is True) "\
                            "where  vendors_invoice.id in %s ", (tuple(orders.ids), ))
            res = self._cr.dictfetchall()
            total = {'amount_total': res[0]['amount_total'] is None and 0.0 or res[0]['amount_total'], 
                     #'total_taxed_amount': res[0]['total_taxed_amount'] is None and 0.0 or res[0]['total_taxed_amount'], 
                     'total_taxed_amount': total_tax1, 
                     'credit_custody': res[0]['credit_custody'] is None and 0.0 or res[0]['credit_custody'], 
                     'cash_custody': res[0]['cash_custody'] is None and 0.0 or res[0]['cash_custody'],
                     'commision_total_amount': res[0]['commision_total_amount'] is None and 0.0 or res[0]['commision_total_amount'], 
                     'credit_sales': res[0]['credit_sales'] is None and 0.0 or res[0]['credit_sales'], 
                     'main_customer_total_untaxed': res[0]['main_customer_total_untaxed'] is None and 0.0 or res[0]['main_customer_total_untaxed'], }
            
            # self._cr.execute("select vendors_custody_line.vendor_invoice_id, (sum(vendors_custody_line.amount)  /51.0) " \
            #                  "from vendors_custody_line "\
            #                  "where vendors_custody_line.is_complex is True and vendors_custody_line.vendor_invoice_id in %s "\
            #                  "group by vendors_custody_line.vendor_invoice_id ", (tuple(orders.ids), ))
            # res2 = self._cr.fetchall()
            # print res2
            # number_of_cards = dict(res2)
            #print orders,"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO"

        '''if docs.date_from and docs.date_to:
            for order in orders:
                if parse(docs.date_from) <= parse(order.invoice_date) \
                        and parse(docs.date_to) >= parse(order.invoice_date):
                    sales_records.append(order)
                    total['amount_total'] += abs(
                        round(order.amount_total, 2))
                    total['total_taxed_amount'] += abs(
                        round(order.main_customer_total_amount-order.main_customer_total_untaxed, 2))
                    total['credit_custody'] += abs(
                        round(order.credit_custody, 2))
                    total['cash_custody'] += abs(
                        round(order.cash_custody, 2))
                    total['commision_total_amount'] += abs(
                        round(order.commision_total_amount, 2))
                    total['credit_sales'] += abs(
                        round(order.credit_sales, 2))
                    total['main_customer_total_untaxed'] += abs(
                        round(order.main_customer_total_untaxed, 2))
                    number_of_cards[order.id] = 0.0
                    for custody in order.custody_lines:
                        if custody.is_complex:
                            number_of_cards[order.id] += custody.amount 
                    number_of_cards[order.id] = round(number_of_cards[order.id]/ 51.0, 0)

        else:
            raise UserError("Please enter duration")'''
        

        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            #'orders': sales_records,
            'orders': final_data,
            'total': total,
            #'number_of_cards': number_of_cards
        }
        return self.env['report'].render('sales_report.detailed_salesman_report', docargs)
