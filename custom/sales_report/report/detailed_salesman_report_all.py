# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError


class ReportSalespersonall(models.AbstractModel):
    _name = 'report.sales_report.detailed_salesman_report_all'

    def get_data(self, date_from, date_to,vendor_id):
        report_data = []
        final_data ={}
        act_domain = ''
        invoice_obj = self.env['vendors.invoice']
        person_obj=self.env['hr.employee']
        if vendor_id ==False:
            act_domain = [('invoice_date','>=',date_from),('invoice_date',  '<=', date_to),('state', '=','posted')]
        else:
            act_domain = [('vendor_id', '=', vendor_id[0]),('invoice_date','>=',date_from),('invoice_date',  '<=', date_to),
            ('state', '=','posted')]
        result = invoice_obj.search(act_domain)
        persons_ids = person_obj.search([])
        for person in persons_ids:
            for line in result:
                if person == line.sales_man_id:
                    amount_total=total_taxed_amount=credit_custody=commision_total_amount=cash_custody=credit_sales=main_customer_total_untaxed=0.0
                    total = {'amount_total': 0.0, 'total_taxed_amount': 0.0, 'credit_custody': 0.0, 'cash_custody': 0.0,
                         'commision_total_amount': 0.0, 'credit_sales': 0.0, 'main_customer_total_untaxed': 0.0, }
                    number_of_cards = 0.0
                    amount_total=line.amount_total
                    total_taxed_amount=line.main_customer_total_amount-line.main_customer_total_untaxed
                    credit_custody=line.credit_custody
                    cash_custody=line.cash_custody
                    commision_total_amount=line.commision_total_amount
                    credit_sales=line.credit_sales
                    main_customer_total_untaxed=line.main_customer_total_untaxed
                    for custody in line.custody_lines:
                        if custody.is_complex:
                            number_of_cards += custody.amount
                    number_of_cards = number_of_cards/ 51.0

                    final_data[person] = final_data.get(person,{'data':[],'amount_total': 0.0, 'total_taxed_amount': 0.0, 'credit_custody': 0.0, 'cash_custody': 0.0,
                         'commision_total_amount': 0.0, 'credit_sales': 0.0, 'main_customer_total_untaxed': 0.0})
                    
                    final_data[person]['data'].append({
                                        'date': line.invoice_date,
                                        'name':line.name,
                                        'transmission_number':line.transmission_number,
                                        'amount_total':amount_total,
                                       'total_taxed_amount': total_taxed_amount,
                                       'credit_custody':credit_custody,
                                       'cash_custody':cash_custody,
                                       'commision_total_amount':commision_total_amount,
                                       'credit_sales':credit_sales,
                                       'number_of_cards':int(number_of_cards),
                                       'main_customer_total_untaxed':main_customer_total_untaxed,
                                       })
                    final_data[person]['amount_total']+=amount_total
                    final_data[person]['total_taxed_amount']+=total_taxed_amount
                    final_data[person]['credit_custody']+=credit_custody
                    final_data[person]['cash_custody']+=cash_custody
                    final_data[person]['commision_total_amount']+=commision_total_amount
                    final_data[person]['credit_sales']+=credit_sales
                    final_data[person]['main_customer_total_untaxed']+=main_customer_total_untaxed
        return final_data

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        vendor_id = data['form'].get('vendor_id')
        date_from=data['form'].get('date_from')
        date_to=data['form'].get('date_to')
        rm_act=self.with_context(data['form'].get('used_context',{}))
        data_res = rm_act.get_data(date_from, date_to,vendor_id)
        docargs = {
            'doc_ids': docids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'final_data': data_res,
            #'total': total,
            #'number_of_cards': number_of_cards
        }
        return self.env['report'].render('sales_report.detailed_salesman_report_all', docargs)
