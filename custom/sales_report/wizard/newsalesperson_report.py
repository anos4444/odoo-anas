# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import datetime

class newSalespersonWizard(models.TransientModel):
    _name = "newsalesperson.wizard"
    _description = "New Salesperson wizard"
    
    salesperson_ids = fields.Many2many('hr.employee', 'for_cust_inv_rep', string='Salepersons',domain=[('is_market_employee', '=', True)])
    salesperson_id = fields.Many2one('hr.employee', string='Salesperson',domain=[('is_market_employee', '=', True)])
    vendor_id = fields.Many2one('res.partner', string='Vendor Name', domain=['&',('supplier', '=', True),('is_market', '=', True)])
    date_from = fields.Date(string='Start Date',default=fields.Date.context_today)
    date_to = fields.Date(string='End Date',default=fields.Date.context_today)
    

    @api.multi
    def check_report(self, context=None):
        data = {}
        data['form'] = self.read(['salesperson_id', 'vendor_id', 'date_from', 'date_to'])[0]
        if context and 'detailed' in context and context['detailed']:
            return self._print_detailed_report(data)
        return self._print_report(data)

    def _print_report(self, data): 
        data['form'].update(self.read(['salesperson_id','vendor_id', 'date_from', 'date_to'])[0])
        return self.env['report'].get_action(self, 'sales_report.report_newsalesperson', data=data)
    
   
   
