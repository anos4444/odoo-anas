# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class CompanyData(models.Model):
    _inherit = 'res.company'
   

    name_eng = fields.Char('')
    company_rigist = fields.Char('')
    city_eng = fields.Char('')
    state_ara = fields.Char('')
    state_eng = fields.Char('')
    country_ara = fields.Char('')
    country_eng = fields.Char('')

