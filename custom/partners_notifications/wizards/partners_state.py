# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import date,datetime
from dateutil.parser import parse


class partner_state_reportWizard(models.TransientModel):
    _name = "partner_state_report.wizard"
    _description = "partner_state_report wizard"

    customers = fields.Boolean('Customers')
    suppliers = fields.Boolean('Suppliers')
    partners_ids = fields.Many2many(
        'res.partner', 'partners_state_report_partners_rel', string='Partners')
    date_to = fields.Date(string='Balance Date')

    balance = fields.Boolean('Balance not equals to zero')
    exceeded = fields.Boolean('exceeded')
    balance_to_limit_rate = fields.Boolean('balance rate not equals to zero')


    def get_data(self, data):
        domain = ['|', ('parent_id', '=', False), ('is_company', '=', True),'|',('customer', '=', self.customers),('supplier', '=', self.suppliers),]
        if self.balance:
            domain = [('balance','!=',0)]+ domain
        if self.exceeded:
            domain = [('exceeded','!=',0)]+ domain
        if self.balance_to_limit_rate:
            domain = [('balance_to_limit_rate','!=',0)]+ domain
        if self.partners_ids:
            domain = [('id', 'in', [x.id for x in self.partners_ids])]

        partners = self.env['res.partner'].search(domain)

        partners_ids = [x.id for x in partners]
        for p in partners:
            balance = 0.0
            for line in p.moves_lines_ids:
                if parse(line.date).date() < parse(self.date_to).date() and line.account_id.id == p.property_account_receivable_id.id:
                    balance += round(line.debit, 2) - round(line.credit, 2)

                # vendors
                if parse(line.date).date() < parse(self.date_to).date()  and line.account_id.id == p.property_account_payable_id.id:
                    balance +=  round(line.debit, 2)- round(line.credit, 2)
            p.balance=balance

        view = self.env.ref('partners_notifications.partner_state_tree')
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'res.partner',
            'view_mode': 'tree',
            'view_type': 'form',
            'domain': [('id','in',partners_ids)],
            'view_id': view.id,
            'target': 'current',
        }

        # data['form'].update(self.read(['partners_ids', 'date_to'])[0])
        # return self.env['report'].get_action(self,
        # 'partners_notifications.report_partner_state_report', data=data)
def get_date(str):
    return datetime.strptime(str, "%Y-%m-%d").date()