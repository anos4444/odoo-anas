# -*- coding: utf-8 -*-
{   
    'name': 'Partners Notifications',
    'summary': 'Partners debit and credit and payment constraints and notifications for sales manager',
    'version': '1.0',
    'category': 'accounting',
    'license': 'AGPL-3',
    'author': 'IES Team',
    'depends': ['base', 'account','cost_center'],

    "data": [
        "views/res_partner_view.xml",
        "wizards/partners_state.xml",
        "data/scheduler.xml",
        "reports/partners_noti_report.xml",
        "reports/partners_noti_balance.xml",
    ],
    "installable": True,
}
