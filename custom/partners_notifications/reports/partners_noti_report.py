# -*- coding: utf-8 -*-

from odoo import api, models


class partner_noti_report_template(models.AbstractModel):
    _name = 'report.partners_notifications.partner_noti_report_template'

    @api.model
    def render_html(self, docids, data=None):
        docs = self.env['res.partner'].browse(docids)
        partners = []
        docargs = {
            'doc_ids': docids,
            'doc_model': 'res.partner',
            'docs': docs,
        }

        return self.env['report'].render('partners_notifications.partner_noti_report_template', docargs)
