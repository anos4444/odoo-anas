# -*- coding: utf-8 -*-

from odoo import api, fields, models


class StockMove(models.Model):
    _inherit = "stock.move"

    new_quantity = fields.Float('Quantity')
    new_price = fields.Float('Price')
    new_weight = fields.Float('Weight', default=1)
    free_quantity = fields.Float('Free Quantity')
