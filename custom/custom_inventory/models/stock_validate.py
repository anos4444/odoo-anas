# -*- coding: utf-8 -*-

from odoo import api, models


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.model
    def create(self, vals):
        cr_location_id = self.env.context.get('cr_location_id')
        warehouse_id = self.env.context.get('warehouse_id')
        if cr_location_id and warehouse_id:
            vals['location_dest_id'] = cr_location_id.id
            vals['warehouse_id'] = warehouse_id.id
        return super(StockMove, self).create(vals)


class StockPackOp(models.Model):
    _inherit = "stock.pack.operation"

    @api.model
    def create(self, vals):
        vals['ordered_qty'] = vals.get('product_qty')
        vals['qty_done'] = vals.get('product_qty')
        return super(StockPackOp, self).create(vals)


class StocQuant(models.Model):
    _inherit = "stock.quant"

    @api.model
    def _quant_create_from_move(self, qty, move, lot_id=False, owner_id=False,
                                src_package_id=False, dest_package_id=False,
                                force_location_from=False, force_location_to=False):
        cr_location_id = self.env.context.get('cr_location_id')
        return super(StocQuant, self)._quant_create_from_move(qty, move, lot_id,owner_id,
                                src_package_id, dest_package_id,
                                force_location_from,cr_location_id)