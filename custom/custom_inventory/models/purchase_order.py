# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    warehouse_id = fields.Many2one(default=lambda self: self.env.user.user_default_warehouse)
