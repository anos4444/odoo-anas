# -*- coding: utf-8 -*-

from odoo import models, fields


class ResUsers(models.Model):
    _inherit = 'res.users'

    user_warehouses_ids = fields.Many2many('stock.warehouse', 'stock_warehouse_user_rel', 'user_id', 'warehouse_id',
                                           string='Allowed Warehouses')
    user_default_warehouse = fields.Many2one('stock.warehouse', string='Default Warehouse')


class Warehouse(models.Model):
    _inherit = "stock.warehouse"

    warehouse_users_ids = fields.Many2many('res.users', 'stock_warehouse_user_rel', 'warehouse_id', 'user_id',
                                           string='Allowed Users')
