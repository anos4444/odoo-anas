# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : "Cost Center",
    'version' : '1.0',
    'summary': '',
    'sequence': 2,
    'description':'',
    'category': 'Custom',
    'website': '',
    'depends' : ['base_setup', 'account_accountant', 'vegetables'],

    'data': [
            'security/groups.xml',
            'security/ir.model.access.csv',
            'security/security.xml',
            'views/areas_view.xml',
            'views/branches_view.xml',
            'views/department_view.xml',
            'views/cost_center_view.xml',
            'views/cars_view.xml',
            'views/project_view.xml',
            'views/shortcut_view.xml',
            'views/menus_view.xml',
            'views/balance_view.xml',
            'views/qweb.xml',
            #'views/user_view.xml',

    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

