# -*- coding: utf-8 -*-

from odoo import api, fields, models


class CostCenterCostCenter(models.Model):
    _name = 'cost_center.cost_center'

    name = fields.Char('Name')
    ar_name = fields.Char('Arabic Name')
    code = fields.Char('Code')
    state = fields.Selection([('active', 'Active'), ('in_active', 'Inactive')], string='Status', default='active')
    active = fields.Boolean(default=True)
    user_ids = fields.Many2many('res.users', 'cost_centers_user_rel', 'cost_center_id', 'user_id', string='Users')

    first_group = fields.Many2one('cost_center.cost_center.groups', string='First Group')
    second_group = fields.Many2one('cost_center.cost_center.groups', string='Second Group')
    third_group = fields.Many2one('cost_center.cost_center.groups', string='Third Group')
    forth_group = fields.Many2one('cost_center.cost_center.groups', string='Forth Group')
    fifth_group = fields.Many2one('cost_center.cost_center.groups', string='Group')

    @api.multi
    def name_get(self):
        result = []
        lang = self.env.user.lang
        for record in self:
            if lang == 'en_US':
                result.append((record.id, record.name))
            elif lang == 'ar_SY':
                result.append((record.id, record.ar_name or record.name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('name', operator, name), ('code', operator, name)]
        car = self.search(domain + args, limit=limit)
        return car.name_get()

    @api.multi
    def change_cost_center_status(self):
        for rec in self:
            if rec.state == 'active':
                rec.state = 'in_active'
                rec.active = False
            else:
                rec.state = 'active'
                rec.active = True

    @api.onchange('fifth_group')
    def onchange_fifth_group(self):
        if self.fifth_group.parent_group:
            self.forth_group = self.fifth_group.parent_group.id

        if self.forth_group.parent_group:
            self.third_group = self.forth_group.parent_group.id

        if self.third_group.parent_group:
            self.second_group = self.third_group.parent_group.id

        if self.second_group.parent_group:
            self.first_group = self.second_group.parent_group.id


class CostCenterGroups(models.Model):

    _name = 'cost_center.cost_center.groups'

    name = fields.Char('Group Name')
    ar_name = fields.Char('Arabic Name')
    parent_group = fields.Many2one('cost_center.cost_center.groups', string='Parent Group')
