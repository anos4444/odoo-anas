# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from openerp.exceptions import UserError, ValidationError


class CostCenterProject(models.Model):
    _name = 'custom.shortcuts'

    name = fields.Char(string='Shortcuts Name',required=True,copy=False)
    group_name = fields.Char(string='Group Name',required=True,copy=False)
    action_id = fields.Many2one('ir.actions.act_window', string='Action',required=True,copy=False)
    note = fields.Html('Note')
    state = fields.Selection([('draft','Draft'),('open','Open')],string='State',copy=False,default='draft')

    @api.one
    def to_draft(self):
        self.state = 'draft'
        return self.state

    @api.one
    def to_open(self):
        self.state = 'open'
        return self.state


    def go_action(self):

        if self.action_id:
            ty = [(False, str(line)) for line in self.action_id.view_mode.split(',')]
	    wwww = []
	    for a in self.action_id.view_ids:
		wwww.append((a.view_id.id,a.view_mode))
            data = {
                'name':str(self.action_id.name),
                'type':'ir.actions.act_window',
                'res_model':str(self.action_id.res_model),
                'context': self.action_id.context and str(self.action_id.context) or {},
                'domain':self.action_id.domain and str(self.action_id.domain) or [],
                'views':wwww or ty or [],
                'target':str(self.action_id.target),
            }
            return data
