# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : "Custom Check",
    'version' : '1.0',
    'summary': '',
    'sequence': 3,
    'description':'',
    'category': 'Custom',
    'website': '',
    'depends' : ['invoice_number','multi_supplier_payment','custom_account', 'check_followups'],

    'data': [
	'security/groups.xml',
	'views/invoice_number_view.xml',
        'views/res_company_view.xml',
	'views/account_payment_view.xml',
    ],
    #'css': ['static/src/css/account_move_line.css'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

