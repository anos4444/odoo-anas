# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import time

class InvoiceNumbers(models.Model):
    _inherit = 'manual.invoice.number'

    type = fields.Selection(selection_add=[('bank_payment', 'Bank Payment')], string='Type')
    bank_type = fields.Selection([('check', 'Check')], string='Bank Type')
    journal_id = fields.Many2one('account.journal', string="Journal")


class ManualNumberReportWizard(models.TransientModel):
    _inherit = "manual_number_report.wizard"

    type = fields.Selection(selection_add=[('bank_payment', 'Bank Payment')], string='Type', default='vendor_invoice')




