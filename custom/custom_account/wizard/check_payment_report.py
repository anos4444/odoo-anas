# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import api, fields, models


class check_payment_reportWizard(models.TransientModel):
    _name = "check_payment_report.wizard"
    _description = "check_payment_report wizard"

    date_from = fields.Date(string='Start Date',default=datetime.today())
    date_to = fields.Date(string='End Date')

    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['date_from', 'date_to'])[0]
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['date_from', 'date_to'])[0])
        return self.env['report'].get_action(self, 'custom_account.report_check_payment_report', data=data)
