# -*- coding: utf-8 -*-
from datetime import datetime
 
from odoo import api, fields, models, _

class VendorsCommessionsReport(models.Model):
    _name = 'vendors.commissions.wizard'

    date_from = fields.Date(string='Start Date',default=fields.Date.context_today,required="1")
    date_to = fields.Date(string='End Date',default=fields.Date.context_today,required="1")
    partner_id = fields.Many2one('res.partner', 'Vendor', domain=['&',('supplier', '=', True),('is_market', '=', True)])
    detailed=fields.Boolean(string='Detailed')

    @api.multi
    def print_report(self,data):
        data['form'] = self.read(['detailed'])[0]
        if data['form']['detailed'] == True:
            data = {
                'ids': self.ids,
                'model': 'vendors.invoice',
                'form': self.read(['date_from', 'date_to','partner_id'])[0]
            }
            return self.env['report'].get_action(self, 'custom_account.detailed_report_vendors_commissions',data=data)
        else:
            data = {
            'ids': self.ids,
            'model': 'vendors.invoice',
            'form': self.read(['date_from', 'date_to','partner_id'])[0]
            }
            return self.env['report'].get_action(self, 'custom_account.report_vendors_commissions',data=data)
