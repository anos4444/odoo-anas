# -*- coding: utf-8 -*-
from datetime import datetime
 
from odoo import api, fields, models, _

class AccountsTrialBalance(models.Model):
    _name = 'accounts.trial.balance'

    accounts = fields.Char(string='Account')
    accounts_code = fields.Char(string='Code')

    OPB_D = fields.Float(string='OPB Debit')
    OPB_C = fields.Float(string='OPB Credt')
    OPB_D_Date = fields.Float(string='OPB Debit Date')
    OPB_C_Date = fields.Float(string='OPB Credit Date')

    opb_debit = fields.Float(string='debit pefor start date')
    opb_credit = fields.Float(string='credit pefor start date')
    sum_debit = fields.Float(string='Sum Depit')
    sum_credit = fields.Float(string='Sum Credits')
    balance = fields.Float(string='Balance')
    balance_debit = fields.Float(string='Balance Debit')
    balance_credit = fields.Float(string='Balance Credit')
    move_line_id = fields.Integer(string='Move Line')

class print_accounts_trial_balance_report(models.TransientModel):

    _name = "accounts.trial.balance.wizard"
    
    date_from = fields.Date(string='Start Date',default=datetime.today())
    date_to = fields.Date(string='End Date',default=datetime.today())
     

    accounts_ids = fields.Many2many(
        'account.account', 'accounts_trial_balance_report', string='Accounts')

    check_balance = fields.Boolean('Balance not equals to zero ')
 

    def get_opb(self, account_id,move_line_ids,date_from):

        account_move_obj = self.env['account.move.line']
        res=[]
        
        OPB_D=OPB_C=OPB_D_Date=OPB_C_Date=opb_debit=opb_credit=sum_debit=sum_credit=0
        move_line = account_move_obj.search([('account_id','=',account_id),('id','in',move_line_ids)])
        name=''
        for line in move_line: 
            sum_debit=sum_debit+line.debit
            sum_credit=sum_credit+line.credit


        move_line = account_move_obj.search([('account_id','=',account_id),('date', '<', date_from )])
        for line in move_line:            
            if line.name :
                if line.name[0:3] == 'OPB':
                    if line.debit:
                        OPB_D=OPB_D+line.debit
                    else:
                        OPB_C=OPB_C+line.credit
                else:
                    OPB_D_Date=OPB_D_Date+line.debit
                    OPB_C_Date=OPB_C_Date+line.credit
            else:
                OPB_D_Date=OPB_D_Date+line.debit
                OPB_C_Date=OPB_C_Date+line.credit


        opb_debit=OPB_D+OPB_D_Date
        opb_credit=OPB_C+OPB_C_Date



        res.append(opb_debit)
        res.append(opb_credit)
        res.append(sum_debit)
        res.append(sum_credit)
        balance=opb_debit+sum_debit-opb_credit-sum_credit
        res.append(balance)

        res.append(OPB_D)
        res.append(OPB_C)
        res.append(OPB_D_Date)
        res.append(OPB_C_Date)

        return res

    
    def get_data(self, data):

        domain=[]
        account_ids=[]

        account_move_obj = self.env['account.move.line']
        account_obj = self.env['account.account']
        account_trial_balance = self.env['accounts.trial.balance']

        form_name='Accounts Trial Balance from '+str(self.date_from)+' to '+str(self.date_to)
        if self.accounts_ids:
            for x in self.accounts_ids:
                account_ids.append(x.id)
            domain = [('account_id','in',account_ids)]
        else:
            all_accounts = account_obj.search([])
            for account in all_accounts:
                account_ids.append(account.id)

        if self.date_from:
            domain = [('date', '>=', self.date_from )]+domain
        if self.date_to:
            domain = [('date', '<=', self.date_to )]+domain

        date_opb=self.date_from

        
        move_line = account_move_obj.search(domain)
        move_line_ids = [x.id for x in move_line]


        #delete old data in accounts.trial.balance
        old_data = account_trial_balance.search([])
        for x in old_data:
            x.unlink()

        for rec in account_ids:
            accounts_code = account_obj.search([('id', '=', rec )]).code
            account_name = account_obj.search([('id', '=', rec )]).name

            res=self.get_opb(rec, move_line_ids, date_opb)
            if self.check_balance:
                if res[4] != 0 :
                    balance_grater = 0
                    balance_less = 0

                    if res[4] > 0:
                        balance_grater = res[4]
                    elif res[4] < 0:
                        balance_less = res[4]
                    account_trial_balance_id = account_trial_balance.sudo().create({
                            'accounts_code': accounts_code,
                            'accounts': account_name,
                            'opb_debit': res[0],
                            'opb_credit': res[1],
                            'sum_debit': res[2],
                            'sum_credit': res[3],
                            'balance': res[4],
                            'balance_debit': balance_grater,
                            'balance_credit': balance_less * -1,

                            'OPB_D': res[5],
                            'OPB_C': res[6],
                            'OPB_D_Date': res[7],
                            'OPB_C_Date': res[8],
                        })
            else:

                balance_grater = 0
                balance_less = 0

                if res[4] > 0:
                    balance_grater = res[4]
                elif res[4] < 0:
                    balance_less = res[4]
                account_trial_balance_id = account_trial_balance.sudo().create({
                            'accounts_code': accounts_code,
                            'accounts': account_name,
                            'opb_debit': res[0],
                            'opb_credit': res[1],
                            'sum_debit': res[2],
                            'sum_credit': res[3],
                            'balance': res[4],
                            'balance_debit': balance_grater,
                            'balance_credit': balance_less * -1,

                            'OPB_D': res[5],
                            'OPB_C': res[6],
                            'OPB_D_Date': res[7],
                            'OPB_C_Date': res[8],
                        })
            
        return {
            'name':form_name,
            'type': 'ir.actions.act_window',
            'res_model': 'accounts.trial.balance',
            'view_type': 'form',            
            'view_mode': 'tree,form,graph',
            'views': [(False, 'tree'), (False, 'form'),(False, 'graph')],
            'target': 'current',
        }
