# -*- coding: utf-8 -*-

from odoo import api, models
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic


class partner_noti_report_template(models.AbstractModel):
    _name = 'report.custom_account.payment_print_template'


    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')

        docs = self.env['account.payment'].browse(docids)

        docargs = {
            'doc_ids': docids,
            'doc_model': 'account.payment',
            'docs': docs,

        }

        return self.env['report'].render('custom_account.payment_print_template', docargs)
