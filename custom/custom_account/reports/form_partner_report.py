#-*- coding:utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic


class FormReport(models.AbstractModel):
    _name = 'report.custom_account.form_partner_report'

    @api.model
    def render_html(self, docids, data=None):
        partner = self.env['customers.suppliers'].browse(docids)
        #print '******************************** ',partner.date_entry_mathcing,' ------------------------ ',partner.partner_name.id
        customer_invoice = self.env['account.invoice'].search([('partner_id', '=', partner.partner_name.id),('date', '=', partner.date_entry_mathcing)])
        #print '***********************---------------------------------------', customer_invoice.name
        docargs = {
            'doc_ids': docids,
            'doc_model': 'customers.suppliers',
            'total_words': amount_to_text_arabic(abs(partner.actual_balance), 'SAR'),
            'last_invoice': customer_invoice.name,
            'docs': partner,
            'data': data,
        }
        return self.env['report'].render('custom_account.form_partner_report', docargs)
