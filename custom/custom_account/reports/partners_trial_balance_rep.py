# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError

class partners_trial_balance(models.AbstractModel):
    _name = 'report.custom_account.partners_trial_balance_rep'
    @api.model
    def render_html(self, docids, data=None):
        docs = self.env['partners.trial.balance'].browse(docids)
        partners_balance=self.env['partners.trial.balance']
    	partners_balance_ids = partners_balance.search([])
    	tot_balance=tot_sum_credit=tot_sum_debit=tot_opb_credit=tot_opb_debit=0.0
        totals_res = {}
        for line in partners_balance_ids:
        	balance=credit=debit=opb_credit=opb_debit=0.0
        	tot_balance+=line.balance
        	tot_sum_credit+=line.sum_credit
        	tot_sum_debit+=line.sum_debit
        	tot_opb_credit+=line.opb_credit 
        	tot_opb_debit+=line.opb_debit
        totals_res = {
             'tot_balance': round(tot_balance,2),
             'tot_sum_credit': round(tot_sum_credit,2),
             'tot_sum_debit': round(tot_sum_debit,2),
             'tot_opb_credit':round(tot_opb_credit,2) ,
             'tot_opb_debit':round(tot_opb_debit,2),
        }
        docargs = {
            'doc_ids': docids,
            'doc_model': 'partners.trial.balance',
            'docs': docs,
            'totals': totals_res,
        }
        return self.env['report'].render('custom_account.partners_trial_balance_rep', docargs)
