# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    manual_inv_num = fields.Char('Manual Number')
    invoice_type = fields.Char('Invoice Type', default='customer invoice')

    @api.constrains('manual_inv_num')
    def _check_manual_inv_num(self):

        if self.type in ['out_invoice', 'out_refund']:
            invoice_numbers = self.env['manual.invoice.number'].sudo().search([('type', '=', 'customer')])
        else:
            invoice_numbers = self.env['manual.invoice.number'].sudo().search([('type', '=', 'vendor')])

        if self.manual_inv_num and invoice_numbers:

            if len(self.env['account.invoice'].sudo().search(
                    [('manual_inv_num', '=', self.manual_inv_num), ('state', '!=', 'cancel')])) > 1:
                raise ValidationError(_('This number is reserved in another invoice..!'))

            invoice_number_found = False

            for invoice in invoice_numbers:
                if invoice.range_from <= int(self.manual_inv_num) <= invoice.range_to:
                    invoice_number_found = True
                    break
            if not invoice_number_found and len(invoice_numbers) > 0:
                raise ValidationError(_('Invalid invoice number, this number not found in Invoices range.'))


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    @api.depends('invoice_id.manual_inv_num')
    def _compute_manual_inv_num(self):
        for rec in self:
            if rec.invoice_id:
                rec.manual_inv_num = rec.invoice_id.manual_inv_num

    account_id = fields.Many2one(required=False)
    manual_inv_num = fields.Char('Manual Number', compute=_compute_manual_inv_num)
    new_quantity = fields.Integer('Quantity', compute='_compute_new_quantity')
    new_price = fields.Float('Price')
    new_weight = fields.Float('Weight')
    return_products = fields.Float('Return')
    scrap_products = fields.Float('Scrap')
    free_quantity = fields.Float('Free')
    new_price_total = fields.Float('Total', compute='_compute_total_price')
    tax_amount = fields.Float('Tax Amount', compute='_compute_tax_amount')
    warehouse_id = fields.Many2one('stock.warehouse', string='Source Warehouse')

    @api.depends('quantity', 'new_weight', 'free_quantity')
    def _compute_new_quantity(self):
        for rec in self:
            if rec.new_weight:
                rec.new_quantity = (rec.quantity / rec.new_weight) - rec.free_quantity

    @api.depends('invoice_line_tax_ids', 'quantity', 'price_unit')
    def _compute_tax_amount(self):
        for rec in self:
            if rec.invoice_line_tax_ids:
                rec.tax_amount = (rec.quantity * rec.price_unit) * rec.invoice_line_tax_ids[0].amount / 100

    @api.depends('new_price', 'new_quantity', 'invoice_line_tax_ids')
    def _compute_total_price(self):
        for rec in self:
            if rec.invoice_line_tax_ids:
                rec.new_price_total = rec.new_quantity * rec.new_price + \
                                      (rec.new_quantity * rec.new_price) * rec.invoice_line_tax_ids[0].amount / 100
            else:
                rec.new_price_total = rec.new_quantity * rec.new_price

    @api.onchange('quantity', 'price_unit')
    def _onchange_quantity_price_unit(self):
        if self.quantity > 0 and self.purchase_line_id:
            if self.purchase_line_id.new_quantity < self.quantity:
                self.quantity = self.purchase_line_id.new_quantity - self.purchase_line_id.new_qty_invoiced
                raise ValidationError(_('Quantity must be less than ' + str(self.quantity) + ' for product ' + str(
                    self.product_id.display_name)))
        elif self.price_unit > 0and self.purchase_line_id:
            if self.purchase_line_id.new_price < self.price_unit:
                self.price_unit = self.purchase_line_id.new_price
                raise ValidationError(_('Price must be less than ' + str(self.price_unit) + ' for product ' + str(
                    self.product_id.display_name)))
