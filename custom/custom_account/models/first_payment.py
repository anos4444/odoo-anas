# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError
import datetime


class first_payment(models.Model):
    _name = "account.first_payment"
    _description = "first payment"
    _order = "payment_date desc, name desc"

    # The name is attributed upon post()
    name = fields.Char(readonly=True, copy=False, default="Draft Payment")
    state = fields.Selection([('draft', 'Draft'), ('posted', 'Posted'), ('sent', 'Sent'), (
        'reconciled', 'Reconciled')], readonly=True, default='draft', copy=False, string="Status")

    payment_type = fields.Selection([('inbound', 'Credit'), (
        'outbound', 'Debit')], string='Payment Type', required=True, default='outbound')

    payment_date = fields.Date(
        string='Payment Date', default=fields.Date.context_today, required=True, copy=False)

    partner_type = fields.Selection(
        [('customer', 'Customer'), ('supplier', 'Vendor')])
    partner_id = fields.Many2one('res.partner', string='Partner')

    journal_id = fields.Many2one('account.journal', string='Payment Journal', domain=[
                                 ('type', 'in', ('bank', 'cash'))])

    amount = fields.Monetary(string='Payment Amount')
    currency_id = fields.Many2one('res.currency', string='Currency', required=True,
                                  default=lambda self: self.env.user.company_id.currency_id)

    company_id = fields.Many2one(
        'res.company', related='journal_id.company_id', string='Company', readonly=True)

    payment_method_id = fields.Many2one(
        'account.payment.method', string='Payment Method Type', oldname="payment_method")

    payment_method_code = fields.Char(related='payment_method_id.code',
                                      help="Technical field used to adapt the interface to the payment type selected.", readonly=True)

    payment_id = fields.Many2one('account.payment', string='Payment', copy=False)

    invoice_id = fields.Many2one('account.invoice', string='Ivoice', copy=False)

    @api.one
    @api.constrains('amount')
    def _check_amount(self):
        if not self.amount > 0.0:
            raise ValidationError(
                _('The payment amount must be strictly positive.'))

    @api.onchange('journal_id')
    def _onchange_journal(self):
        if self.journal_id:
            self.currency_id = self.journal_id.currency_id or self.company_id.currency_id
            # Set default payment method (we consider the first to be the
            # default one)
            payment_methods = self.payment_type == 'inbound' and self.journal_id.inbound_payment_method_ids or self.journal_id.outbound_payment_method_ids
            self.payment_method_id = payment_methods and payment_methods[0] or False
            # Set payment method domain (restrict to methods enabled for the
            # journal and to selected payment type)
            payment_type = self.payment_type in (
                'outbound', 'transfer') and 'outbound' or 'inbound'
            return {'domain': {'payment_method_id': [('payment_type', '=', payment_type), ('id', 'in', payment_methods.ids)]}}
        return {}

    @api.onchange('partner_type')
    def _onchange_partner_type(self):
        # Set partner_id domain
        if self.partner_type:
            return {'domain': {'partner_id': [(self.partner_type, '=', True)]}}

    @api.onchange('payment_type')
    def _onchange_payment_type(self):
        # Set default partner type for the payment type
        if self.payment_type == 'inbound':
            self.partner_type = 'customer'
        elif self.payment_type == 'outbound':
            self.partner_type = 'supplier'
        # Set payment method domain
        res = self._onchange_journal()
        if not res.get('domain', {}):
            res['domain'] = {}
        res['domain']['journal_id'] = self.payment_type == 'inbound' and [
            ('at_least_one_inbound', '=', True)] or [('at_least_one_outbound', '=', True)]
        res['domain']['journal_id'].append(('type', 'in', ('bank', 'cash')))
        return res
    @api.multi
    def to_draft(self):
        for rec in self:
            if rec.payment_id:
                rec.payment_id.cancel()
                #rec.payment_id = False
            if rec.invoice_id:
                now = datetime.now().date()
                invoice_id = rec.invoice_id
                move_id = rec.invoice_id.move_id
                rec.invoice_id.action_invoice_cancel()
                rec.invoice_id.action_invoice_draft()
                
                #move_id.button_cancel()
                #move_id.unlink()
                #vv = rec.invoice_id.move_id.reverse_moves(now, False)
                #rec.invoice_id.cancel_invoice()
                rec.invoice_id = False
                #invoice_id.unlink()
            rec.write({'state': 'draft'})
    @api.multi
    def post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        for rec in self:

            if rec.state != 'draft':
                raise UserError(
                    _("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)

            # Use the right sequence to set the name
            else:
                if (rec.partner_type == 'customer' and rec.payment_type == 'inbound') or (rec.partner_type == 'supplier' and rec.payment_type == 'outbound'):
                    data = rec.read()[0]
                    del data['create_uid']
                    del data['create_date']
                    del data['name']
                    for i in data:
                        if type(data[i]) is tuple:  # for many2one fields
                            data[i] = data[i][0]
                    payment_id = self.env['account.payment'].create(data)
                    payment_id.post()
                    rec.write({'state': 'posted', 'payment_id': payment_id.id})

                if (rec.partner_type == 'customer' and rec.payment_type == 'outbound') or (rec.partner_type == 'supplier' and rec.payment_type == 'inbound'):
                    data= {'type': rec.partner_type == 'customer' and 'out_invoice' or rec.partner_type == 'supplier' and 'in_invoice',
                    'partner_id': rec.partner_id.id,
                    'date_invoice':rec.payment_date,
                    'legacy_amount':rec.amount,
                    'currency_id':rec.currency_id.id,
                    }
                    if rec.partner_type == 'supplier' : data['journal_type'] = 'purchase'
                    invoice_id = self.env['account.invoice'].create(data)
                    invoice_id.action_invoice_open()
                    rec.write({'state': 'posted', 'invoice_id': invoice_id.id})
                
                
                sequence_code = 'account.first.payment'
                    
                rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(sequence_code)

                # Create the journal entry
                #amount = rec.amount * (rec.payment_type in ('outbound', 'transfer') and 1 or -1)

    @api.multi
    def button_payment(self):
        return {
            'name': _('Payments'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.payment',
            'view_id': self.env.ref('account.view_account_payment_form').id ,
            'type': 'ir.actions.act_window',
            'res_id' : self.payment_id.id
        }
    
    @api.multi
    def button_invoice(self):
        return {
            'name': _('invoices'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'view_id': self.partner_type == 'supplier' and self.env.ref('account.invoice_supplier_form').id or self.env.ref('account.invoice_form').id ,
            'type': 'ir.actions.act_window',
            'res_id' : self.invoice_id.id
        }
    @api.multi
    def unlink(self):
        for invoice in self:
            if invoice.state != 'draft':
                raise UserError(_('You cannot delete post payment'))

        res = super(first_payment, self).unlink()
        return res



class account_invoice(models.Model):
    _inherit = "account.invoice"
    legacy_amount = fields.Monetary(string='Payment Amount', required=True)
    first_payment_id = fields.Many2one('account.first_payment', string='Payment', copy=False)

    @api.multi
    def unlink(self):
        # for invoice in self:
        #     if invoice.legacy_amount != 0:
        #         raise UserError(_('You cannot delete post invoice'))

        res = super(account_invoice, self).unlink()
        return res
    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice', 'type')
    def _compute_amount(self):
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_tax = sum(line.amount for line in self.tax_line_ids)
        self.amount_total = self.amount_untaxed + self.amount_tax
        if self.legacy_amount != 0:
            self.amount_total = self.amount_untaxed = self.legacy_amount
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign
        self.residual=self.amount_total

        aaaaa = 0.0
        for invoice_line in self.sudo().invoice_line_ids:
            aaaaa += invoice_line.tax_amount #* invoice_line.quantity * 5 / 100
            self.amount_tax = aaaaa

        
    

    @api.multi
    def compute_invoice_totals(self, company_currency, invoice_move_lines):
        total = 0
        total_currency = 0
        for line in invoice_move_lines:
            if self.currency_id != company_currency:
                currency = self.currency_id.with_context(date=self.date_invoice or fields.Date.context_today(self))
                if not (line.get('currency_id') and line.get('amount_currency')):
                    line['currency_id'] = currency.id
                    line['amount_currency'] = currency.round(line['price'])
                    line['price'] = currency.compute(line['price'], company_currency)
            else:
                line['currency_id'] = False
                line['amount_currency'] = False
                line['price'] = self.currency_id.round(line['price'])
            if self.type in ('out_invoice', 'in_refund'):
                total += line['price']
                total_currency += line['amount_currency'] or line['price']
                line['price'] = - line['price']
            else:
                total -= line['price']
                total_currency -= line['amount_currency'] or line['price']
        if self.legacy_amount != 0:
             total = total_currency = self.legacy_amount
        return total, total_currency, invoice_move_lines
    
    @api.depends('invoice_line_ids', 'invoice_line_ids.discount_amount', 'invoice_line_ids.discount_percentage_total')
    def _get_total_invoice(self):
        for rec in self:
            total = 0
            discount_amount = 0
            discount_percentage_amount = 0
            net_total = 0
            for line in rec.invoice_line_ids:
                total += line.price_unit * line.executed_quantity

                discount_amount += line.discount_amount
                discount_percentage_amount += line.discount_percentage_total
                net_total += line.price_subtotal

            rec.total_discount_amount = discount_amount
            rec.total_percentage_discount_amount = discount_percentage_amount

            rec.amount_untaxed = total - (rec.total_discount_amount + rec.total_percentage_discount_amount)
            rec.amount_total = net_total
            if rec.legacy_amount != 0:
                rec.amount_untaxed = rec.amount_total = rec.legacy_amount
    
    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']
        account_account = self.env['account.account']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(_('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                #for first payments invoices
                if inv.legacy_amount == 0 and inv.first_payment_id:
                    #print "***********************************************************************"
                    raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            print inv.invoice_line_ids

            if not inv.date_invoice:
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            date_invoice = inv.date_invoice
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()

            diff_currency = inv.currency_id != company_currency

            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, iml)
            name = inv.name or '/'
            if inv.vegetables: 
                name += _(' : فاتورة رقم')
            if inv.payment_term_id:
                totlines = \
                    inv.with_context(ctx).payment_term_id.with_context(currency_id=company_currency.id).compute(total,
                                                                                                                date_invoice)[
                        0]
                res_amount_currency = total_currency
                ctx['date'] = date_invoice
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency
                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            else:
                # iml.append({
                #     'type': 'dest',
                #     'name': name,
                #     'price': total,
                #     'account_id': inv.account_id.id,
                #     'date_maturity': inv.date_due,
                #     'amount_currency': diff_currency and total_currency,
                #     'currency_id': diff_currency and inv.currency_id.id,
                #     'invoice_id': inv.id
                # })

                # Make Customer account and discount in Debit site
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total - (inv.total_percentage_discount_amount + inv.total_discount_amount),
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })
                if inv.total_percentage_discount_amount + inv.total_discount_amount > 0:
                    iml.append({
                        'type': 'dest',
                        'name': _("Discount"),
                        'price': inv.total_percentage_discount_amount + inv.total_discount_amount,
                        'account_id': inv.partner_id.discount_account.id,
                        'date_maturity': inv.date_due,
                        'amount_currency': diff_currency and total_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            
            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
            line = [(0, 0, self.line_get_convert(l, part.id)) for l in iml]
            line = inv.group_lines(iml, line)
            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)

            if inv.legacy_amount != 0:
                temp = {}
                for k in line[0][2]:
                   temp[k] = line[0][2][k] 
                
                if inv.type == 'out_invoice':
                    temp['account_id'] = inv.company_id.first_payment_account_id.id
                if inv.type == 'in_invoice':
                    line[0][2]['account_id'] = inv.company_id.first_payment_account_id.id
                credit = temp['credit']
                temp['credit'] = temp['debit']
                temp['debit'] = credit
                
                line.append((0,0,temp))
            date = inv.date or date_invoice



            
          

            ######################################################################
            
            if inv.type == 'out_invoice':
                #print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
                user_id = self.env.user.id
                move_name = journal.sequence_id.with_context(ir_sequence_date=date).next_by_id()
                #move_date = datetime.datetime.now().strftime("%Y-%m-%d")
                self._cr.execute("select id from account_move where ref = '%s';" %(inv.name))
                res = self._cr.dictfetchall()
                
                if not res :
                    
                    self._cr.execute("insert into account_move "\
                                    "( create_uid, name, state, ref, company_id, journal_id, "\
                                    "  currency_id, amount, matched_percentage, write_date, date, "\
                                    "  create_date, write_uid, reversed, custom_view, credit_amount) VALUES "\
                                    "( %s, '%s', '%s', '%s' ,%s, %s, "\
                                    " %s, %s, %s, '%s', '%s', "\
                                    " '%s', %s, '%s', '%s', %s) ;" 
                                    %(user_id, move_name, 'draft', inv.name, self.company_id.id, journal.id,
                                    self.currency_id.id, inv.amount_total, 0.0, datetime.datetime.now(), date,
                                    datetime.datetime.now(), user_id, 'f', 'default', inv.amount_total))
                    
                    
                    self._cr.execute("select id from account_move where ref = '%s';" %(inv.name))
                    res = self._cr.dictfetchall()
                    account_move_id_new = res[0]['id']
                else:
                    account_move_id_new = res[0]['id']

                for lin in line:
                    self._cr.execute("insert into account_move_line "\
                    "( create_date, journal_id, date_maturity, user_type_id, "\
                    "  partner_id, create_uid, amount_residual, company_id, "\
                    "  credit_cash_basis, amount_residual_currency ,debit, "\
                    "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                    "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                    "  company_currency_id, name, credit, "\
                    "  amount_currency, balance, active, invoice_id, product_id, quantity, product_uom_id) values "\
                    "( '%s',%s,'%s', %s, "\
                    "  %s, %s, %s, %s, "\
                    "  %s, %s, %s, "\
                    " '%s', %s, %s, '%s' , '%s', "\
                    "  %s, '%s' ,'%s' ,%s , %s, "\
                    "  %s ,'%s' , %s, "\
                    "  %s ,%s ,'%s' ,%s, %s, %s, %s );" 
                    %(datetime.datetime.now(), journal.id, date, account_account.browse(lin[2]['account_id']).user_type_id.id,
                    lin[2]['partner_id'], user_id, 0.0, self.company_id.id,
                    0.0, 0.0, lin[2].get('debit', 0.0) or 0.0,
                    inv.name, lin[2]['account_id'], 0.0, 'f', 't',
                    0.0, datetime.datetime.now(), date, user_id, account_move_id_new,
                    self.currency_id.id, lin[2]['name'], lin[2].get('credit', 0.0) or 0.0,
                    0.0, lin[2].get('debit', 0.0) or 0.0 - lin[2].get('credit', 0.0) or 0.0, 't', inv.id, lin[2].get('product_id', 'null') or 'null', lin[2].get('quantity', ''), lin[2].get('product_uom_id', '') or 'null'))

                account_move.browse(account_move_id_new).post()

                vals = {
                        'move_id': account_move_id_new,
                        'date': date,
                        'move_name': move_name,
                    }
                inv.with_context(ctx).write(vals)
            ######################################################################
            else:
                 
                move_vals = {
                    'ref': inv.sequence_number,
                    'line_ids': line,
                    'journal_id': journal.id,
                    'date': date,
                    'narration': inv.comment,
                }
                ctx['company_id'] = inv.company_id.id
                ctx['invoice'] = inv
                ctx_nolang = ctx.copy()
                ctx_nolang.pop('lang', None)
                move = account_move.with_context(ctx_nolang).create(move_vals)
                # Pass invoice in context in method post: used if you want to get the same
                # account move reference when creating the same invoice after a cancelled one:
                move.post()
            # make the invoice point to that move
                vals = {
                    'move_id': move.id,
                    'date': date,
                    'move_name': move.name,
                }
                inv.with_context(ctx).write(vals)

            self._compute_amount()
            
        return True
        
    
class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'

    first_payment_account_id = fields.Many2one(related='company_id.first_payment_account_id', 
        string="First Payment Account",)

class ResCompany(models.Model):
    _inherit = 'res.company'

    first_payment_account_id = fields.Many2one(
        'account.account',
        string="First Payment Account",)
