# -*- coding: utf-8 -*-

from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'stock.warehouse'

    warehouse_type_id = fields.Many2many('company.types', 'warehouse_type_rel', 'warehouse_id' , 'type_id' , 'Type', required=True)

    is_market_warehouse = fields.Boolean(string='Agricultural Market', default=True)
    is_vegetable_warehouse = fields.Boolean(string='Is Vegetable')
