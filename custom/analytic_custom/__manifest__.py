# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name' : 'Analytic Accounting Custom',
    'version': '1.1',
    'category': 'Hidden/Dependency',
    'depends' : ['account','analytic'],
    'description': """
Module for defining analytic accounting object.
===============================================

In Odoo, adding analytic plans 
    """,
    'data': [
        'views/analytic_account_views.xml',
        'views/account_payment.xml',
        'views/account_move.xml',
        ],
    'installable': True,
    'auto_install': False,
}
