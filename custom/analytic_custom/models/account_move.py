# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    analytic_plan_id = fields.Many2one(
        'account.analytic.plan', string='Analytic Plan')
    
    @api.multi
    def create_analytic_lines(self):
        """ Create analytic items upon validation of an account.move.line having an analytic account. This
            method first remove any existing analytic item related to the line before creating any new one.
        """
        for obj_line in self:
            if obj_line.analytic_line_ids:
                obj_line.analytic_line_ids.unlink()
            if obj_line.analytic_account_id:
                vals_line = obj_line._prepare_analytic_line()[0]
                self.env['account.analytic.line'].create(vals_line)
            if obj_line.analytic_plan_id:
                vals_line = obj_line._prepare_analytic_line()[0]
                for val in vals_line:
                    self.env['account.analytic.line'].create(val)
    
    @api.one
    def _prepare_analytic_line(self):
        """ Prepare the values used to create() an account.analytic.line upon validation of an account.move.line having
            an analytic account. This method is intended to be extended in other modules.
        """
        amount = (self.credit or 0.0) - (self.debit or 0.0)
        
        if not self.analytic_plan_id:
            return {
                'name': self.name,
                'date': self.date,
                'account_id': self.analytic_account_id.id,
                'tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
                'unit_amount': self.quantity,
                'product_id': self.product_id and self.product_id.id or False,
                'product_uom_id': self.product_uom_id and self.product_uom_id.id or False,
                'amount': self.company_currency_id.with_context(date=self.date or fields.Date.context_today(self)).compute(amount, self.analytic_account_id.currency_id) if self.analytic_account_id.currency_id else amount,
                'general_account_id': self.account_id.id,
                'ref': self.ref,
                'move_id': self.id,
                'user_id': self.invoice_id.user_id.id or self._uid,
            }
        if self.analytic_plan_id:
            list_dict = []
            for acc in self.analytic_plan_id.accounts_lines:
                temp_amount = (amount * acc.percent)/100.0
                list_dict.append({
                'name': self.name,
                'date': self.date,
                'account_id': acc.account_id.id,
                'tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
                'unit_amount': self.quantity,
                'product_id': self.product_id and self.product_id.id or False,
                'product_uom_id': self.product_uom_id and self.product_uom_id.id or False,
                'amount': self.company_currency_id.with_context(date=self.date or fields.Date.context_today(self)).compute(temp_amount, acc.account_id.currency_id) if acc.account_id.currency_id else temp_amount,
                'general_account_id': self.account_id.id,
                'ref': self.ref,
                'move_id': self.id,
                'user_id': self.invoice_id.user_id.id or self._uid,
            })

        return list_dict
