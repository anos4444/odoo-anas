# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class AccountAnalyticplan(models.Model):
    _name = 'account.analytic.plan'
    _description = 'Analytic Plans'
    name = fields.Char(string='Analytic plan', index=True, required=True)
    color = fields.Integer('Color Index')
    accounts_lines = fields.One2many('account.analytic.plan.line', 'plan_id', string='Accounts', copy=True)

class AccountAnalyticplanLine(models.Model):
    _name = 'account.analytic.plan.line'
    _description = 'Analytic Plan Line'

    percent = fields.Float('Percent %100', default=0.0)
    account_id = fields.Many2one('account.analytic.account', 'Analytic Account', required=True, ondelete='restrict', index=True)
    plan_id = fields.Many2one('account.analytic.plan', 'Analytic Account Plan', required=True, ondelete='restrict', index=True)


