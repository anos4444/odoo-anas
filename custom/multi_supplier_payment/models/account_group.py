# -*- coding: utf-8 -*-

from odoo import api, fields, models


class AccountGroup(models.Model):

    _name = 'account.group'

    name = fields.Char('Name')
    ar_name = fields.Char('Arabic Name')
    parent_id = fields.Many2one('account.group', 'Parent')
    user_type_id = fields.Many2one('account.account.type', string='Type', required=True, oldname="user_type",
            help="Account Type is used for information purpose, ""to generate country-specific legal reports, \
            and set the rules to close a fiscal year and generate opening entries.")
    code = fields.Char('Code', copy=False)
    next_number = fields.Integer('Next Number', default=1, copy=False)
    next_group_number = fields.Integer('Next Group Number', default=1, copy=False)

    _sql_constraints = [
        ('unique_code', 'unique(code, company_id)', 'Group code must be unique')
    ]

    # @api.multi
    # def name_get(self):
    #     def get_names(cat):
    #         """ Return the list [cat.name, cat.parent_id.name, ...] """
    #         lang = self.env.user.lang
    #         if lang == 'en_US':
    #             res = []
    #             while cat:
    #                 res.append(cat.name)
    #                 cat = cat.parent_id
    #             return res
    #         elif lang == 'ar_SY':
    #             res = []
    #             while cat:
    #                 if cat.ar_name:
    #                     res.append(cat.ar_name)
    #                     cat = cat.parent_id
    #                 else:
    #                     res.append(cat.name)
    #                     cat = cat.parent_id
    #             return res
    #
    #     return [(cat.id, " / ".join(reversed(get_names(cat)))) for cat in self]

    @api.onchange('parent_id')
    def onchange_parent_id(self):
        if self.parent_id:
            self.user_type_id = self.parent_id.user_type_id.id
            if self.parent_id.code and self.parent_id.next_group_number:
                self.code = self.parent_id.code + str(str(self.parent_id.next_group_number).zfill(2))
                self.parent_id.write({'next_group_number': self.parent_id.next_group_number +1})