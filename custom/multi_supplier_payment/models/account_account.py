# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import time

class AccountAccount(models.Model):
    _inherit = 'account.account'

    def get_value(self, rec):
        if len(rec.childs_ids) == 0:
            return self.query_results[rec.id]['debit'] or 0.0,self.query_results[rec.id]['credit'] or 0.0
        debit = self.query_results[rec.id]['debit'] or 0.0
        credit = self.query_results[rec.id]['credit'] or 0.0
        for child in rec.childs_ids:
            temp_debit,temp_credit = self.get_value(child)
            debit += temp_debit
            credit += temp_credit
        return debit,credit
    @api.depends('move_lines_ids')
    def _get_move_lines_amounts(self):
        self.env.cr.execute(
                    """select sum(l.debit) as debit , sum(l.credit) as credit, ac.id from account_account ac
                       left join account_move_line l on (l.account_id=ac.id)
                       group by ac.id""")
        query_results = self.env.cr.dictfetchall()
        query_results = {x['id']:x for x in query_results}
        self.query_results = query_results
        for rec in self:
            if rec.id in query_results:
                all_debit,all_credit = self.get_value(rec)
                rec.all_debit = all_debit or 0.0
                rec.all_credit = all_credit or 0.0

    group_id = fields.Many2one('account.group', string='Account Group')
    parent_group_id = fields.Many2one('account.group', string='Parent Account Group')
    payment_type = fields.Selection([('cash', 'Cash'), ('bank', 'Bank')], 'Payment Type')

    view_payment_type = fields.Boolean(default=False)
    
    first_group = fields.Many2one('account.group', string='First Group')
    second_group = fields.Many2one('account.group', string='Second Group')
    third_group = fields.Many2one('account.group', string='Third Group')
    forth_group = fields.Many2one('account.group', string='Forth Group')
    fifth_group = fields.Many2one('account.group', string='Fifth Group')
    ar_name = fields.Char('Arabic Name')
    parent_id = fields.Many2one('account.account', string='Parent')
    childs_ids = fields.One2many('account.account', 'parent_id', string='Childs')
    move_lines_ids = fields.One2many('account.move.line', 'account_id', string='Moves Lines')
    all_debit = fields.Float(compute='_get_move_lines_amounts', string='Sum Debit', default=0.0)
    all_credit = fields.Float(compute='_get_move_lines_amounts', string='Sum Credit', default=0.0)
    level = fields.Integer(string='Level', default=0)
    code_pading = fields.Integer(string='code pading', default=3)

    @api.model
    def default_get(self, default_fields):
        """If we're creating a new account through a many2one, there are chances that we typed the account code
        instead of its name. In that case, switch both fields values.
        """
        default_name = self._context.get('default_name')
        default_code = self._context.get('default_code')
        if default_name and not default_code:
            try:
                default_code = int(default_name)
            except ValueError:
                pass
            if default_code:
                default_name = False
        if not default_code:
            count = 1
            while True:
                try:
                    self.env.cr.execute(
                                    "select id from account_account where code='"+str(count).zfill(3)+"'")
                    query_results = self.env.cr.dictfetchall()
                    if query_results:
                        count += 1
                        continue
                    default_code = str(count).zfill(3)
                    
                    break
                except :
                    count += 1

        contextual_self = self.with_context(default_name=default_name, default_code=default_code)
        return super(AccountAccount, contextual_self).default_get(default_fields)

    @api.multi
    def name_get(self):
        result = []
        lang = self.env.user.lang
        for record in self:
            if lang == 'en_US':
                result.append((record.id, record.name))
            elif lang == 'ar_SY':
                result.append((record.id, record.ar_name or record.name))
        return result

    @api.onchange('user_type_id')
    def onchange_user_type_id(self):
        result = {}
        if self.user_type_id:
            self.group_id = False
            if self.user_type_id.name == 'Bank and Cash'.decode(
                    'utf-8') or self.user_type_id.name == 'النقدية و البنك'.decode('utf-8'):
                self.view_payment_type = True
            else:
                self.view_payment_type = False
            result['domain'] = {'group_id': [('user_type_id', '=', self.user_type_id.id)]}
        return result

    @api.onchange('group_id')
    def onchange_group_id(self):
        if self.group_id.parent_id:
            self.parent_group_id = self.group_id.parent_id.id

        if self.group_id.code and self.group_id.next_number:
            self.code = self.group_id.code + str(str(self.group_id.next_number).zfill(4))
            self.group_id.write({'next_number': self.group_id.next_number +1})
    
    def set_code_level_rc(self, parent, count):
        parent.code = count
        code_pading = parent.code_pading
        if parent.childs_ids == []:
            return

        ch_count = 1
        for ch in parent.childs_ids:
            ch.code_pading = code_pading
            self.set_code_level_rc(ch, str(count).zfill(code_pading) +  str(ch_count).zfill(code_pading))
            ch_count += 1

        
    def set_code_level(self):
        parents = self.search([('parent_id','=',False)])
        count = 1
        for parent in parents:
            code_pading = parent.code_pading
            parent.code = str(count).zfill(code_pading)
            ch_count = 1
            for ch in parent.childs_ids:
                ch.code_pading = code_pading
                self.set_code_level_rc(ch, str(count).zfill(code_pading) +  str(ch_count).zfill(code_pading))
                ch_count += 1
            count += 1
    

    def set_level_rc(self, parent, count):
        parent.level = count
        if parent.childs_ids == []:
            return

        ch_count = count + 1
        for ch in parent.childs_ids:
            self.set_level_rc(ch, ch_count)
            
    def set_level(self):
        parents = self.search([('parent_id','=',False)])
        count = 0
        for parent in parents:
            parent.level = 0
            ch_count = 1
            for ch in parent.childs_ids:
                self.set_level_rc(ch, ch_count)
                     
    

    @api.model
    def create(self, vals):
        rec = super(AccountAccount, self.with_context(mail_create_nolog=True)).create(vals)
        if 'parent_id' in vals:
            parent_code = ""
            if rec.parent_id:
                parent_code = rec.parent_id.code
                rec.code_pading = rec.parent_id.code_pading
            parent_level = -1
            if rec.parent_id:
                parent_level = rec.parent_id.level
            rec.level = parent_level+1
            count = 1
            if 'code' in vals and not rec.parent_id:
                rec.code = vals['code']
            if rec.parent_id or 'code' not in vals:
                while True:
                    try:
                        self.env.cr.execute(
                                        "select id from account_account where code='"+parent_code + str(count).zfill(rec.code_pading)+"'")
                        query_results = self.env.cr.dictfetchall()
                        if query_results:
                            count += 1
                            continue
                        rec.code = parent_code + str(count).zfill(rec.code_pading)
                        
                        break
                    except :
                        count += 1
                
            ch_count = 1
            for ch in rec.childs_ids:
                ch.code_pading = rec.code_pading
                self.set_code_level_rc(ch, rec.code +  str(ch_count).zfill(rec.code_pading))
                ch_count += 1
        return rec
    
    @api.multi
    def write(self, vals):
        res = super(AccountAccount, self).write(vals)
        if 'parent_id' in vals and not 'no_parent_update' in self.env.context :
            for rec in self:
                parent_code = ""
                if rec.parent_id:
                    parent_code = rec.parent_id.code
                    rec.code_pading = rec.parent_id.code_pading
                count = 1
                if 'code' in vals and not rec.parent_id:
                    rec.code = vals['code']
                if rec.parent_id or 'code' not in vals:
                    while True:
                        try:
                            self.env.cr.execute(
                                        "select id from account_account where code='"+parent_code + str(count).zfill(rec.code_pading)+"'")
                            query_results = self.env.cr.dictfetchall()
                            if query_results:
                                count += 1
                                continue
                            
                            rec.code = parent_code + str(count).zfill(rec.code_pading)
                            break
                        except :
                            count += 1
                    
                ch_count = 1
                for ch in rec.childs_ids:
                    ch.code_pading = rec.code_pading
                    self.set_code_level_rc(ch, rec.code +  str(ch_count).zfill(rec.code_pading))
                    ch_count += 1
                break #needed only once
            #self.set_level()
        return res




class AccountJournal(models.Model):
    _inherit = 'account.journal'

    partner_id = fields.Many2one('res.partner', string='Partner')
