# -*- coding: utf-8 -*-

from odoo import SUPERUSER_ID
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class customers_supliers_balances(models.Model):
    
    _name = 'customers.suppliers'

    partner_name = fields.Many2one('res.partner',string='Partner Name',domain="[('parent_id', '=', False)]",required=True)
    matching_date = fields.Date(string='Matching Date', required=True)
    balance = fields.Float(string='Balance', required=True)
    date_entry_mathcing = fields.Date(string='Date of entry of mathcing',default=fields.Date.context_today)
    responsible_person = fields.Many2one('res.partner',string='Responsible Person',domain="[('parent_id', '=', partner_name)]",required=True)
    actual_balance = fields.Float(string='Actual Balance', readonly=True)
    note = fields.Text(string='Note')




    def get_actual_balance(self,partner,matching_date):
        #matching_date=check_date
        sum_of_balance=0
        wanted_partner=partner
        #print self.matching_date,'in onchange..................',self.partner_name.id
        if partner and matching_date:
            #print'matching_date........................',matching_date

            getObj= self.env['account.move']
            getAllIds = getObj.search([('partner_id','=',wanted_partner)])
            for purchase_line_id in getAllIds :
                wanted_partner_id=getObj.browse([purchase_line_id])
                #print wanted_partner_id.id.amount,'infor.........................',wanted_partner_id.id.date                
                if wanted_partner_id.id.date <= matching_date :
                    sum_of_balance=sum_of_balance+wanted_partner_id.id.amount
                    #print'in if ............................................',sum_of_balance

        #print'.........................??????????'     
        return sum_of_balance
    @api.onchange('partner_name','matching_date')
    def set_actual_balance(self):
        #print'ttttttttttttttttttttttttt',self.partner_name.id
        self.actual_balance=self.get_actual_balance(self.partner_name.id,self.matching_date) 


    @api.model
    def create(self,vals):
        #print 'innn create.......................',vals
        vals['actual_balance'] = self.get_actual_balance(vals['partner_name'],vals['matching_date']) 
        res = super(customers_supliers_balances, self).create(vals)
        return res


    @api.multi
    def write(self,vals):
        print vals,'in write.......................',self.matching_date
        #print'goaa...........................'
        flag=1
        if 'matching_date' not in vals and 'partner_name' not in vals:
            flag=0
        if 'matching_date' not in vals:
            vals['matching_date']=self.matching_date
        if 'partner_name' not in vals:
            vals['partner_name']=self.partner_name.id
        if flag :
            vals['actual_balance'] = self.get_actual_balance(vals['partner_name'],vals['matching_date'])
        res = super(customers_supliers_balances, self).write(vals)
        return res


    @api.multi
    def update_balances(self):
        #print 'in update_balances.......................',self
        #print'goaa...........................',self.actual_balance
        context = self._context
        active_ids = context.get('active_ids')
        for a_id in active_ids:
            customers_supplieres_brw = self.env['customers.suppliers'].browse(a_id)
            #print'matching date.........................',customers_supplieres_brw.actual_balance
            matching_date=customers_supplieres_brw.matching_date
            partner_name=customers_supplieres_brw.partner_name.id
            customers_supplieres_brw.actual_balance = self.get_actual_balance(partner_name,matching_date)
                


