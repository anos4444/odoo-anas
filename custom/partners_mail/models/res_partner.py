# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import date, datetime
from odoo.exceptions import ValidationError
from dateutil.parser import parse


class MailType(models.Model):
    _name = 'mail_type'

    name = fields.Char('Name', required=True)
    subject = fields.Char('Subject', required=True)
    text = fields.Text('Text', required=True)
    model = fields.Many2one('ir.model', required=True)
    external_id = fields.Char('external id', readonly=True, invisible=True, copy=False)
    partner_id = fields.Many2one('res.partner', 'Partner', readonly=True, invisible=True, copy=False)

    @api.multi
    def send_mail_template(self):
        template = self.env.ref('partners_mail.partners_email_template')
        self.env.cr.execute("""update  mail_template set body_html='%s', model='%s',subject='%s',partner_to=%s where id=%s"""%(self.text,self.model.model, self.subject, self.partner_id.id, template.id))
        self.env['mail.template'].browse(template.id).send_mail(int(self.partner_id.id))
        self.env.cr.execute("""update  mail_template set body_html='%s',model='%s',subject='%s',partner_to=%s where id=%s"""%("","res.partner", "hello ${object.name}", False, template.id))


class ResPartner(models.Model):
    _inherit = 'res.partner'
    mail_types = fields.One2many('mail_type', 'partner_id', 'Mail Types')

def get_date(str):
    return datetime.strptime(str, "%Y-%m-%d").date()
