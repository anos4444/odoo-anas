# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class ProductLine(models.Model):
    _name = 'product.line'

    product_id = fields.Many2one('product.product', string='Product')
    location_id = fields.Many2one('product.location', string='Location')
    sequence = fields.Integer('Sequence')
    partner_id = fields.Many2one(related='location_id.partner_id', readonly=True)

    @api.one
    @api.constrains('sequence', 'product_id', 'location_id', 'partner_id')
    def _check_all(self):
        if self.search([('id', '!=', self.id),('product_id', '=', self.product_id.id),('location_id', '=', self.location_id.id),('partner_id', '=', self.partner_id.id)]):
            raise ValidationError(_('product must be uniqe per location'))
        if self.search([('id', '!=', self.id),('sequence', '=', self.sequence),('location_id', '=', self.location_id.id),('partner_id', '=', self.partner_id.id)]):
            raise ValidationError(_('sequence must be uniqe per location'))


class ProductLocation(models.Model):
    _name = 'product.location'

    @api.multi
    def name_get(self):
        result = []
        for inv in self:
            result.append((inv.id, "%s" % (inv.location_id.name or '')))
        return result

    products_ids = fields.One2many('product.line', 'location_id', string='Product')
    location_id = fields.Many2one('stock.warehouse', string='Location')
    partner_id = fields.Many2one('res.partner', string='Partner')

    @api.one
    @api.constrains('location_id', 'partner_id')
    def _check_all(self):
        if self.search([('id', '!=', self.id),('location_id', '=', self.location_id.id),('partner_id', '=', self.partner_id.id)]):
            raise ValidationError(_('location must be uniqe per partner'))
    
class ResPartner(models.Model):
    _inherit = 'res.partner'

    product_location_ids = fields.One2many('product.location', 'partner_id', string='Products')