# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "VE.18.05 Custom Sale",
    'version': '1.1',
    'summary': '',
    'sequence': 1,
    'description': 'Module for customize sale order',
    "author": "IES Team",
    'category': 'IES Addons',
    'website': 'www.ies-it.com',
    'depends': ['base_setup',
                'sale',
                'sale_stock',
                'custom_inventory',
                'custom_account',
                'check_followups',
                'product_tags'
                ],
    'data': [
                'security/security_access_groups.xml',
                'views/sale_config_view.xml',
                'views/sale_order_line_view.xml',
                'views/sale_order_view.xml',
                'wizard/sale_order_confirm.xml',
                'views/partner_view.xml',
                'reports/sale_orders_report.xml',

    ],
    'installable': True,
    'application': True,
}
