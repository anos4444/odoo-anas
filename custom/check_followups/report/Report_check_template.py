# -*- coding: utf-8 -*-
from openerp import api, models,fields
import datetime
from datetime import date, timedelta,datetime
import re


class wizard_Custom_Report(models.AbstractModel):
    _name = 'report.check_followups.check_bank_template3'


    @api.multi
    def render_html(self,docids,data):
        
        report_obj = self.env['check_followups.check_followups'].search([('payment_id', '=', data['id'])])
        rec = self.env['account.payment'].browse(data['id'])

        a=""
        b=""
        import re
        if re.match('[A-Z]',data['Name'])!=None:
            a="text-align: left;"

        else:a="text-align: right;"
        if re.match('[A-Z]', data['Amount_in_text']): #report_obj.payment_id.check_amount_in_words
            b  = "text-align: left;"
        else:
            b="text-align: right;"

        print data
        docargs = {
            'doc': rec,

            'Datex': '' +str(data['datex']),
            'Datey': '' +  str(data['datey']),

            'firstx': '' + str(data['firstx']),
            'firsty': '' + str(data['firsty']),

            'Memox': '' + str(data['memox']),
            'Memoy': '' + str(data['memoy']),

            'Cityx': '' + str(data['cityx']),
            'Cityy': '' + str(data['cityy']),

            'amountx': '' + str(data['amountx']),
            'amounty': '' + str(data['amounty']),

            'amount_textx': '' + str(data['amount_textx']),
            'amount_texty': '' + str(data['amount_texty']),
            'money_text_width': '' + str(data['money_text_width']),
            'money_text_height': '' + str(data['money_text_height']),
            
            'account_holderx': '' + str(data['acc_holderx']),
            'account_holdery': '' + str(data['acc_holdery']),
            'account_holder_width': '' + str(data['account_holder_width']),
            
            'name':data['Name'],
            'city':data['city'],
            'Amount_in_text':data['Amount_in_text'],
            'memo':data['memo'],
            'data': data,
            'date': data['date'],
            'amount': rec.amount
        }


        return self.env['report'].render('check_followups.check_bank_template3', docargs)
        