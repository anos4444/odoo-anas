# -*- coding: utf-8 -*-
{
    'name': "Check Followup",

    'summary': """
        It Help You To Keep Tracking of Your Checks""",

    'description': """
        This Module Handle The Check Payment And Enable You To Print Your Checks And Keep
        Tracking Your check book .
    """,

    'author': "intellisoft Team",
    'website': "mbrgroupint.com",

    'category': 'Uncategorized',
    'version': '0.1',

     'depends': [
        'base',
        'account',
        'cost_center',
	'custom_account',

    ],

    'data': [
	'data/sequence.xml',
        'security/ir.model.access.csv',
        'wizard/print_check_wizard.xml',
        'wizard/check_replacement_wizard.xml',
        'views/views.xml',
        # 'views/templates.xml',
        'report/check_bank_template.xml',
        'views/bankTamplate.xml',
        'views/company.xml',
        'views/res_partner_view.xml',
        'views/beneficiaries_view.xml',
        # 'report/partner_summary.xml',
        #'report/reports.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}
