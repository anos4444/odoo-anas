# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ResPartners(models.Model):
    _inherit = 'res.partner'

    beneficiaries_ids = fields.One2many('check.beneficiaries', 'partner_ids', 'Associated Beneficiaries')
    #beneficiaries_id =  fields.Many2one('check.beneficiaries')
    benefici_id = fields.One2many('res.partner.line', 'partner_id', 'Associated Beneficiaries')


class ResPartners_line(models.Model):
    _name = 'res.partner.line'

    partner_id = fields.Many2one('res.partner')
    name = fields.Many2one("check.beneficiaries")
