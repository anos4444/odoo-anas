# -*- coding: utf-8 -*-

from openerp import fields, models, api


class AccountStatementReport(models.TransientModel):


    @api.model
    def _get_check_number(self):
        return self.env['account.payment'].browse(self._context['active_id']).Check_no

    @api.model
    def _get_check_name(self):
        return self.env['account.payment'].browse(self._context['active_id']).partner_id.name

    def _default_domain_beneficiary_template_id(self):
        partner_id = self.env['account.payment'].browse(self._context.get('active_id')).partner_id.id
        return [('partner_id', '=', partner_id)]

    @api.model
    def _get_memo(self):
        return self.env['account.payment'].browse(self._context['active_id']).check_memo

    _name = 'payment.check_reports'
    check_no = fields.Integer("Check No", required=True, default=_get_check_number)
    reprint_flag = fields.Boolean(default=False)
    Account_Holder_Name = fields.Char(default=_get_check_name)
    beneficiary_id = fields.Many2one('check.beneficiaries', string="Beneficiary",
                                    domain=lambda self: self._default_domain_beneficiary_template_id())
    Amount_in_word = fields.Char(compute='_get_amount_in_text')
    memo = fields.Char(default=_get_memo)
    amount_lang = fields.Selection([('Ar', 'Arabic'), ('En', 'English')], string='Amount Language', default='Ar')
    template = fields.Many2one('res.bank', string="Template", required=True)

    @api.depends('amount_lang')
    def _get_amount_in_text(self):
        from ..models.money_to_text_ar import amount_to_text_arabic
        if self.amount_lang == 'Ar':
            self.Amount_in_word = amount_to_text_arabic(
                self.env['account.payment'].browse(self._context['active_id']).amount,
                self.env['account.payment'].browse(self._context['active_id']).currency_id.name)
        else:
            from ..models.money_to_text_en import amount_to_text
            self.Amount_in_word = amount_to_text(self.env['account.payment'].browse(self._context['active_id']).amount,
                                                 self.env['account.payment'].browse(
                                                     self._context['active_id']).currency_id.name)

    @api.onchange('beneficiary_id')
    def onchange_beneficiary_id(self):
        if self.beneficiary_id:
            self.Account_Holder_Name = self.beneficiary_id.name

    @api.multi
    def print_check_write(self):

        return self.print_()



    @api.multi
    def print_(self):
        print self.template.acc_holderx,'ppppppppppppppppppppppppppppppppppppppppppppppppppp'
        data=[]
        if not self.reprint_flag:
            rec = self.env['account.payment'].browse(self._context['active_id'])
            rec.Check_no = self.check_no
            rec.journal_id.sudo().write({'Check_no' : self.check_no})
            self.reprint_flag = True
            # rec.state = 'sent'
            data ={
                'id': self._context['active_id'],
                'Name': self.Account_Holder_Name,
                'Amount_in_text': self.Amount_in_word,
                'memo': self.memo,
                'amount_textx' : self.template.amount_textx,
                'amount_texty' : self.template.amount_texty,
                'acc_holderx' : self.template.acc_holderx,
                'acc_holdery' : self.template.acc_holdery,
                'datex' : self.template.datex,
                'datey' : self.template.datey,
                'firstx' : self.template.firstx,
                'firsty' : self.template.firsty,
                'memox' : self.template.memox,
                'memoy' : self.template.memoy,
                'cityx' : self.template.cityx,
                'cityy' : self.template.cityy,
                'amountx' : self.template.amountx,
                'amounty' : self.template.amounty,
                'rec': rec.id,
            }

        #return self.env['report'].get_action(self,'check_followups.check_bank_template1',data=data)
        return self.env['report'].get_action(self,'check_followups.check_bank_template3',data=data)
