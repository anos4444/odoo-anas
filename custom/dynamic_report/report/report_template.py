# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from operator import itemgetter
import ast


class all_reports(models.AbstractModel):
    _name = 'report.dynamic_report.report_template_custom'

    @api.model
    def render_html(self, docids, data=None):
        name = data['form']['name']
        domain = data['form']['domain']
        model = data['form']['model']
        fields_ids = data['form']['fields_ids']
        lang = self.env.context.get('lang',False)
        context = self.env.context

        fields_obj = self.env['ir.model.fields']
        translation_obj = self.env['ir.translation']
        model_obj = self.env['ir.model']

        if (domain == '') or (not domain):
            domain = '[]'
        domain = ast.literal_eval(domain)
        model = model[0]
        model = model_obj.browse(model).model
        model_obj = self.env[model]
        

        fields = []
        fields_dis = []
        selection_fields = []
        fields_vals = fields_obj.read(fields_ids, ['name','field_description'])

        for field in fields_obj.browse(fields_ids):
            fields.append(str(field.name))
            f_dis = str(field.field_description.encode('utf-8'))
            
            if lang:
                translation_ids = translation_obj.search([('src','=', f_dis), ('lang', '=', lang)])
                translation_recs = translation_obj.read(translation_ids, [])
                f_dis = translation_recs and translation_recs[0] or f_dis

            fields_dis.append(f_dis)

            if field.ttype == 'selection':
                selection_fields.append(str(field.name))

        data = model_obj.search(domain)
        

        new_lines = []
        for l1 in data:
            line = []
            data1 = l1.read(fields)
            l = data1[0]
            for f in fields:
                if type(l[f]) in [list, set, tuple]:
                    l[f] = l[f][1]
                if f in selection_fields:
                    output = l[f]
                    if lang and lang != 'en_US':
                        translation_ids = translation_obj.search([('src','=', output), ('lang', '=', lang)])
                        translation_recs = translation_obj.read(translation_ids, [])
                        output = translation_recs and translation_recs[0] or output
                    l[f] = output
                line.append(l[f])
            if lang and lang == 'ar_SY':
               line.reverse()
            new_lines.append(line)


        if lang and lang == 'ar_SY':
            fields_dis.reverse()

        docargs = {
            'doc_ids': self.ids,
            'time': time,
            'header': [fields_dis],
            'report_name': name, 
            'all_data': new_lines,
        }
        return self.env['report'].render('dynamic_report.report_template_custom', docargs)

