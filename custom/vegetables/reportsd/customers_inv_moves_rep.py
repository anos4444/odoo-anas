# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError

class Customers_Inv_Moves_Rep(models.AbstractModel):
    _name = 'report.vegetables.customers_inv_moves_rep'

    @api.model
    def render_html(self, docids, data=None):
        docs = self.env['account.invoice.line'].browse(docids)
        docargs = {
            'doc_ids': docids,
            'doc_model': 'account.invoice.line',
            'docs': docs,
        }

        return self.env['report'].render('vegetables.customers_inv_moves_rep', docargs)
