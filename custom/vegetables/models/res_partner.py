# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from lxml import etree


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(ResPartner, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        user_ids = []
        emp_ids= self.env['hr.employee'].search([('is_sales_man', '=', True)])
        for emp in emp_ids:
            if emp.user_id and emp.user_id.id not in user_ids:
                user_ids.append(emp.user_id.id)
            else:
                continue
            
            if view_type =='form':
                doc = etree.XML(res['arch'])
                for node in doc.xpath("//field[@name='user_id']"):
                    domain = [('id', 'in', user_ids)]
                    node.set('domain', str(domain))
                    res['arch'] = etree.tostring(doc)
        return res

    @api.one
    @api.constrains('name')
    def _check_partner_name(self):
        for rec in self:
            if rec.supplier:
                if len(self.env['res.partner'].search([('name', '=', rec.name), ('supplier', '=', True)])) > 1:
                    raise UserError(_('Vendor name must be unique...!'))

            elif rec.customer:
                if len(self.env['res.partner'].search([('name', '=', rec.name), ('customer', '=', True)])) > 1:
                    raise UserError(_('Customer name must be unique...!'))

    @api.one
    @api.depends('vendors_invoice_line_ids')
    def _get_vendor_total_amount(self):
        for rec in self:
            sum = 0.0
            self.env.cr.execute(
                "select sum(total_price) as sum from vendors_invoice_line where customer_id=" + str(rec.id))
            sum = self.env.cr.dictfetchall()
            sum = sum[0]['sum']

            if not sum:
                sum = 0
            # for inv in rec.vendors_invoice_line_ids:
            #    sum += inv.total_price

            rec.vendor_total_amount = round(sum, 2)

    @api.one
    @api.depends('customer_account_invoice_ids')
    def _get_customer_total_amount(self):
        for rec in self:
            sum = 0.0
            for inv in rec.customer_account_invoice_ids:
                if inv.type == 'out_invoice':
                    sum += inv.amount_total
                if inv.type == 'out_refund':
                    sum -= inv.amount_total
            rec.customer_total_amount = round(sum, 2)

    @api.one
    @api.depends('vendors_invoice_line_ids', 'customer_account_invoice_ids')
    def _get_difference(self):
        for rec in self:
            rec.difference = abs(rec.vendor_total_amount - rec.customer_total_amount)

    code = fields.Char('Code', copy=False, required=True)
    first_group = fields.Many2one('res.partner.groups', string='First Group')
    second_group = fields.Many2one('res.partner.groups', string='Second Group')
    third_group = fields.Many2one('res.partner.groups', string='Third Group')
    forth_group = fields.Many2one('res.partner.groups', string='Forth Group')
    fifth_group = fields.Many2one('res.partner.groups', string='Group')
    currency_id = fields.Many2one(default=lambda self: self.env.user.company_id.currency_id.id)
    active_credit_limit = fields.Boolean(string='Activate Credit Limit', default=True)
    credit_limit = fields.Monetary(string='Credit Limit', default=5000)
    vendor_invoice_sequence = fields.Integer('Vendor Sequence', copy=False)
    is_market= fields.Boolean(string='Agricultural Market', default=True)
    is_vegetable= fields.Boolean(string='Is Vegetable')

    # Customer filed
    expense_account = fields.Many2one('account.account', string='Expense Account', domain=[])
    expense_partner = fields.Many2one('res.partner', string='Expense Partner')

    discount_account = fields.Many2one('account.account', string='Discount Account',
                                       domain=['|', ('user_type_id.name', '=', 'Expenses'),
                                               ('user_type_id.name', '=', 'المصروفات')])
    property_account_receivable_id = fields.Many2one(required=False,
                                                     domain=['|', '|', ('user_type_id.name', '=', 'Bank and Cash'),
                                                             ('user_type_id.name', '=', 'النقدية و البنك'), '|',
                                                             ('user_type_id.name', '=', 'Receivable'),
                                                             ('user_type_id.name', '=', 'الدائنون')])

    # supplier fields
    supplier_commission = fields.Float(string='Commission')
    property_account_payable_id = fields.Many2one(required=False, domain=[])
    # domain=['|', '|',('user_type_id.name', '=', 'Bank and Cash'),
    #         ('user_type_id.name', '=', 'النقدية و البنك'), '|',
    #         ('user_type_id.name', '=', 'Payable'),
    #         ('user_type_id.name', '=', 'المدفوعات')])

    department_ids = fields.One2many('supplier.department', 'partner_id')
    has_taxes = fields.Boolean('Has Taxes')
    has_taxes_vendor = fields.Boolean('Has Taxes Vendor')
    tax_number = fields.Char('Tax Number')
    is_cash = fields.Boolean('Is Cash')
    vendors_invoice_line_ids = fields.One2many('vendors.invoice.line', 'customer_id')
    customer_account_invoice_ids = fields.One2many('account.invoice', 'partner_id')
    vendor_total_amount = fields.Float(string="Vendor Total Amount", compute='_get_vendor_total_amount', store=True)
    customer_total_amount = fields.Float(string="Customer Total Amount", compute='_get_customer_total_amount',
                                         store=True)
    difference = fields.Float(string="Difference", compute='_get_difference')
    department_id = fields.Many2one('stock.warehouse', string='Department')
    sales_man_id = fields.Many2one('hr.employee', string='Sales Man',
                                   domain=[('is_sales_man', '=', True)])

    @api.one
    @api.constrains('code')
    def _check_partner_code(self):
        for rec in self:
            if rec.customer and rec.code:
                customers = self.env['res.partner'].sudo().search([('customer', '=', True), ('code', '=', rec.code)])
                if len(customers) > 1:
                    raise ValidationError(_('customer code must be unique'))

            if rec.supplier and rec.code:
                suppliers = self.env['res.partner'].sudo().search([('supplier', '=', True), ('code', '=', rec.code)])

                if len(suppliers) > 1:
                    raise ValidationError(_('supplier code must be unique'))

    @api.one
    @api.constrains('supplier_commission')
    def _check_supplier_commission(self):
        for rec in self:
            if rec.supplier_commission <= 0 and rec.supplier:
                raise ValidationError(_('Please enter commission percentage for vendor'))

    # @api.one
    # @api.onchange('customer')
    # def onchange_customer(self):
    #     result = {}
    #     for rec in self:
    #         if rec.customer:
    #             result['domain'] = {'fifth_group': [('type', '=', 'customer')]}

    #     return result

    # @api.one
    # @api.onchange('supplier')
    # def onchange_supplier(self):
    #     for rec in self:
    #         result = {}
    #         if rec.supplier:
    #             result['domain'] = {'fifth_group': [('type', '=', 'supplier')]}

    #     return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('name', operator, name), ('code', operator, name)]
        partner = self.search(domain + args, limit=limit)
        return partner.name_get()

        # @api.multi
        # def name_get(self):
        #     result = []
        #     for record in self:
        #         if record.code:
        #             result.append((record.id, '[' + record.code + '] ' + record.name))
        #         else:
        #             result.append((record.id, record.name))
        #     return result


class CustomerGroups(models.Model):
    _name = 'res.partner.groups'

    name = fields.Char('Group Name')
    ar_name = fields.Char('Arabic Name')
    parent_group = fields.Many2one('res.partner.groups', string='Parent Group')
    type = fields.Char()


class SupplierDepartmrnts(models.Model):
    _name = 'supplier.department'

    partner_id = fields.Many2one('res.partner', required=True)
    department_id = fields.Many2one('stock.warehouse', string='Department')
    sales_man_id = fields.Many2one('hr.employee', string='Sales Man', domain=[('is_sales_man', '=', True)])
