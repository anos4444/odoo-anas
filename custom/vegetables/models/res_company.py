# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class CompanyData(models.Model):
    _inherit = 'res.company'
    _rec_name = 'seq_number'
    _order = 'seq_number desc'

    name_eng = fields.Char('Company Name By English')
    company_rigist = fields.Char('Company Registry Bu English')
    city_eng = fields.Char('Customer Code')

