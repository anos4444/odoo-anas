# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from lxml import etree
from odoo.osv.orm import setup_modifiers

class AccountMove(models.Model):
    _name = 'account.move'
    _inherit = ['account.move','mail.thread']

    journal_id = fields.Many2one(track_visibility='onchange')
    ref = fields.Char(track_visibility='onchange')
    date = fields.Date(track_visibility='onchange')
    state = fields.Selection(track_visibility='onchange')

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(AccountMove, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
	if view_type =='form':
		if not self.env.user.has_group('vegetables.history_access_group'):
			doc = etree.XML(res['arch'])
			for node in doc.xpath("//field[@name='message_ids']"):
				node.set('invisible', "1")
				setup_modifiers(node, res['fields']['message_ids'])
			for node in doc.xpath("//field[@name='message_follower_ids']"):
				node.set('invisible', "1")
				setup_modifiers(node, res['fields']['message_follower_ids'])
			res['arch'] = etree.tostring(doc)
        return res



class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    sales_man_id = fields.Many2one('hr.employee', string='Sales Man',
                                   domain=[('is_sales_man', '=', True)], readonly=True)


    @api.multi
    def write(self, vals):
	mess_obj= self.env['mail.message']
	a_obj= self.env['account.account']
	e_obj= self.env['hr.employee']
	partner_obj= self.env['res.partner']
        message = _("This Line has been modified: <a href=# data-oe-model=account.move.line data-oe-id=%d>%s</a>") % (self.id, self.id)
        if vals:
		mess_ids= mess_obj.search([('model','=','account.move'),('res_id','=',self.move_id.id),('parent_id','=',False)],limit=1)
		dct = {'body': message, 'model': 'account.move', 'attachment_ids': [], 'res_id': self.move_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False}
		lst = []
		if vals.get('account_id',False):
			nproduct= a_obj.browse(vals.get('account_id',False))
			lst.append([0, 0, {'field': 'account_id', 'field_desc': u'Account', 'field_type': 'many2one', 'new_value_char': nproduct.display_name, 'old_value_char': self.account_id.display_name, 'new_value_integer': vals.get('account_id',False), 'old_value_integer': self.account_id.id}])
		if vals.get('partner_id',False):
			ncustomer= partner_obj.browse(vals.get('partner_id',False))
			lst.append([0, 0, {'field': 'partner_id', 'field_desc': u'Partner', 'field_type': 'many2one', 'new_value_char': ncustomer.display_name, 'old_value_char': self.partner_id.display_name, 'new_value_integer': vals.get('partner_id',False), 'old_value_integer': self.partner_id.id}])
		if vals.get('sales_man_id',False):
			ncustomer= e_obj.browse(vals.get('sales_man_id',False))
			lst.append([0, 0, {'field': 'sales_man_id', 'field_desc': u'Partner', 'field_type': 'many2one', 'new_value_char': ncustomer.name, 'old_value_char': self.sales_man_id.name, 'new_value_integer': vals.get('sales_man_id',False), 'old_value_integer': self.sales_man_id.id}])
		if vals.get('name',False):
			lst.append([0, 0, {'field': 'name', 'field_desc': u'Label', 'field_type': 'char', 'new_value_char': vals.get('name',False), 'old_value_char': self.name}])
		if vals.get('debit',False):
			lst.append([0, 0, {'field': 'debit', 'field_desc': u'Debit', 'field_type': 'monetary', 'new_value_monetary': vals.get('debit',False), 'old_value_monetary': self.debit}])
		if vals.get('credit',False):
			lst.append([0, 0, {'field': 'debit', 'field_desc': u'Credit', 'field_type': 'monetary', 'new_value_monetary': vals.get('credit',False), 'old_value_monetary': self.credit}])
		if lst:
			dct.update({'tracking_value_ids':lst})
			mess_obj.create(dct)
			mess_obj.create({'body': message, 'model': 'account.move', 'attachment_ids': [], 'res_id': self.move_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False})
        res = super(AccountMoveLine, self).write(vals)		        
        return res


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    _rec_name = 'sequence_number'
    _order = 'sequence_number desc'

    email_status = fields.Selection([('sent', 'Sent'), ('not_sent', 'Not Sent')], 'Email Status', default='not_sent')
    sequence_number = fields.Char(default='New')
    partner_code = fields.Char('Customer Code')
    date_invoice = fields.Date('Date', default=fields.Date.today, required=True)
    department_id = fields.Many2one('stock.warehouse', string='Department', readonly=True,
                                    states={'draft': [('readonly', False)]})
    total_discount_amount = fields.Monetary(string='Discount Amount', compute='_get_total_invoice')
    total_percentage_discount_amount = fields.Monetary(string='Discount Percentage Amount',
                                                       compute='_get_total_invoice')
    name = fields.Char(related='sequence_number')
    amount_tax = fields.Monetary(store=False)
    payed_amount = fields.Monetary('Payed Amount', compute='_get_payed_amount', store=False)
    invoice_line_ids = fields.One2many(readonly=False)
    
    #residual = fields.Monetary(string='Amount Due', compute='_compute_amount', store=True, help="Remaining amount due.")

    custody_accounts_ids = fields.One2many('account.invoice.custody', 'invoice_id', string='Custody Accounts Lines',
                                           readonly=True, states={'draft': [('readonly', False)]}, copy=True)
    edited = fields.Boolean(default=False, copy=False)
    
    legacy_amount = fields.Monetary(required=False,default=0)

    version_number = fields.Integer(string="Version Number", store=True, default=1)

    vegetables = fields.Boolean(string="Vegetables", default=False, copy=False)

    #sales_man_id = fields.Many2one('hr.employee', string='Sales Man',
    #                               domain=[('is_sales_man', '=', True)], readonly=True)

    @api.model
    def create(self, vals):
        # modification for adding first payment form custom_account
        if 'legacy_amount' in vals:
            vals['sequence_number'] = self.env['ir.sequence'].next_by_code('account.first.payment_v') or _('_New')

        if 'legacy_amount' not in vals:
            vals['sequence_number'] = self.env['ir.sequence'].next_by_code('customer.invoice') or _('_New')
        # else:
        #  invoice_sequence = int(self.env["res.partner"].sudo().browse(int(vals['partner_id'])).invoice_sequence) + 1
        #  self.env["res.partner"].sudo().browse(int(vals['partner_id'])).invoice_sequence = invoice_sequence
        #  vals['sequence_number'] = invoice_sequence

        result = super(AccountInvoice, self).create(vals)

        return result

    @api.multi
    def write(self, values):
        if values:
            for rec in self:
                record = rec.env["account.invoice"].browse(rec.id)
                # make the record as deited
                if not "edited" in values and record.state != 'draft' and (record.amount_total == record.residual):
                    values['edited'] = True

        return super(AccountInvoice, self).write(values)

    @api.multi
    def unlink(self):
        res = super(AccountInvoice, self).unlink()
        return res

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        for line in self.invoice_line_ids:
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0) - line.discount_amount
            taxes = line.invoice_line_tax_ids.compute_all(price_unit, self.currency_id, line.quantity, line.product_id,
                                                          self.partner_id)['taxes']
            for tax in taxes:
                val = self._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)

                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += val['base']
        return tax_grouped

    @api.onchange('invoice_line_ids')
    def _onchange_invoice_line_ids(self):

        taxes_grouped = self.get_taxes_values()
        tax_lines = self.tax_line_ids.filtered('manual')
        for tax in taxes_grouped.values():
            tax_lines += tax_lines.new(tax)
        self.tax_line_ids = tax_lines

        cus_accounts_grouped = self.get_custody_accounts_values()
        cus_accounts_lines = self.env['account.invoice.custody']
        for line in cus_accounts_grouped.values():
            cus_accounts_lines += cus_accounts_lines.new(line)
        self.custody_accounts_ids = cus_accounts_lines

        return

    @api.onchange('state')
    def _onchange_state(self):

        for line in self.invoice_line_ids:
            line.state = self.state


    @api.onchange('partner_code')
    def onchange_partner_code(self):
        if self.partner_code:
            self.partner_id = []
            customer = self.env['res.partner'].sudo().search([('code', '=', self.partner_code), ('customer', '=', True)])
            if customer:
                self.partner_id = customer.id

    @api.model
    def _default_picking_type(self):
        type_obj = self.env['stock.picking.type']
        company_id = self.env.context.get('company_id') or self.env.user.company_id.id
        types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id.company_id', '=', company_id)])
        if not types:
            types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id', '=', False)])
        return types[:1]

    @api.depends('invoice_line_ids', 'invoice_line_ids.discount_amount', 'invoice_line_ids.discount_percentage_total')
    def _get_total_invoice(self):
        for rec in self:
            total = 0
            discount_amount = 0
            discount_percentage_amount = 0
            net_total = 0
            for line in rec.invoice_line_ids:
                total += line.price_unit * line.executed_quantity

                discount_amount += line.discount_amount
                discount_percentage_amount += line.discount_percentage_total
                net_total += line.price_subtotal

            rec.total_discount_amount = discount_amount
            rec.total_percentage_discount_amount = discount_percentage_amount

            rec.amount_untaxed = total - (rec.total_discount_amount + rec.total_percentage_discount_amount)
            rec.amount_total = net_total

    @api.depends('payed_amount', 'residual', 'residual')
    def _get_payed_amount(self):
        for rec in self:
            rec.payed_amount = rec.amount_total - rec.residual

    @api.multi
    @api.onchange('partner_id', 'date_invoice', 'department_id')
    def get_customer_invoice(self):
        if self.partner_id:
            self.partner_code = self.partner_id.code

        if self.partner_id and self.date_invoice:

            self.invoice_line_ids = []

            customer_invoice_lines = self.env['account.invoice.line']
            vendors_lines = self.env['vendors.invoice.line'].search(
                [('customer_id', '=', self.partner_id.id),
                 ('executed_line', '=', False),
                 ('state', '=', 'posted')])

            for line in vendors_lines:

                if line.vendor_invoice_id.invoice_date == self.date_invoice and True if not self.department_id else \
                                        line.vendor_invoice_id.department_id == self.department_id and line.vendor_invoice_id.invoice_date == self.date_invoice:
                    customer_invoice_lines |= customer_invoice_lines.new(
                        {
                            'invoice_id': self.id,
                            'vendor_invoice_id': line.vendor_invoice_id.id,
                            'vendor_invoice_line_id': line.id,
                            'product_id': line.product_id.id,
                            'uom_id': line.uom_id.id,
                            'currency_id': line.currency_id.id,
                            'quantity': line.product_qty - line.executed_quantity,
                            'executed_quantity': line.product_qty - line.executed_quantity,
                            'price_unit': line.unit_price,
                            'invoice_line_tax_ids': [(4, line.product_id.taxes_id.id)] if line.product_id.taxes_id.id and line.customer_id.has_taxes else False,
                            'total_after_discount': line.total_price,
                            'executed_line': line.executed_line,
                            'driver_name': line.vendor_invoice_id.driver_name,
                            'postal_number': line.vendor_invoice_id.postal_number,
                            'transmission_number': line.vendor_invoice_id.transmission_number,
                            'transmission_date': line.vendor_invoice_id.transmission_date,
                            'sales_man_id': line.vendor_invoice_id.sales_man_id.id,
                            'account_id': line.vendor_invoice_id.sales_man_id.custody_account.id,
                            'department_id': line.vendor_invoice_id.department_id.id
                        })
            self.get_custody_accounts_values()


    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(_('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            date_invoice = inv.date_invoice
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, iml)
            name = inv.name or '/'
            if inv.vegetables: 
                name += _(' : فاتورة رقم ')
            if inv.payment_term_id:
                totlines = \
                    inv.with_context(ctx).payment_term_id.with_context(currency_id=company_currency.id).compute(total,
                                                                                                                date_invoice)[
                        0]
                res_amount_currency = total_currency
                ctx['date'] = date_invoice
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency
                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            else:
                # iml.append({
                #     'type': 'dest',
                #     'name': name,
                #     'price': total,
                #     'account_id': inv.account_id.id,
                #     'date_maturity': inv.date_due,
                #     'amount_currency': diff_currency and total_currency,
                #     'currency_id': diff_currency and inv.currency_id.id,
                #     'invoice_id': inv.id
                # })

                # Make Customer account and discount in Debit site
                iml.append({
                    'type': 'dest',
                    'name': str(name)+_(' : فاتورة مبيعات رقم '),
                    'price': total - (inv.total_percentage_discount_amount + inv.total_discount_amount),
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })
                if inv.total_percentage_discount_amount + inv.total_discount_amount > 0:
                    iml.append({
                        'type': 'dest',
                        'name': _("Discount"),
                        'price': inv.total_percentage_discount_amount + inv.total_discount_amount,
                        'account_id': inv.partner_id.discount_account.id,
                        'date_maturity': inv.date_due,
                        'amount_currency': diff_currency and total_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
            line = [(0, 0, self.line_get_convert(l, part.id)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)
            date = inv.date or date_invoice



            


            move_vals = {
                'ref': inv.sequence_number,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
        return True

    @api.multi
    def action_invoice_open(self):
        # lots of duplicate calls to action_invoice_open, so we remove those already open
        to_open_invoices = self.filtered(lambda inv: inv.state != 'open')
        if to_open_invoices.filtered(lambda inv: inv.state not in ['proforma2', 'draft']):
            raise UserError(_("Invoice must be in draft or Pro-forma state in order to validate it."))
        to_open_invoices.action_date_assign()
        to_open_invoices.action_move_create()
        to_open_invoices.invoice_validate()
        if self.state != 'draft':
            for vendor_line in self.invoice_line_ids:
                if vendor_line.vendor_invoice_line_id.executed_line:
                    vendor_line.vendor_invoice_line_id.executed_line = True

    '''@api.model
    def invoice_line_move_line_get(self):
        res = []
        tax_ids = []
        for line in self.invoice_line_ids:
            for tax in line.invoice_line_tax_ids:
                tax_ids.append((4, tax.id, None))
                for child in tax.children_tax_ids:
                    if child.type_tax_use != 'none':
                        tax_ids.append((4, child.id, None))
        for line in self.custody_accounts_ids:

            move_line_dict = {

                'name': line.account_id.name,
                'price': line.amount,
                'account_id': line.account_id.id,
                'invoice_id': self.id,
                'tax_ids': tax_ids,
            }
            # if line['account_analytic_id']:
            #     move_line_dict['analytic_line_ids'] = [(0, 0, line._get_analytic_line())]
            res.append(move_line_dict)


        return res'''

    @api.multi
    def get_custody_accounts_values(self):
        account_grouped = {}
        for line in self.invoice_line_ids:
            if line.account_id.id not in account_grouped:
                account_grouped[line.account_id.id] = {'account_id': line.account_id.id,
                                                       'amount': line.quantity * line.price_unit}
            else:
                account_grouped[line.account_id.id]['amount'] += line.quantity * line.price_unit

        return account_grouped

    @api.multi
    def update_info(self):
        for rec in self:
            old_move_id = rec.move_id
            rec.move_id = False
            old_move_id.button_cancel()
            old_move_id.unlink()

            for invoice_line in rec.sudo().invoice_line_ids:
                if rec.partner_id.has_taxes:
                    invoice_line.invoice_line_tax_ids = invoice_line.product_id.taxes_id[0]
                else:
                    invoice_line.invoice_line_tax_ids = [(5,)]

            rec._onchange_invoice_line_ids()
            
            rec.sudo().action_date_assign()
            rec.sudo().action_move_create()
            rec.sudo().invoice_validate()

            '''for invoice_line in rec.invoice_line_ids:
                if rec.partner_id.has_taxes:
                    invoice_line.invoice_line_tax_ids = invoice_line.product_id.taxes_id[0]
                else:
                    invoice_line.invoice_line_tax_ids = [(5,)]

            rec._onchange_invoice_line_ids()

            # Generate Account Move Line
            move_line = []
            total = 0
            taxes_total = 0
            # Get Salesman Custody accounts
            iml = rec.invoice_line_move_line_get()
            for line in iml:
                total += line['price']
                move_line.append((0, 0, {
                    'partner_id': rec.partner_id.id,
                    'account_id': line['account_id'],
                    'name': line['name'] + _(' فاتوة مبيعات رقم '),
                    'credit': line['price']
                }))

            # Get taxes Accounts
            iml = rec.tax_line_move_line_get()
            for line in iml:
                taxes_total += line['price_unit']
                move_line.append((0, 0, {
                    'account_id': line['account_id'],
                    'partner_id': rec.partner_id.id,
                    'name': _("VAT out ")+line['name'],
                    'credit': line['price_unit']
                }))
            # if there is discount add't as debit
            if rec.total_percentage_discount_amount + rec.total_discount_amount > 0:
                move_line.append((0, 0, {
                    'partner_id': rec.partner_id.id,
                    'account_id': rec.partner_id.discount_account.id,
                    'name':  _("Discount"),
                    'debit': rec.total_percentage_discount_amount + rec.total_discount_amount
                }))
            # Add customer receivable account as debit
            move_line.append((0, 0, {
                'partner_id': rec.partner_id.id,
                'account_id': rec.partner_id.property_account_receivable_id.id,
                'name': str(rec.sequence_number or '')+_(' فاتوة مبيعات رقم '),
                'debit': rec.amount_total + taxes_total
            }))

            # Update Account move with new update
            # rec.move_id.line_ids.unlink()#delelte 
            #rec.move_id.line_ids.invoice_id.unlink()
            # rec.move_id.update({'line_ids': move_line})

            old_move_id = rec.move_id
            rec.move_id = False
            #rec.account_move_id.line_ids.unlink()
            #rec.account_move_id.update({'line_ids': move_line})
            old_move_id.button_cancel()
            old_move_id.unlink()
            ctx = dict(self._context, lang=rec.partner_id.lang)
            journal_id = rec.journal_id.with_context(ctx)
            move_id = rec.env['account.move'].sudo().create({
                    'date': rec.date_invoice,
                    'journal_id': journal_id.id,
                    'name': '/',
                    'ref': rec.sequence_number,
                    'state': 'draft',
                    'line_ids': move_line,
                    'narration': rec.comment,
                })

                # post journal entry
            move_id.post()
            rec.write({'move_id': move_id.id})


            
            rec.version_number=rec.version_number+1'''
            rec.write({'edited': False})
            
                        
class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    state = fields.Selection([
        ('draft', 'Draft'),
        ('proforma', 'Pro-forma'),
        ('proforma2', 'Pro-forma'),
        ('open', 'Open'),
        ('paid', 'Paid'),
        ('cancel', 'Cancelled'),
    ], string='Status', default='draft', copy=False)

    name = fields.Char(required=False, related='product_id.name')
    tax_amount = fields.Float('Tax Amount', compute='_compute_price')
    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Invoice Reference',
                                        ondelete='cascade', index=True, copy=False, readonly=True)

    vendor_invoice_line_id = fields.Many2one('vendors.invoice.line', string='line Reference',
                                             ondelete='cascade', index=True, copy=False)

    invoice_id = fields.Many2one('account.invoice', ondelete='cascade', index=True, copy=False)

    discount_amount = fields.Monetary('Dis Amount', readonly=False)
    discount_percentage_total = fields.Float(string='Dis Percentage Total', readonly=False)
    executed_quantity = fields.Float("Executed Quantity")
    executed_line = fields.Boolean("executed", default=False)
    department_id = fields.Many2one('stock.warehouse', string='Department', readonly=True)
    driver_name = fields.Char('Driver')
    postal_number = fields.Char('Signal Number')
    transmission_number = fields.Char('Transmission Number', readonly=True)
    transmission_date = fields.Date(string='Transmission Date', readonly=True)
    sales_man_id = fields.Many2one('hr.employee', string='Sales Man',
                                   domain=[('is_sales_man', '=', True)], readonly=True)

    received_product = fields.Boolean("received product", default=False)
    note = fields.Char('Note')

    vendor_id = fields.Many2one(related='vendor_invoice_id.vendor_id')

    # @api.multi
    # def unlink(self, update_info=None):
    #     if self.filtered(lambda r: r.invoice_id and r.invoice_id.state != 'draft'):
    #         if not update_info:
    #             raise UserError(_('You can only delete an invoice line if the invoice is in draft state.'))
    #     return super(AccountInvoiceLine, self).unlink()


    @api.multi
    def unlink(self):
        for rec in self:
            rec.invoice_id.write({'state': 'draft'})
            #rec.invoice_id.unlink()
        return super(AccountInvoiceLine, self).unlink()

    @api.onchange('product_id')
    def _onchange_product_id(self):
        return {}

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity', 'discount_amount',
                 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
                 'invoice_id.date_invoice', 'discount_percentage_total')
    def _compute_price(self):

        if self.discount > 0:
            self.discount_percentage_total = self.price_unit * (self.discount / 100.0)
        else:
            self.discount_percentage_total = 0.0

        currency = self.invoice_id and self.invoice_id.currency_id or None
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0) - self.discount_amount

        taxes = False
        if self.invoice_line_tax_ids:
            taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id,
                                                          partner=self.invoice_id.partner_id)
            self.tax_amount = taxes['taxes'][0]['amount']
        self.price_subtotal = price_subtotal_signed = taxes['total_excluded'] if taxes else self.quantity * price
        if self.invoice_id.currency_id and self.invoice_id.company_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
            price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id.date_invoice).compute(
                price_subtotal_signed, self.invoice_id.company_id.currency_id)
        sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
        self.price_subtotal_signed = price_subtotal_signed * sign


class AccountInvoiceCustody(models.Model):
    _name = "account.invoice.custody"
    _description = "Invoice Salesman Custody"

    invoice_id = fields.Many2one('account.invoice', string='Invoice', ondelete='cascade', index=True)

    account_id = fields.Many2one('account.account', string='Custody Account')

    amount = fields.Monetary()

    currency_id = fields.Many2one('res.currency', related='invoice_id.currency_id', store=True, readonly=True)
