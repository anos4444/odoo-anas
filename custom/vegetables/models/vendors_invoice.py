# -*- coding: utf-8 -*-

#import pymsgbox
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError, Warning
from dateutil.parser import parse
from lxml import etree
from odoo.osv.orm import setup_modifiers
import datetime



class ResPartner(models.Model):
    _inherit = 'res.partner'

    invoice_sequence = fields.Integer('Invoice Sequence')

    partner_type_id = fields.Many2many('company.types', 'partner_type_rel', 'partner_id' , 'type_id' , 'Type',store=True, required=True)
    partner_type_bk = fields.Selection([('vegetables','Vegetables'),('market','Agricultural Market')],string='Partner Type',store=True,compute='get_bk_partnr_type')

    @api.one
    @api.depends('partner_type_id')
    def get_bk_partnr_type(self):
        for tyy in self.partner_type_id:
            if tyy.name == 'Agricultural Market':
                self.partner_type_bk = 'market'
                self.write({'partner_type_bk':'market'})
        return self.partner_type_bk

class VendorsInvoice(models.Model):
    _name = 'vendors.invoice'
    _inherit = ['mail.thread']
    _description = "Vendor Invoice"
    _order = 'invoice_date desc , id desc'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(VendorsInvoice, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
	if view_type =='form':
		if not self.env.user.has_group('vegetables.history_access_group'):
			doc = etree.XML(res['arch'])
			for node in doc.xpath("//field[@name='message_ids']"):
				node.set('invisible', "1")
				setup_modifiers(node, res['fields']['message_ids'])
			for node in doc.xpath("//field[@name='message_follower_ids']"):
				node.set('invisible', "1")
				setup_modifiers(node, res['fields']['message_follower_ids'])
			res['arch'] = etree.tostring(doc)
        return res


    def _get_custoudies_data(self):
        custodies = self.env['vegetables.custody'].search([], order='sequence')
        custody_lines = self.env['vendors.custody.line']
        for custody in custodies:
            custody_lines |= custody_lines.new({
                'vendor_invoice_id': self.id,
                'custody_name': custody.custody_name,
                'account': custody.credit_account.id,
                'custody_type': custody.custody_type,
                'sales_man': custody.sales_man,
                'is_rental': custody.is_rental,
                'is_complex': custody.is_complex,
                'is_card': custody.is_card,
                'partner_id': custody.partner_id,
            })
        return custody_lines

    @api.onchange('transmission_number')
    def _onchange_transmission_number(self):
        if self.transmission_number and self.transmission_number.isdigit():
            self.transmission_number = int(self.transmission_number)
	self.manual_invoice_number = False




    @api.constrains('transmission_number','sales_man_id')
    def _check_transmission_number(self):

        if self.transmission_number :
            if self.transmission_number and ( not self.transmission_number.isdigit() ) : 
                raise ValidationError(
                _('Invoice number should only contains numbers'))

            invoice_numbers = self.env['manual.invoice.number'].sudo().search([('type', '=', 'vendor_invoice')])

            '''if not invoice_numbers :
                raise ValidationError(
                _('Invalid invoice number, this number not found in vendor invoice numbers range.'))'''


            if self.transmission_number and invoice_numbers:
                invoices = self.env['vendors.invoice'].sudo().search([])

                manual_invoice_ids2 = []
                manual_invoice_ids = []
                vendor_invoice_ids= []
                invoice_number_found = False



                for invoice in invoice_numbers:

                    if invoice.range_from <= int(self.transmission_number) <= invoice.range_to:
                        invoice_number_found = True
                        manual_invoice_ids2.append(invoice)

                if not invoice_number_found:
                    raise ValidationError(
                        _('Invalid invoice number, this number not found in vendor invoice numbers range.'))
                if invoice_number_found :
                    for_all_seles_man = False
                    for manual in manual_invoice_ids2 :
                    	if manual.for_all_delegates == True :
                    		for_all_seles_man = True
                        	manual_invoice_ids.append(manual)
                    	if manual.delegate_id == self.sales_man_id :
                    		for_all_seles_man = True 
                        	manual_invoice_ids.append(manual)
                    if for_all_seles_man == False :
                            	raise ValidationError(
                            _('Invalid invoice number, this number not found in vendor invoice numbers range.'))

                    if for_all_seles_man == True :
                	
                        numper_found = False
                        for invoice in invoices :
                            if int(invoice.transmission_number) == int(self.transmission_number): 
                                vendor_invoice_ids.append(invoice)
                                numper_found = True 

                        if not numper_found : 
                            self.manual_invoice_number = manual_invoice_ids[0].id

                        if numper_found :
                                selected_manual =manual_invoice_ids[0]
                        true_selected_manual = True
                        for manual in manual_invoice_ids :
                            true_selected_manual = True
                            for invoice in vendor_invoice_ids :
                                if invoice.manual_invoice_number == manual :
                                    true_selected_manual = False
                            if true_selected_manual == True :
                                self.manual_invoice_number = manual.id 
                                break 
                        if true_selected_manual == False:
                            raise ValidationError(_('This number is reserved in another vendor invoice..!'))









    # @api.depends('invoice_line', 'custody_lines', 'invoice_line.purchase_price')
    @api.depends('invoice_line', 'custody_total', 'invoice_line.purchase_price', 'commision','amount_total','purchase_taxes_total')
    def _get_total_invoice(self):
        for rec in self:
            #print "MMMMMMMM nnnnnn", rec.name
            sum_purchase_taxes = 0
            vb = 0
       
            total = sum_custody = total_tax = customer_total = customer_total_untaxed = 0
            for line in rec.invoice_line:
                vb = vb+(line.purchase_price*line.product_qty)
                total += line.total_price
                total_tax += line.taxes_amount
                if rec.sales_man_id.linked_customer.id == line.customer_id.id:
                    customer_total += line.total_price
                    customer_total_untaxed += line.product_qty * line.unit_price
                if line.vendor_invoice_id.vendor_id.has_taxes_vendor:
                    sum_purchase_taxes += line.purchase_taxes

            '''for custody_line in rec.custody_lines:
                sum_custody += custody_line.credit_amount + custody_line.cash_amount
            for line in rec.invoice_line:
                if line.vendor_invoice_id.vendor_id.has_taxes:
                    sum_purchase_taxes += line.purchase_taxes'''
            rec.purchase_taxes_total = sum_purchase_taxes
            rec.untaxes_total = total - total_tax
            # rec.amount_total = rec.untaxes_total - (rec.untaxes_total * rec.commision / 100) - sum_custody
            rec.amount_total = vb
            #rec.net_total = rec.amount_total+rec.purchase_taxes_total
            #rec.net_total = (vb+sum_purchase_taxes)-rec.vat_commission_amount
            rec.net_total = (vb+sum_purchase_taxes) - rec.custody_total
            rec.total_taxed_amount = total_tax
            #rec.net_total = rec.net_total - rec.custody_total
            rec.main_customer_total_amount = customer_total
            rec.main_customer_total_untaxed = customer_total_untaxed
            rec.credit_sales = (total_tax + (total - total_tax)) - customer_total

    @api.depends('vendor_id','custom_commision')
    def _get_salesman_commission(self):
        for rec in self:
            if rec.vendor_id:
                if not rec.allow_custom_commision:
                    rec.commision = rec.vendor_id.supplier_commission
                if rec.allow_custom_commision:
                    rec.commision = rec.custom_commision



    @api.depends('untaxes_total', 'commision')
    def _get_commision_total_amount(self):
        for rec in self:
            rec.commision_total_amount = rec.untaxes_total * rec.commision / 100

    # @api.depends('untaxes_total', 'main_customer_total_amount', 'commision_total_amount', 'custody_lines',
    #             'total_taxed_amount')
    @api.depends('custody_lines')
    def _get_custody_and_credit_invoice_total(self):
        for rec in self:
            cash_custody = 0
            credit_custody = 0
            # rec.credit_sales = (rec.total_taxed_amount + rec.untaxes_total) - rec.main_customer_total_amount
            for custody in rec.custody_lines:
                if custody.custody_type == 'is_cash':
                    cash_custody += custody.amount
                else:
                    credit_custody += custody.amount

            rec.cash_custody = cash_custody
            rec.credit_custody = credit_custody
            rec.custody_total = rec.cash_custody + rec.credit_custody

    @api.depends('vendor_id')
    def _get_vendor_sequence(self):
        for rec in self:
            if rec.vendor_id:
                all_vendor_invoices = self.search_count(
                    [('vendor_id', '=', rec.vendor_id.id)])#, order="invoice_date")

                if all_vendor_invoices:
                    #max_seq = max([x.vendor_sequence for x in all_vendor_invoices])
                    rec.vendor_sequence = all_vendor_invoices + 1
                else:
                    rec.vendor_sequence = 1

    def update_vendor_sequence(self):
        for rec in self:
            all_vendor_invoices = self.search(
                [('vendor_id', '=', rec.vendor_id.id)], order="invoice_date")
            counter = 1
            for vin_inv in all_vendor_invoices:
                vin_inv.write({'vendor_sequence': counter, 'edited': False})
                counter += 1

    @api.one
    @api.depends('custom_commision', 'commision_total_amount')
    def _get_vat_commission_amount(self):
        if self.commission_vat_id and self.commision_total_amount:
            amount = (self.commission_vat_id.amount * self.commision_total_amount) / 100
            self.vat_commission_amount = amount

    @api.multi
    def _get_default_tax(self):
        res = self.env['account.tax'].search([('type_tax_use', '=', 'purchase'),('name', '=', 'VAT15%')], limit=1)
        if res:
            return res.id

    executed_lines = fields.Integer(string='Executed Lines', default=0)
    name = fields.Char(string='Invoice No.', required=True, copy=False, readonly=True, index=True,
                       default=lambda self: _('New'))
    invoice_date = fields.Date(string='Invoice Date', required=True, default=fields.Date.today)
    commision = fields.Float(String="Commission", compute='_get_salesman_commission')
    commision_total_amount = fields.Monetary('Commission Amount', compute='_get_commision_total_amount', store=True)
    vendor_id = fields.Many2one('res.partner', string='Vendor Name', domain=['&',('is_market', '=', True),('supplier', '=', True)], track_visibility='onchange')

    vendor_code = fields.Char(string='Vendor Code')

    department_id = fields.Many2one('stock.warehouse', string='Department', required=True,
                                    default=lambda self: self.env.user.partner_id.department_id ,track_visibility='onchange',domain=[('is_market_warehouse', '=', True)])
    sales_man_is_manger = fields.Boolean(related='sales_man_id.is_sales_manger')

    supervisor_id = fields.Many2one('hr.employee', string='Supervisor', domain=[('is_sales_manger', '=', True)])

    sales_man_id = fields.Many2one('hr.employee', string='Sales Man', required=True,
                                   domain=['&',('is_market_employee', '=', True),('is_sales_man', '=', True)],default=lambda self: self.env.user.partner_id.sales_man_id ,track_visibility='onchange')


    driver_name = fields.Char('Driver', track_visibility='onchange')
    postal_number = fields.Char('Postal Number' ,track_visibility='onchange')
    transmission_number = fields.Char('Transmission Number' ,track_visibility='onchange')
    manual_invoice_number = fields.Many2one('manual.invoice.number', 'Manual Invoice Number')
    transmission_date = fields.Date(string='Transmission Date' ,track_visibility='onchange')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id.id)

    amount_total = fields.Monetary(string='Total', compute='_get_total_invoice', store=True ,track_visibility='onchange')
    purchase_taxes_total = fields.Monetary(string='Purchase Tax', compute='_get_total_invoice' ,track_visibility='onchange')
    net_total = fields.Monetary(string='Net Total', store=True, compute='_get_total_invoice' ,track_visibility='onchange')
    total_taxed_amount = fields.Monetary(string='Total Taxes', store=True, compute='_get_total_invoice' ,track_visibility='onchange')
    untaxes_total = fields.Monetary('Untaxed Total', store=True, compute='_get_total_invoice' ,track_visibility='onchange')
    main_customer_total_amount = fields.Monetary('Main Customer Total', compute='_get_total_invoice', store=True ,track_visibility='onchange') 
    main_customer_total_untaxed = fields.Monetary('Main Customer Untaxed', compute='_get_total_invoice', store=True ,track_visibility='onchange')

    state = fields.Selection([('unpost', 'Unpost'), ('posted', 'Posted')], 'Status', default='unpost')

    invoice_line = fields.One2many('vendors.invoice.line', 'vendor_invoice_id', string='Invoice Lines', copy=True ,track_visibility='onchange')

    custody_lines = fields.One2many('vendors.custody.line', 'vendor_invoice_id', string='Custody Lines',
                                    default=_get_custoudies_data, copy=True ,track_visibility='onchange')
    account_move_id = fields.Many2one('account.move', string='Journal Entry', copy=False)

    executed = fields.Boolean("executed", default=False, copy=False)

    edited = fields.Boolean('Modified', default=False, copy=False)

    cash_custody = fields.Float(string="Cash Custody Total", compute='_get_custody_and_credit_invoice_total',
                                store=True)
    credit_custody = fields.Float(string="Cash Custody Total", compute='_get_custody_and_credit_invoice_total',
                                  store=True)
    # credit_sales = fields.Float(string="Cash Custody Total", compute='_get_custody_and_credit_invoice_total', store=True)
    credit_sales = fields.Float(string="Cash Custody Total", compute='_get_total_invoice', store=True)
    custody_total = fields.Monetary(string="Custody Total", compute='_get_custody_and_credit_invoice_total', store=True ,track_visibility='onchange')
    vendor_sequence = fields.Integer(string="Vendor Sequence", store=True, compute='_get_vendor_sequence',track_visibility='onchange')

    version_number = fields.Integer(string="Version Number", store=True, default=1,track_visibility='onchange')
    write_text = fields.Text('Text')

    company_id = fields.Many2one('res.company', 'Company',
        default=lambda self: self.env['res.company']._company_default_get('vendors.invoice'))

    custom_commision = fields.Float(String="Custom Commission", copy=False)

    allow_custom_commision = fields.Boolean("Allow Custom Commission", default=False, copy=False)
    allow_update = fields.Boolean("Allow update", default=False, copy=False)
    vat_commission_amount = fields.Monetary(string='Vat on Commission', compute='_get_vat_commission_amount')
    commission_vat_id = fields.Many2one('account.tax', string="Tax", domain=[('type_tax_use', '=', 'purchase')], default=_get_default_tax)

    
    
    @api.onchange('company_id')
    def onchange_company_id(self):
        if self.company_id:
            self.allow_custom_commision = self.company_id.allow_custom_commision
    
    #@api.onchange('vendor_id', 'department_id')
    @api.onchange('vendor_id')
    def onchange_vendor_id(self):

        #if self.vendor_id and self.department_id:
        #    for department in self.vendor_id.department_ids:
        #        if department.department_id.id == self.department_id.id:
        #            self.sales_man_id = department.sales_man_id.id
        if self.vendor_id:
            self.vendor_code = self.vendor_id.code
            self.custom_commision = self.vendor_id.supplier_commission

        # get account for credit custody
        for custody_line in self.custody_lines:
            # if is rental costudy
            if self.vendor_id.expense_partner:
                if custody_line.is_rental and custody_line.custody_type == 'is_credit':
                    custody_line.credit_account = self.vendor_id.expense_partner.property_account_payable_id.id
                    custody_line.partner_id = self.vendor_id.expense_partner.id
            # *****************
            if custody_line.custody_type == 'is_credit' and custody_line.is_rental == False and custody_line.is_card == False:
                custody_line.partner_id = custody_line.partner_id.id
                custody_line.credit_account = custody_line.partner_id.property_account_payable_id.id
            # *****************
            elif custody_line.custody_type == 'is_credit' and custody_line.is_rental == False and custody_line.is_card == True:
                custody_line.partner_id = self.sales_man_id.card_partner.id
                custody_line.credit_account = self.sales_man_id.card_partner.property_account_payable_id.id

    @api.onchange('sales_man_id')
    def onchange_sales_man_id(self):

        # check if sales man is not supervisor set supervisor
        if not self.sales_man_id.is_sales_manger:
            self.supervisor_id = self.sales_man_id.supervisor
        else:
            self.supervisor_id = False

        # if costody is cash
        if self.sales_man_id.cash_account:
            for custody_line in self.custody_lines:
                # custody type salesman account from salesman
                if custody_line.sales_man == 'salesman':
                    if custody_line.custody_type == 'is_cash':
                        custody_line.cash_account = self.sales_man_id.expense_partner.property_account_receivable_id.id
                        custody_line.partner_id = self.sales_man_id.expense_partner.id
                # custody type supervisor account from supervisor
                else:
                    if self.supervisor_id.cash_account:
                        if custody_line.custody_type == 'is_cash':
                            custody_line.cash_account = self.supervisor_id.expense_partner.property_account_receivable_id.id
                            custody_line.partner_id = self.supervisor_id.expense_partner.id

    @api.onchange('vendor_code')
    def onchange_vendor_code(self):
        if self.vendor_code:
            vendor = self.env['res.partner'].sudo().search([('code', '=', self.vendor_code), ('supplier', '=', True)])
            if vendor:
                self.vendor_id = vendor.id

    @api.model
    def create(self, vals):
        if not vals.get('invoice_line'):
            raise UserError(_('You cannot save this invoice. please add product.'))
        # if vals.get('name', _('New')) == _('New'):
        vals['name'] = self.env['ir.sequence'].next_by_code('vendors.invoice')
        if vals.get('department_id'):
            self.env.user.partner_id.write({'department_id':vals.get('department_id')})
        if vals.get('sales_man_id'):
            self.env.user.partner_id.write({'sales_man_id':vals.get('sales_man_id')})
        result = super(VendorsInvoice, self).create(vals)
        return result

    @api.one
    def calculate_custody(self):
        custodies = self.env['vegetables.custody'].search([('id','=','4')])

        for custody_line in self.custody_lines:
            if custody_line.custody_name == 'كرت':
                custody_line.is_card = True

            if custody_line.custody_type == 'is_credit' and custody_line.is_rental == False and custody_line.is_card == False:
                custody_line.partner_id = custodies.partner_id.id
                custody_line.credit_account = custodies.partner_id.property_account_payable_id.id
            elif custody_line.custody_type == 'is_credit' and custody_line.is_rental == False and custody_line.is_card == True:
                custody_line.partner_id = self.sales_man_id.card_partner.id
                custody_line.credit_account = self.sales_man_id.card_partner.property_account_payable_id.id
            elif custody_line.custody_type == 'is_cash': 
                if custody_line.sales_man == 'salesman':
                    custody_line.cash_account = self.sales_man_id.expense_partner.property_account_receivable_id.id
                    custody_line.partner_id = self.sales_man_id.expense_partner.id
                else:
                    if self.supervisor_id.cash_account:
                        custody_line.cash_account = self.supervisor_id.expense_partner.property_account_receivable_id.id
                        custody_line.partner_id = self.supervisor_id.expense_partner.id
            elif custody_line.custody_type == 'is_credit' and custody_line.is_rental == True and custody_line.is_card == False: 
                custody_line.credit_account = self.vendor_id.expense_partner.property_account_payable_id.id
                custody_line.partner_id = self.vendor_id.expense_partner.id

    @api.multi
    def write(self, values):
	#print "AAAAAAA ", values
        if values:
	    for rec in self:
            	    #print "::::::", rec.name
		    record = self.env["vendors.invoice"].browse(rec.id)
		    # below code to minus sequence when change vendor
		    if 'vendor_id' in values:
		        old_partner = self.env['res.partner'].search([('id', '=', record.vendor_id.id)])
		        if record.vendor_id.vendor_invoice_sequence - 1 >= 0:
		            old_partner.update({'vendor_invoice_sequence': record.vendor_id.vendor_invoice_sequence - 1})

		    if not "edited" in values and record.state == 'posted':
			#print "QQQQQQQQQ   "
		        values['edited'] = True
		        #record.update_info()
                	if rec.invoice_date < '2019/01/26':
                            rec.calculate_custody()
		        super(VendorsInvoice, self).write(values)
		        account_move_id = record.update_info()
		        values = {}
		        #values.update({'account_move_id':account_move_id.id})
		        values['account_move_id'] = account_move_id
		        values['version_number'] = record.version_number + 1
		        values['edited'] = False
		        super(VendorsInvoice, self).write(values)
		    else:
		        super(VendorsInvoice, self).write(values)

		    #if "edited" in values and values['edited'] == True:
		        #response = pymsgbox.confirm('Your invoice data has been updated ?', 'Confirm Write',['Confirm', ' Discard'], timeout=6000)
		        # response = 'Confirm'
		        # if response == 'Confirm':
		        #     super(VendorsInvoice, self).write(values)
		        #     account_move_id = record.update_info()
		        #     values['account_move_id'] = account_move_id.id
		        #     values['version_number'] = record.version_number + 1
		        #     values['edited'] = False
		        # else:
		        #     values = {}
        return True

    @api.multi
    def unlink(self):
        for rec in self:
            #counter = 0
            for line in rec.invoice_line:
                line.unlink()

                '''customer_invoice_line = self.env['account.invoice.line'].search([('vendor_invoice_line_id', '=', line.id)])
                num_of_lines_in_account_inv=self.env['account.invoice.line'].search([('invoice_id', '=', customer_invoice_line.invoice_id.id)])
                for num_of_lines in num_of_lines_in_account_inv:
                    counter=counter+1
                if counter <= 1:
                    customer_invoice_line.invoice_id.write({'state': 'draft'})
                    customer_invoice_line.invoice_id.write({'move_name': ''})
                    customer_invoice_line.invoice_id.unlink()
                '''
            rec.account_move_id.unlink()
        res = super(VendorsInvoice, self).unlink()
        return res

    @api.constrains('custody_lines')
    def _check_custody_lines(self):
        for line in self.custody_lines:
            if (line.credit_amount > 0 and not line.credit_account) or (
                            line.cash_amount > 0 and not line.cash_account):
                raise ValidationError(_('Please select account for custody ') + line.custody_name)

    @api.constrains('invoice_line')
    def _check_invoice_line(self):
        customers_grouped = {}
        for line in self.invoice_line:
            if line.customer_id.id not in customers_grouped:
                customers_grouped[line.customer_id.id] = {'customer_id': line.customer_id,
                                                          'amount': line.product_qty * line.unit_price}
            else:
                customers_grouped[line.customer_id.id]['amount'] += line.product_qty * line.unit_price
        '''
        for customer_line in customers_grouped:
            if customers_grouped[customer_line]['customer_id'].credit + customers_grouped[customer_line]['amount'] > \
                    customers_grouped[customer_line]['customer_id'].credit_limit and customers_grouped[customer_line][
                'customer_id'].active_credit_limit:
                raise ValidationError(customers_grouped[customer_line]['customer_id'].name +
                                      _(' has exceeded the limit of credential..! '))
        '''

    # @api.multi
    # def action_invoice_sent(self):
    #     print "********************",self
    #     """ Open a window to compose an email, with the edi invoice template
    #         message loaded by default
    #     """
        
    #     template = self.env.ref('send_by_email.vendor_custom_email_template_edi_invoice', False)
    #     print template
    #     compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
    #     print compose_form
    #     ctx = dict(
    #         default_model='vendors.invoice',
    #         default_res_id=self.id,
    #         default_use_template=bool(template),
    #         default_template_id=template and template.id or False,
    #         default_composition_mode='comment',
    #         custom_layout="account.mail_template_data_notification_email_account_invoice"
    #     )
    #     print ctx
    #     return {
    #         'name': _('Compose Email'),
    #         'type': 'ir.actions.act_window',
    #         'view_type': 'form',
    #         'view_mode': 'form',
    #         'res_model': 'mail.compose.message',
    #         'views': [(compose_form.id, 'form')],
    #         'view_id': compose_form.id,
    #         'target': 'new',
    #         'context': ctx,
    #     }

    @api.one
    def post(self):
	#print "AAAAAAAAAA    "
	#print "WWWWWWWWWWWWWWWWWW  "
	j_obj = self.env['account.journal']
	m_obj = self.env['account.move']
	p_obj = self.env['res.partner']
        #print self
        for rec in self:
            
            user_id = self.env.user.id
            transmission_number = ""
            if rec.transmission_number:
                transmission_number += "اشعار:" + rec.transmission_number + " "
            if rec.driver_name:
                transmission_number += "السائق:" + rec.driver_name + " "
            if rec.postal_number:
                transmission_number += "الارسالية:" + rec.postal_number + " "
            if rec.transmission_date:
                transmission_number += "التاريخ" + rec.transmission_date + " "
            if transmission_number == "":
                transmission_number = "فاتورة مشتريات"

            if rec.state == 'posted':
                raise ValidationError(_('There is no journal items in draft state to post.'))

            posted = m_obj.sudo().search([(
                'ref', '=', rec.name
            )])
            if posted:
                rec.write({'state': 'posted',})

            else:

                journal_id = j_obj.sudo().search(
                    [('id', '=', rec.env.ref('vegetables.vegetable_vendor_journal').id)])
                if not rec.sales_man_id.custody_account:
                    raise ValidationError(_('You should link custody account in sales man page '))
                for custody_line in rec.custody_lines:
                    if (custody_line.cash_amount > 0 and not custody_line.cash_account) or (
                                    custody_line.credit_amount > 0 and not custody_line.credit_account):
                        raise ValidationError(_('You should select Account in ' + custody_line.custody_name))

                for product_line in rec.invoice_line:
                    if product_line.unit_price <= 0:
                        raise ValidationError(_(product_line.product_id.name + ' has no sale price!'))

                #sum_custody = 0
                #for custody_line in rec.custody_lines:
                #    sum_custody += custody_line.credit_amount + custody_line.cash_amount

                vendor_balance = rec.amount_total  # - sum_custody - rec.commision_total_amount - rec.total_taxed_amount
                vb = 0
                for vendor_balance_line in rec.invoice_line:
                    vb = vb+(vendor_balance_line.purchase_price*vendor_balance_line.product_qty)


                # Generate Account Move Line #vh
                move_line = []

                ## new post functions ##
                #move_name = journal_id.sequence_id.with_context(ir_sequence_date=rec.invoice_date).next_by_id()
                
                

                
                move_name = journal_id.sequence_id.with_context(ir_sequence_date=rec.invoice_date).next_by_id()
                #move_date = datetime.datetime.now().strftime("%Y-%m-%d")
                self._cr.execute("select id from account_move where ref = '%s';" %(rec.name))
                res = self._cr.dictfetchall()
                
                if not res :
                    
                    self._cr.execute("insert into account_move "\
                                    "( create_uid, name, state, ref, company_id, journal_id, "\
                                    "  currency_id, amount, matched_percentage, write_date, date, "\
                                    "  create_date, write_uid, reversed, custom_view, credit_amount, "\
                                    "  vendor_invoice_id) VALUES "\
                                    "( %s, '%s', '%s', '%s' ,%s, %s, "\
                                    " %s, %s, %s, '%s', '%s', "\
                                    " '%s', %s, '%s', '%s', %s, "\
                                    " %s ) ;" 
                                    %(user_id, move_name, 'posted', rec.name, self.company_id.id, journal_id.id,
                                    self.currency_id.id, rec.untaxes_total + rec.purchase_taxes_total, 0.0, datetime.datetime.now(), rec.invoice_date,
                                    datetime.datetime.now(), user_id, 'f', 'default', rec.untaxes_total + rec.purchase_taxes_total,
                                    rec.id))
                    
                    
                    self._cr.execute("select id from account_move where ref = '%s';" %(rec.name))
                    res = self._cr.dictfetchall()
                    account_move_id_new = res[0]['id']
                else:
                    account_move_id_new = res[0]['id']
                

                

                
                ## end of new post function ##
                '''
                move_date = datetime.datetime.now().strftime("%Y-%m-%d")
                account_move_id = self.env['account.move'].sudo().create({
                    'date': rec.invoice_date,
                    'journal_id': journal_id.id,
                    'name': '/',
                    'ref': rec.name,
                    'state': 'draft',
                    'vendor_invoice_id': rec.id,
                    'amount' : rec.untaxes_total,
                    'partner_id' : rec.vendor_id.id,
                })
                account_move_id_new = account_move_id.id
                '''
                #account_move_id_new = 55825

                if account_move_id_new:
                    for custody_line in rec.custody_lines:
                        # credit_amount = 0.0
                        # cash_amount = 0.0
                        if rec.transmission_number:
                            custody_name = custody_line.custody_name +" - "+ "اشعار:" + rec.transmission_number
                        else:
                            custody_name = custody_line.custody_name
                        if custody_line.credit_account and custody_line.amount > 0:
                            self._cr.execute("insert into account_move_line "\
                                            "( create_date, journal_id, date_maturity, user_type_id, "\
                                            "  partner_id, create_uid, amount_residual, company_id, "\
                                            "  credit_cash_basis, amount_residual_currency ,debit, "\
                                            "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                            "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                            "  company_currency_id, name, credit, "\
                                            "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                            "( '%s',%s,'%s', %s, "\
                                            "  %s, %s, %s, %s, "\
                                            "  %s, %s, %s, "\
                                            " '%s', %s, %s, '%s' , '%s', "\
                                            "  %s, '%s' ,'%s' ,%s , %s, "\
                                            "  %s ,'%s' , %s, "\
                                            "  %s ,%s ,'%s' ,%s,%s );" 
                                            %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                            custody_line.partner_id.id, user_id, 0.0, self.company_id.id,
                                            0.0, 0.0, 0.0,
                                            rec.name, custody_line.credit_account.id, 0.0, 'f', 't',
                                            0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                            self.currency_id.id, custody_name , custody_line.amount,
                                            0.0, custody_line.amount * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                            #res = self._cr.dictfetchall()
                            '''
                            move_line.append((0, 0, {
                                'account_id': custody_line.credit_account.id,
                                'name': custody_line.custody_name,
                                'credit': custody_line.credit_amount,
                                'partner_id': rec.vendor_id.id,
                                'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                                'vendor_invoice_id': rec.id,
                            }))
                            '''

                        if custody_line.cash_account and custody_line.amount > 0:
                            if rec.transmission_number:
                                custody_name = custody_line.custody_name +" - "+ "اشعار:" + rec.transmission_number
                            else:
                                custody_name = custody_line.custody_name
                            #print rec.sales_man_id.linked_customer.name," @@@@@@@@@@@@@@#######################################  ",rec.sales_man_id.linked_customer.id
                            self._cr.execute("insert into account_move_line "\
                                            "( create_date, journal_id, date_maturity, user_type_id, "\
                                            "  partner_id, create_uid, amount_residual, company_id, "\
                                            "  credit_cash_basis, amount_residual_currency ,debit, "\
                                            "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                            "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                            "  company_currency_id, name, credit, "\
                                            "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                            "( '%s',%s,'%s', %s, "\
                                            "  %s, %s, %s, %s, "\
                                            "  %s, %s, %s, "\
                                            " '%s', %s, %s, '%s' , '%s', "\
                                            "  %s, '%s' ,'%s' ,%s , %s, "\
                                            "  %s ,'%s' , %s, "\
                                            "  %s ,%s ,'%s' ,%s,%s );" 
                                            %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                            custody_line.partner_id.id, user_id, 0.0, self.company_id.id,
                                            0.0, 0.0, 0.0,
                                            rec.name, custody_line.cash_account.id, 0.0, 'f', 't',
                                            0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                            self.currency_id.id, custody_name, custody_line.amount,
                                            0.0, custody_line.amount * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                            #res = self._cr.dictfetchall()
                            '''
                            if rec.sales_man_id.linked_customer.id:
                                move_line.append((0, 0, {
                                    'account_id': rec.sales_man_id.linked_customer.property_account_receivable_id.id,
                                    'name': custody_line.custody_name,
                                    'credit': custody_line.cash_amount,
                                    'partner_id': rec.sales_man_id.linked_customer.id,
                                    'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                                    'vendor_invoice_id': rec.id,

                                }))
                            '''
                        '''
                        else:
                            print 'rec.id  ***************',custody_line.cash_account.id
                            self._cr.execute("insert into account_move_line "\
                                            "( create_date, journal_id, date_maturity, user_type_id, "\
                                            "  partner_id, create_uid, amount_residual, company_id, "\
                                            "  credit_cash_basis, amount_residual_currency ,debit, "\
                                            "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                            "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                            "  company_currency_id, name, credit, "\
                                            "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                            "( '%s',%s,'%s', %s, "\
                                            "  %s, %s, %s, %s, "\
                                            "  %s, %s, %s, "\
                                            " '%s', %s, %s, '%s' , '%s', "\
                                            "  %s, '%s' ,'%s' ,%s , %s, "\
                                            "  %s ,'%s' , %s, "\
                                            "  %s, %s, '%s', %s, %s );" 
                                            %(datetime.datetime.now(), journal_id.id, move_date, rec.sales_man_id.custody_account.user_type_id.id,
                                            rec.vendor_id.id, self.user_id.id, 0.0, self.company_id.id,
                                            0.0, 0.0, 0.0,
                                            rec.name, custody_line.cash_account.id, 0.0, 'f', 't',
                                            0.0, datetime.datetime.now(), move_date, self.user_id.id, account_move_id_new,
                                            self.currency_id.id, custody_line.custody_name, custody_line.cash_amount,
                                            0.0, custody_line.cash_amount * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                        #res = self._cr.dictfetchall()
                        '''
                        '''
                        move_line.append((0, 0, {
                            'account_id': custody_line.cash_account.id,
                            'name': custody_line.custody_name,
                            'credit': custody_line.cash_amount,
                            'partner_id': rec.vendor_id.id,
                            'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                            'vendor_invoice_id': rec.id,
                        }))
                        '''

                    if rec.vendor_id.has_taxes_vendor:
                        select_vender = rec.id
                        for line in self.get_taxes_accounts_values(select_vender).values():
                            if line['taxes_id'].account_id and float(line['amount']) > 0 and line[
                                'taxes_id'].account_id and float(line['purchase_taxes']) > 0:
                                vb += line['purchase_taxes']

                    if vb > 0:
                        self._cr.execute("insert into account_move_line "\
                                            "( create_date, journal_id, date_maturity, user_type_id, "\
                                            "  partner_id, create_uid, amount_residual, company_id, "\
                                            "  credit_cash_basis, amount_residual_currency ,debit, "\
                                            "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                            "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                            "  company_currency_id, name, credit, "\
                                            "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                            "( '%s',%s,'%s', %s, "\
                                            "  %s, %s, %s, %s, "\
                                            "  %s, %s, %s, "\
                                            " '%s', %s, %s, '%s' , '%s', "\
                                            "  %s, '%s' ,'%s' ,%s , %s, "\
                                            "  %s ,'%s' , %s, "\
                                            "  %s ,%s ,'%s' ,%s,%s );" 
                                            %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                            rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                            0.0, 0.0, 0.0,
                                            rec.name, rec.vendor_id.property_account_payable_id.id, 0.0, 'f', 't',
                                            0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                            self.currency_id.id,  transmission_number, vb,
                                            0.0, vb * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                        #res = self._cr.dictfetchall()

                        '''
                        move_line.append((0, 0, {
                            'account_id': rec.vendor_id.property_account_payable_id.id,
                            'name': _('Transmission Number: ') + transmission_number,
                            'credit': vb,
                            'partner_id': rec.vendor_id.id,
                            'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                            'vendor_invoice_id': rec.id,
                        }))
                        '''
                    elif vb < 0:
                        self._cr.execute("insert into account_move_line "\
                                            "( create_date, journal_id, date_maturity, user_type_id, "\
                                            "  partner_id, create_uid, amount_residual, company_id, "\
                                            "  credit_cash_basis, amount_residual_currency ,debit, "\
                                            "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                            "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                            "  company_currency_id, name, credit, "\
                                            "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                            "( '%s',%s,'%s', %s, "\
                                            "  %s, %s, %s, %s, "\
                                            "  %s, %s, %s, "\
                                            " '%s', %s, %s, '%s' , '%s', "\
                                            "  %s, '%s' ,'%s' ,%s , %s, "\
                                            "  %s ,'%s' , %s, "\
                                            "  %s ,%s ,'%s' ,%s,%s );" 
                                            %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                            rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                            0.0, 0.0, vb * -1,
                                            rec.name, rec.vendor_id.property_account_payable_id.id, 0.0, 'f', 't',
                                            0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                            self.currency_id.id, transmission_number, 0.0,
                                            0.0, vb , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                        #res = self._cr.dictfetchall()
                        '''
                        move_line.append((0, 0, {
                            'account_id': rec.vendor_id.property_account_payable_id.id,
                            'name': _('Transmission Number: ') + transmission_number,
                            'debit': vb * -1,
                            'partner_id': rec.vendor_id.id,
                            'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                            'vendor_invoice_id': rec.id,
                        }))
                        '''
                    # vh
                    tax_ids = []
                    sales_man_custody_account_id = rec.sales_man_id.custody_account.id
                    vendor_id = rec.vendor_id.id
                    sales_man_id = rec.sales_man_id and rec.sales_man_id.id or False
                    #vendor_invoice_id = rec.id
                    for line in rec.invoice_line:
                        tax_ids = []
                        if line.taxes_id:
                            tax_ids.append((4, line.taxes_id.id, None))
                            for child in line.taxes_id.children_tax_ids:
                                if child and child.type_tax_use != 'none':
                                    tax_ids.append((4, child.id, None))
                        if line.purchase_price > 0:
                            ## new post ##
                            self._cr.execute("insert into account_move_line "\
                                            "( create_date, journal_id, date_maturity, user_type_id, "\
                                            "  partner_id, create_uid, amount_residual, company_id, "\
                                            "  credit_cash_basis, amount_residual_currency ,debit, "\
                                            "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                            "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                            "  product_id, company_currency_id, name, tax_line_id, credit, "\
                                            "  amount_currency, balance, quantity, active, vendor_invoice_id, sales_man_id, "\
                                            "  vendor_invoice_line_id) values "\
                                            "( '%s', %s, '%s', %s, "\
                                            "  %s, '%s', %s, %s, "\
                                            "  %s, %s, %s, "\
                                            " '%s', %s, %s, '%s', '%s', "\
                                            "  %s, '%s' ,'%s' ,%s ,%s, "\
                                            "  %s ,%s ,'%s' ,%s ,%s ,"\
                                            "  %s ,%s , %s, '%s',%s ,%s ,"\
                                            "  %s);" 
                                            %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                            vendor_id, user_id, 0.0, self.company_id.id,
                                            0.0, 0.0, line.product_qty * line.purchase_price,
                                            rec.name, sales_man_custody_account_id, 0.0, 'f', 't',
                                            0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                            line.product_id.id, self.currency_id.id, line.product_id.name, line.taxes_id.id, 0.0,
                                            0.0, (line.product_qty * line.purchase_price), line.product_qty, 't', rec.id, sales_man_id, 
                                            line.id))
                            #res = self._cr.dictfetchall()
                            ## end new post##
                            '''
                            move_line.append((0,0,{
                                                'name': "sales man  custody account",
                                                'price_unit': line.purchase_price,
                                                'quantity': line.product_qty,
                                                'price': line.product_qty * line.purchase_price,
                                                'account_id': sales_man_custody_account_id,
                                                'product_id': line.product_id.id,
                                                'debit': line.product_qty * line.purchase_price,
                                                'uom_id': line.uom_id.id,
                                                'tax_ids': tax_ids,
                                                'vendor_invoice_id': vendor_invoice_id,
                                                'vendor_invoice_line_id': line.id,
                                                'partner_id': vendor_id,
                                                'sales_man_id': sales_man_id,
                                            }))
                            '''
                        #for tax in line.taxes_id:
                            
                    if (rec.custody_total + rec.commision_total_amount) > 0:
                        self._cr.execute("insert into account_move_line "\
                                            "( create_date, journal_id, date_maturity, user_type_id, "\
                                            "  partner_id, create_uid, amount_residual, company_id, "\
                                            "  credit_cash_basis, amount_residual_currency ,debit, "\
                                            "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                            "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                            "  company_currency_id, name, credit, "\
                                            "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                            "( '%s',%s,'%s', %s, "\
                                            "  %s, %s, %s, %s, "\
                                            "  %s, %s, %s, "\
                                            " '%s', %s, %s, '%s' , '%s', "\
                                            "  %s, '%s' ,'%s' ,%s , %s, "\
                                            "  %s ,'%s' , %s, "\
                                            "  %s ,%s ,'%s' ,%s,%s );" 
                                            %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                            rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                            0.0, 0.0, rec.custody_total + rec.commision_total_amount,
                                            rec.name, rec.sales_man_id.custody_account.id, 0.0, 'f', 't',
                                            0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                            self.currency_id.id, "sales man  custody account", 0.0,
                                            0.0, (rec.custody_total + rec.commision_total_amount), 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                        #res = self._cr.dictfetchall()
                        '''
                        move_line.append((0, 0, {
                            'account_id': rec.sales_man_id.custody_account.id,
                            'name': "sales man  custody account",
                            #'debit': rec.amount_total + sum_custody + rec.commision_total_amount,
                            'debit': rec.custody_total + rec.commision_total_amount,
                            'partner_id': rec.vendor_id.id,
                            #'tax_ids': tax_ids,
                            'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                            'vendor_invoice_id': rec.id,
                        }))
                        '''
                    if rec.vendor_id.has_taxes_vendor:
                        select_vender = rec.id
                        for line in self.get_taxes_accounts_values(select_vender).values():

                            if rec.total_taxed_amount > 0:
                                self._cr.execute("insert into account_move_line "\
                                            "( create_date, journal_id, date_maturity, user_type_id, "\
                                            "  partner_id, create_uid, amount_residual, company_id, "\
                                            "  credit_cash_basis, amount_residual_currency ,debit, "\
                                            "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                            "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                            "  company_currency_id, name, tax_line_id, credit, "\
                                            "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                            "( '%s',%s,'%s', %s, "\
                                            "  %s, %s, %s, %s, "\
                                            "  %s, %s, %s, "\
                                            " '%s', %s, %s, '%s' , '%s', "\
                                            "  %s, '%s' ,'%s' ,%s , %s, "\
                                            "  %s ,'%s' ,%s , %s, "\
                                            "  %s ,%s ,'%s' ,%s,%s );" 
                                            %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                            rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                            0.0, 0.0, line['purchase_taxes'],
                                            rec.name, line['taxes_id'].refund_account_id.id, 0.0, 'f', 't',
                                            0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                            self.currency_id.id, 'Taxes', line['taxes_id'].id, 0.0,
                                            0.0, line['purchase_taxes'] , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                                #res = self._cr.dictfetchall()
                                '''
                                move_line.append((0, 0, {
                                    'account_id': line['taxes_id'].refund_account_id.id,
                                    'name': "Taxes",
                                    'debit': line['purchase_taxes'],
                                    'partner_id': rec.vendor_id.id,
                                    'tax_line_id': line['taxes_id'].id,
                                    'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                                    'vendor_invoice_id': rec.id,
                                }))
                                '''
                    if rec.commision_total_amount != 0:
                        self._cr.execute("insert into account_move_line "\
                                            "( create_date, journal_id, date_maturity, user_type_id, "\
                                            "  partner_id, create_uid, amount_residual, company_id, "\
                                            "  credit_cash_basis, amount_residual_currency ,debit, "\
                                            "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                            "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                            "  company_currency_id, name, credit, "\
                                            "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                            "( '%s',%s,'%s', %s, "\
                                            "  %s, %s, %s, %s, "\
                                            "  %s, %s, %s, "\
                                            " '%s', %s, %s, '%s' , '%s', "\
                                            "  %s, '%s' ,'%s' ,%s , %s, "\
                                            "  %s ,'%s' , %s, "\
                                            "  %s ,%s ,'%s' ,%s,%s );" 
                                            %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                            rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                            0.0, 0.0, 0.0,
                                            rec.name, rec.sales_man_id.commission_account.id, 0.0, 'f', 't',
                                            0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                            self.currency_id.id, _('Commission'), rec.commision_total_amount,
                                            0.0, rec.commision_total_amount * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                        #res = self._cr.dictfetchall()
                        '''
                        move_line.append((0, 0, {
                            'account_id': rec.sales_man_id.commission_account.id,
                            'name': _('Commission'),
                            'credit': rec.commision_total_amount,
                            'partner_id': rec.vendor_id.id,
                            'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                            'vendor_invoice_id': rec.id,
                        }))
                        '''

                # Generate Account Move object
                '''
                account_move_id = m_obj.sudo().create({
                    'date': rec.invoice_date,
                    'journal_id': journal_id.id,
                    'name': '/',
                    'ref': rec.name,
                    'state': 'draft',
                    'line_ids': move_line,
                    'vendor_invoice_id': rec.id,
                })
                '''

                # post journal entry
                #account_move_id.post()

                #m_obj.browse(account_move_id_new).post()
                #if account_move_id_new.state == 'posted':
                rec.write({'state': 'posted', 'edited': False, 'account_move_id': account_move_id_new})
                rec.vendor_id.write({'vendor_invoice_sequence': rec.vendor_id.vendor_invoice_sequence + 1})
		if rec.sales_man_id.is_amount_debit and rec.sales_man_id.amount_debit and rec.sales_man_id.user_id:
			p_ids= p_obj.search([('user_id','=',rec.sales_man_id.user_id.id),('customer','=',True)])
			balance = 0.0
			for recp in p_ids:
			    for line in recp.moves_lines_ids:
				if line.account_id.id not in [recp.property_account_receivable_id.id,recp.property_account_receivable_id.id]:
				    continue

				if line.account_id.id == recp.property_account_receivable_id.id:
				    balance += round(line.debit, 2) - round(line.credit, 2)

				# vendors
				if line.account_id.id == recp.property_account_payable_id.id:
				    balance += round(line.credit, 2) - round(line.debit, 2)
			#print "AAAAAa   ", balance
			if balance  > rec.sales_man_id.amount_debit:
				raise ValidationError(_('Sorry, you cannot Exceed the Limit of specific Sales Man.'))
	    cr = self.env.cr
	    v_invoice = rec.id
	    sql= (" select vil.vendor_invoice_id as vid from vendors_invoice_line vil \
			 WHERE vil.vendor_invoice_id = %s and executed_line = 'f'\
		")
	    params = (v_invoice,)
	    cr.execute(sql, params)
	    f= cr.dictfetchall()
	    if not ( f and f[0] or False):
		    sql= (" update vendors_invoice set executed_lines = 1\
				 WHERE id = %s")
		    params = (v_invoice,)
		    cr.execute(sql, params)
	    else:
		    sql= (" update vendors_invoice set executed_lines = 0\
				 WHERE id = %s")
		    params = (v_invoice,)
		    cr.execute(sql, params)
			#raise ValidationError(_('TestSTSTSTSTSt'))
            return True
    
   

    @api.multi
    def update_info(self):
        print "LLLLLLLLLLLLLLLL"
        # if len(self) > 10:
        #     raise UserError(_('Can\'t update more than 10 records...!'))
        #for rec in self:
	#	rec.commision= rec.vendor_id.supplier_commission
	#	rec.custom_commision= rec.vendor_id.supplier_commission
	j_obj = self.env['account.journal']
	m_obj = self.env['account.move']
	p_obj = self.env['res.partner']
        for rec in self:
            if rec.invoice_date < '2019/01/26':
                rec.calculate_custody()
            user_id = self.env.user.id
            transmission_number = ""
            if rec.transmission_number:
                transmission_number += "اشعار:" + rec.transmission_number + " "
            if rec.driver_name:
                transmission_number += "السائق:" + rec.driver_name + " "
            if rec.postal_number:
                transmission_number += "الارسالية:" + rec.postal_number + " "
            if rec.transmission_date:
                transmission_number += "التاريخ" + rec.transmission_date + " "
            if transmission_number == "":
                transmission_number = "فاتورة مشتريات"

            # if rec.edited:
            if not rec.sales_man_id.custody_account:
                raise ValidationError(_('You should link custody account in sales man page '))
            for custody_line in rec.custody_lines:
                if (custody_line.cash_amount > 0 and not custody_line.cash_account) or (
                                custody_line.credit_amount > 0 and not custody_line.credit_account):
                    raise ValidationError(_('You should select Account in ' + custody_line.custody_name))

            for product_line in rec.invoice_line:
                if product_line.unit_price <= 0:
                    raise ValidationError(_(product_line.product_id.name + ' has no sale price!'))

            #sum_custody = 0
            #for custody_line in rec.custody_lines:
            #    sum_custody += custody_line.credit_amount + custody_line.cash_amount

            #vendor_balance = rec.amount_total  # - sum_custody - rec.commision_total_amount - rec.total_taxed_amount

            vb = 0
            for vendor_balance_line in rec.invoice_line:
                vb = vb+(vendor_balance_line.purchase_price*vendor_balance_line.product_qty)

            # ************************************* Edit post Entry ****************************************************
            # Generate Account Move Line
            #move_line = []
            self._cr.execute("delete from account_move_line where ref = '%s';" %(rec.name))
            self._cr.execute("delete from account_move where ref = '%s';" %(rec.name))
            
            journal_id = j_obj.sudo().search(
                [('id', '=', rec.env.ref('vegetables.vegetable_vendor_journal').id)])

            move_name = journal_id.sequence_id.with_context(ir_sequence_date=rec.invoice_date).next_by_id()
            #move_date = datetime.datetime.now().strftime("%Y-%m-%d")
            self._cr.execute("select id from account_move where ref = '%s';" %(rec.name))
            res = self._cr.dictfetchall()
            
            if not res :
                
                self._cr.execute("insert into account_move "\
                                "( create_uid, name, state, ref, company_id, journal_id, "\
                                "  currency_id, amount, matched_percentage, write_date, date, "\
                                "  create_date, write_uid, reversed, custom_view, credit_amount, "\
                                "  vendor_invoice_id) VALUES "\
                                "( %s, '%s', '%s', '%s' ,%s, %s, "\
                                " %s, %s, %s, '%s', '%s', "\
                                " '%s', %s, '%s', '%s', %s, "\
                                " %s ) ;" 
                                %(user_id, move_name, 'posted', rec.name, self.company_id.id, journal_id.id,
                                self.currency_id.id, rec.untaxes_total + rec.purchase_taxes_total, 0.0, datetime.datetime.now(), rec.invoice_date,
                                datetime.datetime.now(), user_id, 'f', 'default', rec.untaxes_total + rec.purchase_taxes_total,
                                rec.id))
                
                
                self._cr.execute("select id from account_move where ref = '%s';" %(rec.name))
                res = self._cr.dictfetchall()
                account_move_id_new = res[0]['id']
            else:
                account_move_id_new = res[0]['id']
            #account_move_id_new = 55825

            if account_move_id_new:
                #print "**********************************************",account_move_id_new
                for custody_line in rec.custody_lines:

                    if custody_line.credit_account and custody_line.amount > 0:
                        if rec.transmission_number:
                            custody_name = custody_line.custody_name +" - "+ "اشعار:" + rec.transmission_number
                        else:
                            custody_name = custody_line.custody_name
                        self._cr.execute("insert into account_move_line "\
                                        "( create_date, journal_id, date_maturity, user_type_id, "\
                                        "  partner_id, create_uid, amount_residual, company_id, "\
                                        "  credit_cash_basis, amount_residual_currency ,debit, "\
                                        "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                        "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                        "  company_currency_id, name, credit, "\
                                        "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                        "( '%s',%s,'%s', %s, "\
                                        "  %s, %s, %s, %s, "\
                                        "  %s, %s, %s, "\
                                        " '%s', %s, %s, '%s' , '%s', "\
                                        "  %s, '%s' ,'%s' ,%s , %s, "\
                                        "  %s ,'%s' , %s, "\
                                        "  %s ,%s ,'%s' ,%s,%s );" 
                                        %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                        custody_line.partner_id.id, user_id, 0.0, self.company_id.id,
                                        0.0, 0.0, 0.0,
                                        rec.name, custody_line.credit_account.id, 0.0, 'f', 't',
                                        0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                        self.currency_id.id, custody_name, custody_line.amount,
                                        0.0, custody_line.amount * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                        #res = self._cr.dictfetchall()
                        '''
                        move_line.append((0, 0, {
                            'account_id': custody_line.credit_account.id,
                            'name': custody_line.custody_name,
                            'credit': custody_line.credit_amount,
                            'partner_id': rec.vendor_id.id,
                            'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                            'vendor_invoice_id': rec.id,
                        }))
                        '''

                    if custody_line.cash_account and custody_line.amount > 0:
                        if rec.transmission_number:
                            custody_name = custody_line.custody_name +" - "+ "اشعار:" + rec.transmission_number
                        else:
                            custody_name = custody_line.custody_name
                        #print rec.sales_man_id.linked_customer.name," @@@@@@@@@@@@@@#######################################  ",rec.sales_man_id.linked_customer.id
                        self._cr.execute("insert into account_move_line "\
                                        "( create_date, journal_id, date_maturity, user_type_id, "\
                                        "  partner_id, create_uid, amount_residual, company_id, "\
                                        "  credit_cash_basis, amount_residual_currency ,debit, "\
                                        "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                        "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                        "  company_currency_id, name, credit, "\
                                        "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                        "( '%s',%s,'%s', %s, "\
                                        "  %s, %s, %s, %s, "\
                                        "  %s, %s, %s, "\
                                        " '%s', %s, %s, '%s' , '%s', "\
                                        "  %s, '%s' ,'%s' ,%s , %s, "\
                                        "  %s ,'%s' , %s, "\
                                        "  %s ,%s ,'%s' ,%s,%s );" 
                                        %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                        custody_line.partner_id.id, user_id, 0.0, self.company_id.id,
                                        0.0, 0.0, 0.0,
                                        rec.name, custody_line.cash_account.id, 0.0, 'f', 't',
                                        0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                        self.currency_id.id, custody_name, custody_line.amount,
                                        0.0, custody_line.amount * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                        #res = self._cr.dictfetchall()
                        '''
                        if rec.sales_man_id.linked_customer.id:
                            move_line.append((0, 0, {
                                'account_id': rec.sales_man_id.linked_customer.property_account_receivable_id.id,
                                'name': custody_line.custody_name,
                                'credit': custody_line.cash_amount,
                                'partner_id': rec.sales_man_id.linked_customer.id,
                                'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                                'vendor_invoice_id': rec.id,

                            }))
                        '''
                    '''
                    else:
                        print 'rec.id  ***************',custody_line.cash_account.id
                        self._cr.execute("insert into account_move_line "\
                                        "( create_date, journal_id, date_maturity, user_type_id, "\
                                        "  partner_id, create_uid, amount_residual, company_id, "\
                                        "  credit_cash_basis, amount_residual_currency ,debit, "\
                                        "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                        "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                        "  company_currency_id, name, credit, "\
                                        "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                        "( '%s',%s,'%s', %s, "\
                                        "  %s, %s, %s, %s, "\
                                        "  %s, %s, %s, "\
                                        " '%s', %s, %s, '%s' , '%s', "\
                                        "  %s, '%s' ,'%s' ,%s , %s, "\
                                        "  %s ,'%s' , %s, "\
                                        "  %s, %s, '%s', %s, %s );" 
                                        %(datetime.datetime.now(), journal_id.id, move_date, rec.sales_man_id.custody_account.user_type_id.id,
                                        rec.vendor_id.id, self.user_id.id, 0.0, self.company_id.id,
                                        0.0, 0.0, 0.0,
                                        rec.name, custody_line.cash_account.id, 0.0, 'f', 't',
                                        0.0, datetime.datetime.now(), move_date, self.user_id.id, account_move_id_new,
                                        self.currency_id.id, custody_line.custody_name, custody_line.cash_amount,
                                        0.0, custody_line.cash_amount * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                    #res = self._cr.dictfetchall()
                    '''
                    '''
                    move_line.append((0, 0, {
                        'account_id': custody_line.cash_account.id,
                        'name': custody_line.custody_name,
                        'credit': custody_line.cash_amount,
                        'partner_id': rec.vendor_id.id,
                        'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                        'vendor_invoice_id': rec.id,
                    }))
                    '''

                if rec.vendor_id.has_taxes_vendor:
                    select_vender = rec.id
                    for line in self.get_taxes_accounts_values(select_vender).values():
                        if line['taxes_id'].account_id and float(line['amount']) > 0 and line[
                            'taxes_id'].account_id and float(line['purchase_taxes']) > 0:
                            vb += line['purchase_taxes']

                if vb > 0:
                    self._cr.execute("insert into account_move_line "\
                                        "( create_date, journal_id, date_maturity, user_type_id, "\
                                        "  partner_id, create_uid, amount_residual, company_id, "\
                                        "  credit_cash_basis, amount_residual_currency ,debit, "\
                                        "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                        "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                        "  company_currency_id, name, credit, "\
                                        "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                        "( '%s',%s,'%s', %s, "\
                                        "  %s, %s, %s, %s, "\
                                        "  %s, %s, %s, "\
                                        " '%s', %s, %s, '%s' , '%s', "\
                                        "  %s, '%s' ,'%s' ,%s , %s, "\
                                        "  %s ,'%s' , %s, "\
                                        "  %s ,%s ,'%s' ,%s,%s );" 
                                        %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                        rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                        0.0, 0.0, 0.0,
                                        rec.name, rec.vendor_id.property_account_payable_id.id, 0.0, 'f', 't',
                                        0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                        self.currency_id.id, transmission_number, vb,
                                        0.0, vb * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                    #res = self._cr.dictfetchall()

                    '''
                    move_line.append((0, 0, {
                        'account_id': rec.vendor_id.property_account_payable_id.id,
                        'name': _('Transmission Number: ') + transmission_number,
                        'credit': vb,
                        'partner_id': rec.vendor_id.id,
                        'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                        'vendor_invoice_id': rec.id,
                    }))
                    '''
                elif vb < 0:
                    self._cr.execute("insert into account_move_line "\
                                        "( create_date, journal_id, date_maturity, user_type_id, "\
                                        "  partner_id, create_uid, amount_residual, company_id, "\
                                        "  credit_cash_basis, amount_residual_currency ,debit, "\
                                        "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                        "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                        "  company_currency_id, name, credit, "\
                                        "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                        "( '%s',%s,'%s', %s, "\
                                        "  %s, %s, %s, %s, "\
                                        "  %s, %s, %s, "\
                                        " '%s', %s, %s, '%s' , '%s', "\
                                        "  %s, '%s' ,'%s' ,%s , %s, "\
                                        "  %s ,'%s' , %s, "\
                                        "  %s ,%s ,'%s' ,%s,%s );" 
                                        %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                        rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                        0.0, 0.0, vb * -1,
                                        rec.name, rec.vendor_id.property_account_payable_id.id, 0.0, 'f', 't',
                                        0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                        self.currency_id.id, transmission_number, 0.0,
                                        0.0, vb , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                    #res = self._cr.dictfetchall()
                    '''
                    move_line.append((0, 0, {
                        'account_id': rec.vendor_id.property_account_payable_id.id,
                        'name': _('Transmission Number: ') + transmission_number,
                        'debit': vb * -1,
                        'partner_id': rec.vendor_id.id,
                        'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                        'vendor_invoice_id': rec.id,
                    }))
                    '''
                # vh
                tax_ids = []
                sales_man_custody_account_id = rec.sales_man_id.custody_account.id
                vendor_id = rec.vendor_id.id
                sales_man_id = rec.sales_man_id and rec.sales_man_id.id or False
                #vendor_invoice_id = rec.id
                for line in rec.invoice_line:
                    tax_ids = []
                    if line.taxes_id:
                        tax_ids.append((4, line.taxes_id.id, None))
                        for child in line.taxes_id.children_tax_ids:
                            if child and child.type_tax_use != 'none':
                                tax_ids.append((4, child.id, None))
                    if line.purchase_price > 0:
                        ## new post ##
                        self._cr.execute("insert into account_move_line "\
                                        "( create_date, journal_id, date_maturity, user_type_id, "\
                                        "  partner_id, create_uid, amount_residual, company_id, "\
                                        "  credit_cash_basis, amount_residual_currency ,debit, "\
                                        "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                        "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                        "  product_id, company_currency_id, name, tax_line_id, credit, "\
                                        "  amount_currency, balance, quantity, active, vendor_invoice_id, sales_man_id, "\
                                        "  vendor_invoice_line_id) values "\
                                        "( '%s', %s, '%s', %s, "\
                                        "  %s, '%s', %s, %s, "\
                                        "  %s, %s, %s, "\
                                        " '%s', %s, %s, '%s', '%s', "\
                                        "  %s, '%s' ,'%s' ,%s ,%s, "\
                                        "  %s ,%s ,'%s' ,%s ,%s ,"\
                                        "  %s ,%s , %s, '%s',%s ,%s ,"\
                                        "  %s);" 
                                        %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                        vendor_id, user_id, 0.0, self.company_id.id,
                                        0.0, 0.0, line.product_qty * line.purchase_price,
                                        rec.name, sales_man_custody_account_id, 0.0, 'f', 't',
                                        0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                        line.product_id.id, self.currency_id.id, line.product_id.name , line.taxes_id.id, 0.0,
                                        0.0, (line.product_qty * line.purchase_price), line.product_qty, 't', rec.id, sales_man_id, 
                                        line.id))
                        #res = self._cr.dictfetchall()
                        ## end new post##
                        '''
                        move_line.append((0,0,{
                                            'name': "sales man  custody account",
                                            'price_unit': line.purchase_price,
                                            'quantity': line.product_qty,
                                            'price': line.product_qty * line.purchase_price,
                                            'account_id': sales_man_custody_account_id,
                                            'product_id': line.product_id.id,
                                            'debit': line.product_qty * line.purchase_price,
                                            'uom_id': line.uom_id.id,
                                            'tax_ids': tax_ids,
                                            'vendor_invoice_id': vendor_invoice_id,
                                            'vendor_invoice_line_id': line.id,
                                            'partner_id': vendor_id,
                                            'sales_man_id': sales_man_id,
                                        }))
                        '''
                    #for tax in line.taxes_id:
                        
                if (rec.custody_total + rec.commision_total_amount) > 0:
                    self._cr.execute("insert into account_move_line "\
                                        "( create_date, journal_id, date_maturity, user_type_id, "\
                                        "  partner_id, create_uid, amount_residual, company_id, "\
                                        "  credit_cash_basis, amount_residual_currency ,debit, "\
                                        "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                        "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                        "  company_currency_id, name, credit, "\
                                        "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                        "( '%s',%s,'%s', %s, "\
                                        "  %s, %s, %s, %s, "\
                                        "  %s, %s, %s, "\
                                        " '%s', %s, %s, '%s' , '%s', "\
                                        "  %s, '%s' ,'%s' ,%s , %s, "\
                                        "  %s ,'%s' , %s, "\
                                        "  %s ,%s ,'%s' ,%s,%s );" 
                                        %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                        rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                        0.0, 0.0, rec.custody_total + rec.commision_total_amount,
                                        rec.name, rec.sales_man_id.custody_account.id, 0.0, 'f', 't',
                                        0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                        self.currency_id.id, "sales man  custody account", 0.0,
                                        0.0, (rec.custody_total + rec.commision_total_amount), 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                    #res = self._cr.dictfetchall()
                    '''
                    move_line.append((0, 0, {
                        'account_id': rec.sales_man_id.custody_account.id,
                        'name': "sales man  custody account",
                        #'debit': rec.amount_total + sum_custody + rec.commision_total_amount,
                        'debit': rec.custody_total + rec.commision_total_amount,
                        'partner_id': rec.vendor_id.id,
                        #'tax_ids': tax_ids,
                        'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                        'vendor_invoice_id': rec.id,
                    }))
                    '''
                if rec.vendor_id.has_taxes_vendor:
                    select_vender = rec.id
                    for line in self.get_taxes_accounts_values(select_vender).values():

                        if rec.total_taxed_amount > 0:
                            self._cr.execute("insert into account_move_line "\
                                        "( create_date, journal_id, date_maturity, user_type_id, "\
                                        "  partner_id, create_uid, amount_residual, company_id, "\
                                        "  credit_cash_basis, amount_residual_currency ,debit, "\
                                        "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                        "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                        "  company_currency_id, name, tax_line_id, credit, "\
                                        "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                        "( '%s',%s,'%s', %s, "\
                                        "  %s, %s, %s, %s, "\
                                        "  %s, %s, %s, "\
                                        " '%s', %s, %s, '%s' , '%s', "\
                                        "  %s, '%s' ,'%s' ,%s , %s, "\
                                        "  %s ,'%s' ,%s , %s, "\
                                        "  %s ,%s ,'%s' ,%s,%s );" 
                                        %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                        rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                        0.0, 0.0, line['purchase_taxes'],
                                        rec.name, line['taxes_id'].refund_account_id.id, 0.0, 'f', 't',
                                        0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                        self.currency_id.id, 'Taxes', line['taxes_id'].id, 0.0,
                                        0.0, line['purchase_taxes'] , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                            #res = self._cr.dictfetchall()
                            '''
                            move_line.append((0, 0, {
                                'account_id': line['taxes_id'].refund_account_id.id,
                                'name': "Taxes",
                                'debit': line['purchase_taxes'],
                                'partner_id': rec.vendor_id.id,
                                'tax_line_id': line['taxes_id'].id,
                                'sales_man_id': rec.sales_man_id and rec.sales_man_id.id or False,
                                'vendor_invoice_id': rec.id,
                            }))
                            '''
                if rec.commision_total_amount != 0:
                    self._cr.execute("insert into account_move_line "\
                                        "( create_date, journal_id, date_maturity, user_type_id, "\
                                        "  partner_id, create_uid, amount_residual, company_id, "\
                                        "  credit_cash_basis, amount_residual_currency ,debit, "\
                                        "  ref, account_id, debit_cash_basis, reconciled, tax_exigible, "\
                                        "  balance_cash_basis, write_date, date, write_uid, move_id, "\
                                        "  company_currency_id, name, credit, "\
                                        "  amount_currency, balance, active, vendor_invoice_id, sales_man_id) values "\
                                        "( '%s',%s,'%s', %s, "\
                                        "  %s, %s, %s, %s, "\
                                        "  %s, %s, %s, "\
                                        " '%s', %s, %s, '%s' , '%s', "\
                                        "  %s, '%s' ,'%s' ,%s , %s, "\
                                        "  %s ,'%s' , %s, "\
                                        "  %s ,%s ,'%s' ,%s,%s );" 
                                        %(datetime.datetime.now(), journal_id.id, rec.invoice_date, rec.sales_man_id.custody_account.user_type_id.id,
                                        rec.vendor_id.id, user_id, 0.0, self.company_id.id,
                                        0.0, 0.0, 0.0,
                                        rec.name, rec.sales_man_id.commission_account.id, 0.0, 'f', 't',
                                        0.0, datetime.datetime.now(), rec.invoice_date, user_id, account_move_id_new,
                                        self.currency_id.id, _('Commission'), rec.commision_total_amount,
                                        0.0, rec.commision_total_amount * -1 , 't', rec.id, rec.sales_man_id and rec.sales_man_id.id or False))
                    #res = self._cr.dictfetchall()
                   

               

            # post journal entry
            #account_move_id.post()
            #rec.write({'account_move_id': account_move_id.id})

            # update customers invoices
            for line in rec.invoice_line:
                if line.product_id.taxes_id and rec.vendor_id.has_taxes_vendor:
                    line.invoice_line_tax_ids = line.product_id.taxes_id[0]
                else:
                    line.invoice_line_tax_ids = [(5,)]
                    # v8
                customer_invoice_line = rec.env['account.invoice.line'].search(
                    [('vendor_invoice_line_id', '=', line.id)])
                if customer_invoice_line:  # v81
                    if customer_invoice_line.invoice_id.partner_id.id == line.customer_id.id:  # v82
                        # reset taxes
                        if line.product_id.taxes_id and line.customer_id.has_taxes:
                            taxes = line.product_id.taxes_id[0]
                            tax_amt = taxes.amount

                        else:
                            taxes = [(5,)]
                            tax_amt = 0
                        #    """
                        
                        #"""

                        if (line.product_id.id <> customer_invoice_line.product_id.id or
                                    line.uom_id.id <> customer_invoice_line.uom_id.id or
                                    line.product_qty <> customer_invoice_line.quantity or
                                    line.unit_price <> customer_invoice_line.price_unit or
                                    line.vendor_invoice_id.sales_man_id.id <> customer_invoice_line.sales_man_id.id or
                                    line.vendor_invoice_id.sales_man_id.custody_account.id <> customer_invoice_line.account_id.id):

                            #customer_invoice_line.invoice_id.update_info()
                            customer_invoice_line.update({
                            'product_id': line.product_id.id,
                            'uom_id': line.uom_id.id,
                            'quantity': line.product_qty,
                            'price_unit': line.unit_price,
                            'invoice_line_tax_ids': taxes,
                            'tax_amount': ((line.product_qty * line.unit_price) * tax_amt) / 100,
                            'sales_man_id': line.vendor_invoice_id.sales_man_id.id,
                            'account_id': line.vendor_invoice_id.sales_man_id.custody_account.id,
                        })
                            customer_invoice_line.invoice_id.update_info()

                        if ( line.vendor_invoice_id.driver_name <> customer_invoice_line.driver_name or
                                    line.vendor_invoice_id.transmission_number <> customer_invoice_line.transmission_number or
                                    line.vendor_invoice_id.postal_number <> customer_invoice_line.postal_number or
                                    line.vendor_invoice_id.department_id.id <> customer_invoice_line.department_id.id ):

                            customer_invoice_line.write({
                            'driver_name': line.vendor_invoice_id.driver_name,
                            'postal_number': line.vendor_invoice_id.postal_number,
                            'transmission_number': line.vendor_invoice_id.transmission_number,
                            'transmission_date': line.vendor_invoice_id.transmission_date,
                            'department_id': line.vendor_invoice_id.department_id.id
                        })



                    else:  # v83
                        # if not same customer we should delete this line and create new record with new customer
                        # customer_invoice_line.invoice_id.update_info()

                        # remove taxes from account invoice line
                        customer_invoice_line.invoice_line_tax_ids = [(5,)]

                        # save account invoice object to make update after delete the line
                        customer_invoice = customer_invoice_line.invoice_id
                        customer_invoice_line.unlink()
                        customer_invoice[0].write({'state': 'open'})
                        customer_invoice[0].update_info()
                        rec._create_customer_invoice(line)
                else:
                    rec._create_customer_invoice(line)
                    # if line.executed_line:
                    #    rec._create_customer_invoice(line)

            # rec.version_number = rec.version_number + 1
            # rec.write({'edited': False})
	    cr = self.env.cr
	    v_invoice = rec.id
	    sql= (" select vil.vendor_invoice_id as vid from vendors_invoice_line vil \
			 WHERE vil.vendor_invoice_id = %s and executed_line = 'f'\
		")
	    params = (v_invoice,)
	    cr.execute(sql, params)
	    f= cr.dictfetchall()
	    if not ( f and f[0] or False):
		    sql= (" update vendors_invoice set executed_lines = 1\
				 WHERE id = %s")
		    params = (v_invoice,)
		    cr.execute(sql, params)
	    else:
		    sql= (" update vendors_invoice set executed_lines = 0\
				 WHERE id = %s")
		    params = (v_invoice,)
		    cr.execute(sql, params)

            return account_move_id_new

    @api.multi
    def _create_customer_invoice(self, invoice_data):
	print "  RRRRRRRRr  00000000  "
	customer= invoice_data.customer_id
	if customer.active_credit_limit:
		customer_balance= self._get_partner_balance(customer)
		customer_credit_limit = customer.credit_limit or 0.00
                customer_balance += (invoice_data.total_price or 0.00)
		if customer_balance > customer_credit_limit:
			print "WWWWWWWWWWWWWWWWWWWWWWWWW"
			v_invoice= invoice_data.vendor_invoice_id.id
	    		cr = self.env.cr
			sql= (" update vendors_invoice set executed_lines = 0\
				 WHERE id = %s")
			params = (v_invoice,)
			cr.execute(sql, params)
			print "  RRRRRR   11111"
			return False

	print "  RRRRRR   22222222"
        customer_invoice_lines = []
        customer_invoice = self.env['account.invoice'].search(
            [('partner_id', '=', invoice_data.customer_id.id),
             ('date_invoice', '=', invoice_data.vendor_invoice_id.invoice_date)])
        if customer_invoice:
            customer_invoice[0].invoice_line_ids.create({
                'invoice_id': customer_invoice[0].id,
                'vendor_invoice_id': invoice_data.vendor_invoice_id.id,
                'vendor_invoice_line_id': invoice_data.id,
                'product_id': invoice_data.product_id.id,
                'uom_id': invoice_data.uom_id.id,
                'currency_id': invoice_data.currency_id.id,
                'quantity': invoice_data.product_qty - invoice_data.executed_quantity,
                'executed_quantity': invoice_data.product_qty - invoice_data.executed_quantity,
                'price_unit': invoice_data.unit_price,
                'invoice_line_tax_ids': [
                    (4,
                     invoice_data.product_id.taxes_id.id)] if invoice_data.product_id.taxes_id.id and invoice_data.customer_id.has_taxes else False,
                'executed_line': invoice_data.executed_line,
                'driver_name': invoice_data.vendor_invoice_id.driver_name,
                'postal_number': invoice_data.vendor_invoice_id.postal_number,
                'transmission_number': invoice_data.vendor_invoice_id.transmission_number,
                'transmission_date': invoice_data.vendor_invoice_id.transmission_date,
                'sales_man_id': invoice_data.vendor_invoice_id.sales_man_id.id,
                'account_id': invoice_data.vendor_invoice_id.sales_man_id.custody_account.id,
                'department_id': invoice_data.vendor_invoice_id.department_id.id,
            })
            customer_invoice[0]._onchange_invoice_line_ids()
            # customer_invoice[0].update_info()
            invoice_data.executed_line = True
            customer_invoice[0].write({'edited': True,'vegetables': True,})
            customer_invoice[0].write({'state': 'open'})
            customer_invoice[0].update_info()
        else:
            customer_invoice_lines.append((0, 0, {
                'vendor_invoice_id': invoice_data.vendor_invoice_id.id,
                'vendor_invoice_line_id': invoice_data.id,
                'product_id': invoice_data.product_id.id,
                'uom_id': invoice_data.uom_id.id,
                'currency_id': invoice_data.currency_id.id,
                'quantity': invoice_data.product_qty - invoice_data.executed_quantity,
                'executed_quantity': invoice_data.product_qty - invoice_data.executed_quantity,
                'price_unit': invoice_data.unit_price,
                'invoice_line_tax_ids': [
                    (4,
                     invoice_data.product_id.taxes_id.id)] if invoice_data.product_id.taxes_id.id and invoice_data.customer_id.has_taxes else False,
                'executed_line': invoice_data.executed_line,
                'driver_name': invoice_data.vendor_invoice_id.driver_name,
                'postal_number': invoice_data.vendor_invoice_id.postal_number,
                'transmission_number': invoice_data.vendor_invoice_id.transmission_number,
                'transmission_date': invoice_data.vendor_invoice_id.transmission_date,
                'sales_man_id': invoice_data.vendor_invoice_id.sales_man_id.id,
                'account_id': invoice_data.vendor_invoice_id.sales_man_id.custody_account.id,
                'department_id': invoice_data.vendor_invoice_id.department_id.id
            }))
            customer_invoice_id = self.env['account.invoice'].sudo().create({
                'partner_id': invoice_data.customer_id.id,
                'account_id': invoice_data.customer_id.property_account_receivable_id.id,
                'date_invoice': invoice_data.vendor_invoice_id.invoice_date,
                'date_due': invoice_data.vendor_invoice_id.invoice_date,
                'invoice_line_ids': customer_invoice_lines,
                'vegetables': True,
            })
            invoice_data.executed_line = True
            customer_invoice_id._onchange_invoice_line_ids()
            customer_invoice_id.action_invoice_open()
            #customer_invoice_id.update_info()


    def _get_partner_balance(self, partner_id):
        amount = 0
	amount = 0.0
	account = False
	account = partner_id and partner_id.property_account_receivable_id.id or False
	where = ' m.account_id = %s' % str(account) + ' and m.partner_id = %s' % str(partner_id.id)
	if account :
		self._cr.execute(''' select round(sum(m.debit),2) as debit, 
		                round(sum(m.credit),2) as credit, 
		                round(sum(m.debit) - sum(m.credit) ,2) as balance 
		            from account_move_line m , account_move v
		        where m.move_id = v.id and v.state = 'posted' and ''' + where )
	res =  self._cr.dictfetchall()
	if res :
	    amount = res[0]['balance']
	return amount or 0.0

    @api.multi
    def get_taxes_accounts_values(self, select_vender):
        taxes_grouped = {}

        getObj = self.env['vendors.invoice']
        getAllIds = getObj.search([('id', '=', select_vender)])
        for line in getAllIds.invoice_line:
            if line.taxes_id.id not in taxes_grouped:
                taxes_grouped[line.taxes_id.id] = {'taxes_id': line.taxes_id,
                                                   'amount': line.taxes_amount, 'purchase_taxes': line.purchase_taxes}
            else:
                taxes_grouped[line.taxes_id.id]['amount'] += line.taxes_amount
                taxes_grouped[line.taxes_id.id]['purchase_taxes'] += line.purchase_taxes

        return taxes_grouped

        # def _print_report(self):
        #     return self.env['report'].get_action(self, 'vegetables.print_vendor_invoice')


# Invoice Line Class


class VegetablesVendorsInvoiceLine(models.Model):
    _name = 'vendors.invoice.line'
    _order = 'sequence,id'

    @api.depends('unit_price', 'product_qty')
    def _get_total_rice(self):
        for rec in self:
            if rec.product_id:
                if rec.taxes_id:
                    rec.taxes_amount = (rec.taxes_id.amount * (rec.unit_price * rec.product_qty)) / 100
                rec.total_price = rec.taxes_amount + (rec.unit_price * rec.product_qty)

    @api.depends('unit_price', 'product_qty', 'vendor_invoice_id.custody_lines', 'vendor_invoice_id.invoice_line',
                 'vendor_invoice_id.commision_total_amount')
    def _get_purchase_price(self):
        for rec in self:
            custody_total = 0
            total_qty = 0
            total_amount = 0
            purchase_taxes_total = 0
            for line in rec.vendor_invoice_id.invoice_line:
                total_qty += line.product_qty
                total_amount += line.product_qty * line.unit_price

            # update product purchase price
            for line in rec.vendor_invoice_id.invoice_line:
                commission = ((line.product_qty * line.unit_price) * \
                              rec.vendor_invoice_id.commision) / 100
                if line.unit_price <> 0 and line.product_qty <> 0:
                    line.purchase_price = line.unit_price - (commission / line.product_qty) #- \
                                          #(((rec.vendor_invoice_id.custody_total / total_amount) * \
                                           # (line.unit_price * line.product_qty)) / line.product_qty)

                if rec.vendor_invoice_id.vendor_id.has_taxes_vendor:
                    line.purchase_taxes = (line.taxes_id.amount * (line.purchase_price * line.product_qty)) / 100
                    purchase_taxes_total += line.purchase_taxes

            rec.vendor_invoice_id.update({'purchase_taxes_total': purchase_taxes_total})

    @api.depends('taxes_id')
    def _get_taxes_amout(self):
        for rec in self:
            purchase_taxes_total = 0
            if rec.taxes_id:
                rec.taxes_amount = (rec.taxes_id.amount * (rec.unit_price * rec.product_qty)) / 100
            rec.total_price = rec.taxes_amount + (rec.unit_price * rec.product_qty)

            if rec.vendor_invoice_id.vendor_id.has_taxes_vendor:
                for line in rec.vendor_invoice_id.invoice_line:
                    line.purchase_taxes = (line.taxes_id.amount * (line.purchase_price * line.product_qty)) / 100
                    purchase_taxes_total += line.purchase_taxes

            rec.vendor_invoice_id.update({'purchase_taxes_total': purchase_taxes_total})

    @api.depends('product_id')
    def _get_taxes(self):
        for rec in self:
            if rec.product_id.supplier_taxes_id:
                rec.taxes_id = rec.product_id.supplier_taxes_id[0].id

    _description = "Vegetables Vendors Invoice Line"

    @api.multi
    def write(self, vals):
	mess_obj= self.env['mail.message']
	p_obj= self.env['product.product']
	partner_obj= self.env['res.partner']
        message = _("This Line has been modified: <a href=# data-oe-model=vendors.invoice.line data-oe-id=%d>%s</a>") % (self.id, self.id)
        print '///////////////////**********************------------------',self.id
        if vals:
		mess_ids= mess_obj.search([('model','=','vendors.invoice'),('res_id','=',self.vendor_invoice_id.id),('parent_id','=',False)],limit=1)
		dct = {'body': message, 'model': 'vendors.invoice', 'attachment_ids': [], 'res_id': self.vendor_invoice_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False}
		#self.vendor_invoice_id.message_post(body=message)
		lst = []
		if vals.get('product_code',False):
			lst.append([0, 0, {'field': 'product_code', 'field_desc': u'P-Code', 'field_type': 'char', 'new_value_char': vals.get('product_code',False), 'old_value_char': self.product_code}])
		if vals.get('product_id',False):
			nproduct= p_obj.browse(vals.get('product_id',False))
			lst.append([0, 0, {'field': 'product_id', 'field_desc': u'Product', 'field_type': 'many2one', 'new_value_char': nproduct.display_name, 'old_value_char': self.product_id.display_name, 'new_value_integer': vals.get('product_id',False), 'old_value_integer': self.product_id.id}])
		if vals.get('product_qty',False):
			lst.append([0, 0, {'field': 'product_qty', 'field_desc': u'Quantity', 'field_type': 'integer', 'new_value_integer': vals.get('product_qty',False), 'old_value_integer': self.product_qty}])
		if vals.get('unit_price',False):
			lst.append([0, 0, {'field': 'unit_price', 'field_desc': u'Unit Price', 'field_type': 'monetary', 'new_value_monetary': vals.get('unit_price',False), 'old_value_monetary': self.unit_price}])
		if vals.get('customer_code',False):
			lst.append([0, 0, {'field': 'customer_code', 'field_desc': u'C-Code', 'field_type': 'char', 'new_value_char': vals.get('customer_code',False), 'old_value_char': self.customer_code}])
		if vals.get('customer_id',False):
			ncustomer= partner_obj.browse(vals.get('customer_id',False))
			lst.append([0, 0, {'field': 'customer_id', 'field_desc': u'Customer Name', 'field_type': 'many2one', 'new_value_char': ncustomer.display_name, 'old_value_char': self.customer_id.display_name, 'new_value_integer': vals.get('customer_id',False), 'old_value_integer': self.customer_id.id}])
		if lst:
			dct.update({'tracking_value_ids':lst})
			mess_obj.create(dct)
			mess_obj.create({'body': message, 'model': 'vendors.invoice', 'attachment_ids': [], 'res_id': self.vendor_invoice_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False})
		
        res = super(VegetablesVendorsInvoiceLine, self).write(vals)		        
        return res


    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Invoice Reference', required=True,
                                        ondelete='cascade', index=True, copy=False)
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id)
    customer_id = fields.Many2one('res.partner', string='Customer Name', required=True,
                                  domain=['&',('is_market', '=', True),('customer', '=', True)])

    customer_code = fields.Char('C-Code')

    employee= fields.Many2one('hr.employee')

    product_id = fields.Many2one('product.product', string='Product',domain=[('is_market_product', '=', True)],required=True)
    product_code = fields.Char('P-Code')
    # uom_categ_id = fields.Many2one('product.uom.categ', string="Unit Category", related='product_id.uom_categ_id')
    uom_id = fields.Many2one('product.uom', string='Unit of Measure',
                             ondelete='set null', index=True, oldname='uos_id')
    product_qty = fields.Integer(string='Quantity', required=True, default=1.0)
    unit_price = fields.Monetary('Unit Price')
    taxes_id = fields.Many2one('account.tax', string='Taxes', compute='_get_taxes')
    taxes_amount = fields.Monetary('Tax Amount', compute='_get_taxes_amout')
    total_price = fields.Monetary('Sub Total', store=True, compute='_get_total_rice')
    purchase_price = fields.Float('Purchase Price', compute='_get_purchase_price',digits=(16,5))
    purchase_taxes = fields.Monetary('Purchase Taxes', compute='_get_purchase_price')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id.id)
    executed_quantity = fields.Float("Executed Quantity", default=0, copy=False)
    executed_line = fields.Boolean("Executed", default=False, copy=False)
    edit_product_id = fields.Boolean(default=False, copy=False)
    state = fields.Selection([('unpost', 'Unpost'), ('posted', 'Posted')], string='Status',
                             related='vendor_invoice_id.state', default='unpost')
    sequence = fields.Integer(default=10,
        help="Gives the sequence of this line when displaying the invoice.")

    # @api.onchange('uom_id')
    # def onchange_uom_id(self):
    #     if self.uom_id and self.product_id:
    #         self.unit_price = self.product_id.uom_id._compute_price(self.product_id.list_price, self.uom_id)


    @api.multi
    def unlink(self):
        counter = 0
        for line in self:
            # deleted record is ==> customer_invoice_line
            customer_invoice_line = self.env['account.invoice.line'].search([('vendor_invoice_line_id', '=', line.id)])
            if customer_invoice_line:
                account_invoice = customer_invoice_line.invoice_id
                ref_value = account_invoice.sequence_number
                account_move = self.env['account.move'].search([('ref', '=', ref_value)])
                '''deleted_amount=line.total_price#line.unit_price*line.product_qty


                #update account.invoice

                #update account.move
                account_move = self.env['account.move'].search([('ref', '=', ref_value)])
                account_move.update({'credit_amount':account_move.credit_amount-deleted_amount,
                                    'partner_id':account_invoice.partner_id.id,
                                    'amount':account_move.amount-deleted_amount})
                #update account move line
                move_lineee = depit_list = credit_list = tax_list =[]

                account_move_line = self.env['account.move.line'].search([('ref', '=', ref_value)])
                ch=0
                for move_line in account_move_line:
                    ch=ch+1
                if ch == 2:#means 2 line in account move line
                    for move_line in account_move_line:
                        if move_line.debit:
                            move_lineee.append((0, 0,{



                                                    'company_id': move_line.company_id.id,
                                                    'account_id':move_line.account_id.id,
                                                    'partner_id':move_line.partner_id.id,
                                                    'debit':move_line.debit-deleted_amount,
                                                    'debit_cash_basis':move_line.debit_cash_basis-deleted_amount,
                                                    'balance_cash_basis':move_line.balance_cash_basis-deleted_amount,
                                                'balance':move_line.balance-deleted_amount,
                                                        }))

                        if move_line.credit :
                            move_lineee.append((0, 0,{



                                                'company_id': move_line.company_id.id,
                                                'account_id':move_line.account_id.id,
                                                'partner_id':move_line.partner_id.id,
                                                'credit':move_line.credit-deleted_amount,
                                                'credit_cash_basis':move_line.credit_cash_basis-deleted_amount,
                                                'balance_cash_basis':move_line.balance_cash_basis+deleted_amount,
                                                'balance':move_line.balance+deleted_amount,
                                                        }))
                else: # there is 3 line in account move line
                    ch=0
                    for move_line in account_move_line:
                        ch=ch+1
                        #print'hhh...................................',move_line
                        if move_line.tax_line_id:#ch == 1:
                            move_lineee.append((0, 0,{



                                                    'company_id': move_line.company_id.id,
                                                    'account_id':move_line.account_id.id,
                                                    'partner_id':move_line.partner_id.id,
                                                    'credit':move_line.credit-line.taxes_amount,
                                                    'balance':move_line.balance+line.taxes_amount,
                                                    'tax_line_id':move_line.tax_line_id.id
                                                        }))

                        move_line_dic=[]
                        if move_line.debit:
                            move_lineee.append((0, 0,{
                                                    'company_id': move_line.company_id.id,
                                                    'account_id':move_line.account_id.id,
                                                    'partner_id':move_line.partner_id.id,
                                                    'debit':move_line.debit-deleted_amount,
                                                    'amount_residual':move_line.amount_residual-deleted_amount,
                                                    'balance':move_line.balance-deleted_amount,
                                                        }))

                        if move_line.credit and not move_line.tax_line_id:
                            move_lineee.append((0, 0,{
                                                'company_id': move_line.company_id.id,
                                                'account_id':move_line.account_id.id,
                                                'partner_id':move_line.partner_id.id,
                                                'credit':move_line.credit-line.unit_price*line.product_qty,
                                                'balance':move_line.balance+line.unit_price*line.product_qty,
                                                        }))
                #print'm11........................................'
                account_invoice.move_id.line_ids.unlink() 
                #print'm222...........................................'
                '''
                account_invoice[0].write({'edited': True})
                num_of_lines_in_account_inv = self.env['account.invoice.line'].search(
                    [('invoice_id', '=', customer_invoice_line.invoice_id.id)])

                for num_of_lines in num_of_lines_in_account_inv:
                    counter = counter + 1
                # if counter > 1:
                #    #print'm33....................................',move_lineee
                #    account_invoice.move_id.update({'line_ids': move_lineee})
                #    #print'm444..........................................'

                if counter <= 1:  # delete acccount invoice
                    account_invoice.write({'state': 'draft'})
                    account_invoice.write({'move_name': ''})
                    account_invoice.unlink()

                    account_move.unlink()

        return super(VegetablesVendorsInvoiceLine, self).unlink()

    @api.onchange('product_code')
    def onchange_product_code(self):
        if self.product_code:
            product = self.env['product.product'].sudo().search([('default_code', '=', self.product_code),('is_market_product', '=', True)])
            if product:
                self.product_id = product.id

    @api.onchange('product_id')
    def _onchange_product_id(self):
        domain = {}
        #self.customer_id = self.vendor_invoice_id.sales_man_id.linked_customer
        if self.company_id.sale_customer_rel :
            self.customer_id = self.vendor_invoice_id.sales_man_id.linked_customer

        if self.product_id:
            self.product_code = self.product_id.default_code
            if self.product_id.supplier_taxes_id:
                self.taxes_id = self.product_id.supplier_taxes_id[0].id
                self.taxes_amount = (self.taxes_id.amount * (self.unit_price * self.product_qty)) / 100
            else:
                self.taxes_amount = 0
            self.uom_id = self.product_id.uom_id.id
            domain['uom_id'] = [('category_id', '=', self.product_id.uom_categ_id.id)]

        return {'domain': domain}

    @api.onchange('customer_code')
    def onchange_customer_code(self):
        if self.customer_code:
            self.customer_id = []
            customer = self.env['res.partner'].sudo().search(
                [('code', '=', self.customer_code), ('customer', '=', True),('is_market', '=', True)])

            if customer:
                self.customer_id = customer.id

    @api.onchange('customer_id')
    def onchange_customer_id(self):
        if self.customer_id:
            self.customer_code = self.customer_id.code


# Custody Line Class


class VegetablesCustodyLine(models.Model):
    _name = 'vendors.custody.line'
    _description = "Vegetables Custody Line"

    @api.multi
    def write(self, vals):
	mess_obj= self.env['mail.message']
	a_obj= self.env['account.account']
        message = _("This Line has been modified: <a href=# data-oe-model=vendors.custody.line data-oe-id=%d>%s</a>") % (self.id, self.id)
        if vals:
		mess_ids= mess_obj.search([('model','=','vendors.invoice'),('res_id','=',self.vendor_invoice_id.id),('parent_id','=',False)],limit=1)
		dct = {'body': '', 'model': 'vendors.invoice', 'attachment_ids': [], 'res_id': self.vendor_invoice_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False}
		lst = []
		if vals.get('cash_account',False):
			nproduct= a_obj.browse(vals.get('cash_account',False))
			lst.append([0, 0, {'field': 'cash_account', 'field_desc': u'Cash Account', 'field_type': 'many2one', 'new_value_char': nproduct.display_name, 'old_value_char': self.cash_account.display_name, 'new_value_integer': vals.get('cash_account',False), 'old_value_integer': self.cash_account.id}])
		if vals.get('credit_account',False):
			nproduct= a_obj.browse(vals.get('credit_account',False))
			lst.append([0, 0, {'field': 'credit_account', 'field_desc': u'Credit Account', 'field_type': 'many2one', 'new_value_char': nproduct.display_name, 'old_value_char': self.credit_account.display_name, 'new_value_integer': vals.get('credit_account',False), 'old_value_integer': self.credit_account.id}])
		if vals.get('cash_amount',False):
			lst.append([0, 0, {'field': 'cash_amount', 'field_desc': u'Cash Amount', 'field_type': 'integer', 'new_value_integer': vals.get('cash_amount',False), 'old_value_integer': self.cash_amount}])
		if vals.get('credit_amount',False):
			lst.append([0, 0, {'field': 'credit_amount', 'field_desc': u'Credit Amount', 'field_type': 'integer', 'new_value_integer': vals.get('credit_amount',False), 'old_value_integer': self.credit_amount}])
		if lst:
			dct.update({'tracking_value_ids':lst})
			mess_obj.create(dct)
			mess_obj.create({'body': message, 'model': 'vendors.invoice', 'attachment_ids': [], 'res_id': self.vendor_invoice_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False})
        res = super(VegetablesCustodyLine, self).write(vals)		        
        return res

    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Invoice Reference', required=True,
                                        ondelete='cascade', index=True, copy=False)
    custody_name = fields.Char("Custody Name", required=True)
    custody_type = fields.Selection([('is_cash', 'IS Cash'), ('is_credit', 'IS Credit')],
                                    string='Custody Type', default='is_cash')
    sales_man = fields.Selection([('salesman', 'Sales Man'), ('supervisor', 'Supervisor')],
                                 string='Sales Man Type', default='salesman')
    account = fields.Many2one('account.account', string='Account')
    cash_account = fields.Many2one('account.account', string='Cash Account')
    credit_account = fields.Many2one('account.account', string='Credit Account')
    cash_amount = fields.Integer('Cash Amount')
    credit_amount = fields.Integer('Credit Amount')
    amount = fields.Float('Amount')
    partner_id = fields.Many2one('res.partner', 'partner')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id.id)
    is_rental = fields.Boolean('IS Rental')
    is_complex = fields.Boolean('Is Complex')
    is_card = fields.Boolean('is_card')
    

    @api.onchange('cash_amount')
    def _onchange_cash_amount(self):
        if self.cash_amount > 0:
            self.credit_amount = 0
            self.credit_account = []
            if self.sales_man == 'salesman':
                self.cash_account = self.vendor_invoice_id.sales_man_id.cash_account.id
            else:
                self.cash_account = self.vendor_invoice_id.supervisor_id.cash_account.id

    @api.onchange('credit_amount')
    def _onchange_credit_amount(self):
        if self.credit_amount > 0:
            self.cash_amount = 0
            self.cash_account = []
            if self.is_rental:
                self.credit_account = self.vendor_invoice_id.vendor_id.expense_account.id
            else:
                self.credit_account = self.account.id



class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'

    allow_custom_commision = fields.Boolean(related='company_id.allow_custom_commision', 
        string="Allow Custom Commission")

class ResCompany(models.Model):
    _inherit = 'res.company'

    allow_custom_commision = fields.Boolean("Allow Custom Commission", default=False, copy=False)


class AccountMove(models.Model):
    _inherit = "account.move"

    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Vendor Invoice Reference',
                                        ondelete='cascade', index=True, copy=False)


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Vendor Invoice Reference', index=True, copy=False)
    vendor_invoice_line_id = fields.Many2one('vendors.invoice.line', string='Vendor Invoice Line Reference', index=True, copy=False)

class Employee(models.Model):

    _inherit = "hr.employee"

    is_amount_debit = fields.Boolean(string='is Amount Debit?')
    amount_debit = fields.Float(string='Amount Debit')

class InvoiceNumbersLines(models.Model):
    _inherit = 'manual.invoice.number.lines'

    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Vendor Invoice Reference', index=True, copy=False)


class InvoicesUpdateNumber(models.Model):
    _inherit = 'invoices.update.number'

    @api.multi
    def get_numbers(self):
	for rec in self:
		n_obj = self.env['manual.invoice.number.lines']
		vi_obj = self.env['vendors.invoice']
		n_ids= n_obj.search([('number','=',rec.number),('manual_number_id.delegate_id','=',rec.sales_man_id.id),('vendor_invoice_id','=',False),('manual_number_id.state','=','True')])
		p_ids= []
		for n in n_ids:
			if not vi_obj.search([('transmission_number','=',rec.number),('manual_invoice_number','=',n.manual_number_id.id)]):
				p_ids.append(n.manual_number_id.id)
		n_ids= n_obj.search([('number','=',rec.number),('manual_number_id.for_all_delegates','=',True),('vendor_invoice_id','=',False),('manual_number_id.state','=','True')])
		for n in n_ids:
			if not vi_obj.search([('transmission_number','=',rec.number),('manual_invoice_number','=',n.manual_number_id.id)]):
				p_ids.append(n.manual_number_id.id)
		if p_ids:
			rec.number_ids = [(6,0,p_ids)]
		else:
			rec.number_ids = [(6,0,[])]


    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Vendor Invoice Reference', index=True, copy=False)
    compute_check= fields.Integer(compute='compute_manual_number_id', store= True, string='Testing', index=True, copy=False)
    number_ids= fields.Many2many('manual.invoice.number', 'number_invoices_update_rel', 'number_id', 'update_id', compute='get_numbers', string='numbers')


    @api.depends('manual_number_id','vendor_invoice_id')
    def compute_manual_number_id(self):
	if self.vendor_invoice_id:
		if self.manual_number_id:
        		self.env.cr.execute("""update  vendors_invoice set manual_invoice_number='%s' where id=%s"""%(self.manual_number_id.id,self.vendor_invoice_id.id))
		else:
        		self.env.cr.execute("""update  vendors_invoice set manual_invoice_number=null where id=%s"""%(self.vendor_invoice_id.id))
	self.compute_check= 1
