# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class Employee(models.Model):
    _inherit = 'hr.employee'

    code = fields.Char('Code')
    is_sales_man = fields.Boolean('IS Sales Man', default=False)
    is_sales_manger = fields.Boolean('IS Sales Manger', default=False)
    linked_customer = fields.Many2one("res.partner", string='Customer', domain=[('customer', '=', True)])
    expense_partner = fields.Many2one('res.partner', string='Expense Partner')
    card_partner = fields.Many2one('res.partner', string='Card Partner')
    custody_account = fields.Many2one('account.account', string='Custody Account')
	#,domain=['|', ('user_type_id.name', '=', 'Current Assets'),('user_type_id.name', '=', 'الأصول المتداولة')]
    cash_account = fields.Many2one('account.account', string='Cash Account')
    # domain=['|', ('user_type_id.name', '=', 'Receivable'),
                                                #    ('user_type_id.name', '=', 'الدائنون')]

    commission_account = fields.Many2one('account.account', string='Commission Account', required=False,
                                      domain=['|', ('user_type_id.name', '=', 'Income'),
                                      ('user_type_id.name', '=', 'الدخل')])

    supervisor = fields.Many2one("hr.employee", string='Supervisor', domain=[('is_sales_manger', '=', True)])



    is_market_employee = fields.Boolean(string='Agricultural Market', default=True)
    is_vegetable_employee = fields.Boolean(string='Is Vegetable')

    partner_id = fields.Many2one('res.partner', string='Partner')
    is_amount_debit = fields.Boolean(string='is Amount Debit?')
    amount_debit = fields.Float(string='Amount Debit')


    @api.multi
    def create_partner(self):
	partner_obj= self.env['res.users']
	for rec in self:
		user_id= partner_obj.create({
			'name': rec.name,
			'login': rec.name,
			})
		rec.user_id= user_id.id
		rec.partner_id= user_id.partner_id.id
		user_id.active= False

