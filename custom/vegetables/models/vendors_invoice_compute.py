# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError, Warning


class VendorsInvoice(models.Model):
    _name = 'vendors.invoice'
    _inherit = ['mail.thread']
    _description = "Vendor Invoice"
    _order = 'invoice_date desc , id desc'

    def _get_custoudies_data(self):
        custodies = self.env['vegetables.custody'].search([], order='sequence')
        custody_lines = self.env['vendors.custody.line']
        for custody in custodies:
            custody_lines |= custody_lines.new({
                'vendor_invoice_id': self.id,
                'custody_name': custody.custody_name,
                'account': custody.credit_account.id,
                'custody_type': custody.custody_type,
                'sales_man': custody.sales_man,
                'is_rental': custody.is_rental,
                'is_complex': custody.is_complex,
            })
        return custody_lines

    @api.depends('write_text')
    def _compute_all_func_fields(self):
        print "-------------------------_compute_all_func_fields----",self
        for rec in self:
            sum_purchase_taxes=0
            total = sum_custody = total_tax = customer_total = customer_total_untaxed = 0
            for line in rec.invoice_line:
                total += line.total_price
                total_tax += line.taxes_amount
                if rec.sales_man_id.linked_customer.id == line.customer_id.id:
                    customer_total += line.total_price
                    customer_total_untaxed += line.product_qty * line.unit_price
                if line.vendor_invoice_id.vendor_id.has_taxes:
                    sum_purchase_taxes += line.purchase_taxes

            cash_custody = 0
            credit_custody = 0
            #rec.credit_sales = (rec.total_taxed_amount + rec.untaxes_total) - rec.main_customer_total_amount
            for custody in rec.custody_lines:
                cash_custody += custody.cash_amount
                credit_custody += custody.credit_amount

            rec.cash_custody = cash_custody
            rec.credit_custody = credit_custody
            rec.custody_total = cash_custody + credit_custody

            #for custody_line in rec.custody_lines:
            #    sum_custody += custody_line.credit_amount + custody_line.cash_amount
            #for line in rec.invoice_line:
            #    if line.vendor_invoice_id.vendor_id.has_taxes:
            #        sum_purchase_taxes += line.purchase_taxes
            rec.purchase_taxes_total=sum_purchase_taxes
            untaxes_total = total - total_tax
            rec.untaxes_total = total - total_tax
            rec.amount_total = untaxes_total - (untaxes_total * rec.commision / 100) - (cash_custody + credit_custody)
            rec.total_taxed_amount = total_tax
            rec.main_customer_total_amount = customer_total
            rec.main_customer_total_untaxed = customer_total_untaxed
            rec.credit_sales = (total_tax + untaxes_total) - customer_total
            rec.net_total = sum_purchase_taxes + (untaxes_total - (untaxes_total * rec.commision / 100) - (cash_custody + credit_custody))
            rec.commision_total_amount = untaxes_total * rec.commision / 100
            print ("-------------rec.purchase_taxes_total",rec.purchase_taxes_total,rec.untaxes_total,rec.amount_total)
            print ("-------------rec.total_taxed_amount",rec.total_taxed_amount,rec.main_customer_total_amount,rec.main_customer_total_untaxed)
            print ("-------------rec.credit_sales",rec.credit_sales,rec.net_total,rec.commision_total_amount)

    #@api.depends('invoice_line', 'custody_lines', 'invoice_line.purchase_price')
    @api.depends('invoice_line', 'custody_total', 'invoice_line.purchase_price')
    def _get_total_invoice(self):
        print "-------------------------_get_total_invoice----",self
        for rec in self:
            sum_purchase_taxes=0
            total = sum_custody = total_tax = customer_total = customer_total_untaxed = 0
            for line in rec.invoice_line:
                total += line.total_price
                total_tax += line.taxes_amount
                if rec.sales_man_id.linked_customer.id == line.customer_id.id:
                    customer_total += line.total_price
                    customer_total_untaxed += line.product_qty * line.unit_price
                if line.vendor_invoice_id.vendor_id.has_taxes:
                    sum_purchase_taxes += line.purchase_taxes

            #for custody_line in rec.custody_lines:
            #    sum_custody += custody_line.credit_amount + custody_line.cash_amount
            #for line in rec.invoice_line:
            #    if line.vendor_invoice_id.vendor_id.has_taxes:
            #        sum_purchase_taxes += line.purchase_taxes
            rec.purchase_taxes_total=sum_purchase_taxes
            untaxes_total = total - total_tax
            rec.untaxes_total = total - total_tax
            rec.amount_total = untaxes_total - (untaxes_total * rec.commision / 100) - rec.custody_total
            rec.total_taxed_amount = total_tax
            rec.main_customer_total_amount = customer_total
            rec.main_customer_total_untaxed = customer_total_untaxed
            rec.credit_sales = (total_tax + (total - total_tax)) - customer_total
            rec.net_total = sum_purchase_taxes + (total - total_tax - ((total - total_tax) * rec.commision / 100) - rec.custody_total)
            rec.commision_total_amount = (total - total_tax) * rec.commision / 100
            print ("-------------rec.purchase_taxes_total",rec.purchase_taxes_total,rec.untaxes_total,rec.amount_total)
            print ("-------------rec.total_taxed_amount",rec.total_taxed_amount,rec.main_customer_total_amount,rec.main_customer_total_untaxed)
            print ("-------------rec.credit_sales",rec.credit_sales,rec.net_total,rec.commision_total_amount)

    @api.depends('vendor_id')
    def _get_salesman_commission(self):
        for rec in self:
            if rec.vendor_id:
                rec.commision = rec.vendor_id.supplier_commission

    @api.depends('purchase_taxes_total', 'amount_total')
    def _get_net_total(self):
        for rec in self:
            rec.net_total = rec.purchase_taxes_total + rec.amount_total

    @api.depends('untaxes_total', 'commision')
    def _get_commision_total_amount(self):
        for rec in self:
            rec.commision_total_amount = rec.untaxes_total * rec.commision / 100

    @api.depends('untaxes_total', 'commision', 'custody_total')
    def _get_amount_total(self):
        for rec in self:
            rec.amount_total = rec.untaxes_total - (rec.untaxes_total * rec.commision / 100) - rec.custody_total

    #@api.depends('untaxes_total', 'main_customer_total_amount', 'commision_total_amount', 'custody_lines',
    #             'total_taxed_amount')
    @api.depends('custody_lines')
    def _get_custody_and_credit_invoice_total(self):
        print "--------------_get_custody_and_credit_invoice_total", self
        for rec in self:
            cash_custody = 0
            credit_custody = 0
            #rec.credit_sales = (rec.total_taxed_amount + rec.untaxes_total) - rec.main_customer_total_amount
            for custody in rec.custody_lines:
                cash_custody += custody.cash_amount
                credit_custody += custody.credit_amount

            rec.cash_custody = cash_custody
            rec.credit_custody = credit_custody
            rec.custody_total = rec.cash_custody + rec.credit_custody
    
    @api.depends('vendor_id')
    def _get_vendor_sequence(self):
        for rec in self:
            all_vendor_invoices = self.search(
                [('vendor_id', '=', rec.vendor_id.id)], order="invoice_date")
            max_seq = max([x.vendor_sequence for x in all_vendor_invoices])
            rec.vendor_sequence = max_seq+1
            counter = 1
            


    name = fields.Char(string='Invoice No.', required=True, copy=False, readonly=True, index=True,
                       default=lambda self: _('New'))
    invoice_date = fields.Date(string='Invoice Date', required=True, default=fields.Date.today)
    commision = fields.Float(String="Commision", compute='_get_salesman_commission')
    #commision_total_amount = fields.Monetary('Commision Amount', compute='_get_commision_total_amount',store=True)
    commision_total_amount = fields.Monetary('Commision Amount', compute='_compute_all_func_fields',default=lambda self: self._get_total_invoice(),store=True)
    vendor_id = fields.Many2one('res.partner', string='Vendor Name', domain=[('supplier', '=', True)])
    vendor_code = fields.Char(string='Vendor Code')
    
    department_id = fields.Many2one('stock.warehouse', string='Department', required=True,
                                    default=lambda self: self.env.user.company_id.default_department_id)
    sales_man_id = fields.Many2one('hr.employee', string='Sales Man', required=True,
                                   domain=[('is_sales_man', '=', True)])

    sales_man_is_manger = fields.Boolean(related='sales_man_id.is_sales_manger')

    supervisor_id = fields.Many2one('hr.employee', string='Supervisor', domain=[('is_sales_manger', '=', True)])

    driver_name = fields.Char('Driver')
    postal_number = fields.Char('Postal Number')
    transmission_number = fields.Char('Transmission Number')
    transmission_date = fields.Date(string='Transmission Date')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id.id)

    #amount_total = fields.Monetary(string='Total', compute='_get_total_invoice',default=lambda self: self._get_total_invoice(), store=True)
    amount_total = fields.Monetary(string='Total', compute='_compute_all_func_fields', store=True)
    purchase_taxes_total = fields.Monetary(string='Purchase Tax')
    #net_total = fields.Monetary(string='Net Total', store=True, compute='_get_net_total')
    net_total = fields.Monetary(string='Net Total', store=True, compute='_compute_all_func_fields')
    #total_taxed_amount = fields.Monetary(string='Total Taxes', store=True, compute='_get_total_invoice')
    total_taxed_amount = fields.Monetary(string='Total Taxes', store=True, compute='_compute_all_func_fields')
    #untaxes_total = fields.Monetary('Untaxed Total', store=True, compute='_get_total_invoice')
    #main_customer_total_amount = fields.Monetary('Main Customer Total', compute='_get_total_invoice', default=lambda self: self._get_total_invoice(),store=True)
    #main_customer_total_untaxed = fields.Monetary('Main Customer Untaxed', compute='_get_total_invoice', default=lambda self: self._get_total_invoice(),store=True)
    untaxes_total = fields.Monetary('Untaxed Total', store=True, compute='_compute_all_func_fields')
    main_customer_total_amount = fields.Monetary('Main Customer Total', compute='_compute_all_func_fields',store=True)
    main_customer_total_untaxed = fields.Monetary('Main Customer Untaxed', compute='_compute_all_func_fields',store=True)

    state = fields.Selection([('unpost', 'Unpost'), ('posted', 'Posted')], 'Status', default='unpost')

    invoice_line = fields.One2many('vendors.invoice.line', 'vendor_invoice_id', string='Invoice Lines', copy=True)

    custody_lines = fields.One2many('vendors.custody.line', 'vendor_invoice_id', string='Custody Lines',
                                    default=_get_custoudies_data, copy=True)
    account_move_id = fields.Many2one('account.move', string='Journal Entry', copy=False)

    executed = fields.Boolean("executed", default=False, copy=False)

    edited = fields.Boolean('Modified', default=False, copy=False)

    #cash_custody = fields.Float(string="Cash Custody Total", compute='_get_custody_and_credit_invoice_total', store=True)
    #credit_custody = fields.Float(string="Cash Custody Total", compute='_get_custody_and_credit_invoice_total', store=True)
    cash_custody = fields.Float(string="Cash Custody Total", compute='_compute_all_func_fields', store=True)
    credit_custody = fields.Float(string="Credit Custody Total", compute='_compute_all_func_fields', store=True)
    #credit_sales = fields.Float(string="Cash Custody Total", compute='_get_custody_and_credit_invoice_total', default=lambda self: self._get_custody_and_credit_invoice_total(),store=True)
    #credit_sales = fields.Float(string="Cash Custody Total", compute='_get_total_invoice',store=True)
    credit_sales = fields.Float(string="Cash Custody Total", compute='_compute_all_func_fields',store=True)
    #custody_total = fields.Monetary(string="Custody Total", compute='_get_custody_and_credit_invoice_total',store=True)
    custody_total = fields.Monetary(string="Custody Total", compute='_compute_all_func_fields',store=True)
    vendor_sequence = fields.Integer(string="Vendor Sequence", store=True, compute='_get_vendor_sequence')
    
    version_number = fields.Integer(string="Version Number",store=True ,default=1) 
    write_text = fields.Text('Text')

    @api.onchange('vendor_id', 'department_id')
    def onchange_vendor_id(self):

        if self.vendor_id and self.department_id:
            for department in self.vendor_id.department_ids:
                if department.department_id.id == self.department_id.id:
                    self.sales_man_id = department.sales_man_id.id
        if self.vendor_id:
            self.vendor_code = self.vendor_id.code

        # get account for credit custody
        for custody_line in self.custody_lines:
            # if is rental costudy
            if self.vendor_id.expense_account:
                if custody_line.is_rental:
                    custody_line.credit_account = self.vendor_id.expense_account.id
            # if is normal custody
            if custody_line.custody_type == 'is_credit' and custody_line.is_rental == False:
                custody_line.credit_account = custody_line.account.id

    @api.onchange('sales_man_id')
    def onchange_sales_man_id(self):

        # check if sales man is not supervisor set supervisor
        if not self.sales_man_id.is_sales_manger:
            self.supervisor_id = self.sales_man_id.supervisor
        else:
            self.supervisor_id = False

        # if costody is cash
        if self.sales_man_id.cash_account:
            for custody_line in self.custody_lines:
                # custody type salesman account from salesman
                if custody_line.sales_man == 'salesman':
                    if custody_line.custody_type == 'is_cash':
                        custody_line.cash_account = self.sales_man_id.cash_account.id
                # custody type supervisor account from supervisor
                else:
                    if self.supervisor_id.cash_account:
                        if custody_line.custody_type == 'is_cash':
                            custody_line.cash_account = self.supervisor_id.cash_account.id

    @api.onchange('vendor_code')
    def onchange_vendor_code(self):
        if self.vendor_code:
            vendor = self.env['res.partner'].sudo().search([('code', '=', self.vendor_code), ('supplier', '=', True)])
            if vendor:
                self.vendor_id = vendor.id

    @api.model
    def create(self, vals):
        if not vals.get('invoice_line'):
            raise UserError(_('You cannot save this invoice. please add product.'))
        # if vals.get('name', _('New')) == _('New'):
        vals['name'] = self.env['ir.sequence'].next_by_code('vendors.invoice')
        if vals.get('department_id'):
            self.env.user.company_id.default_department_id = self.env["stock.warehouse"].browse(
                vals.get('department_id')).id
        result = super(VendorsInvoice, self).create(vals)
        return result

    @api.multi
    def write(self, values):

        if values:
            record = self.env["vendors.invoice"].browse(self.id)
            # below code to minus sequence when change vendor 
            if 'vendor_id' in values:
                old_partner = self.env['res.partner'].search([('id', '=', record.vendor_id.id)])
                if record.vendor_id.vendor_invoice_sequence - 1 >= 0 :
                    old_partner.update({'vendor_invoice_sequence': record.vendor_id.vendor_invoice_sequence - 1})


            if not "edited" in values and record.state == 'posted':
                values['edited'] = True

        return super(VendorsInvoice, self).write(values)


    @api.multi
    def unlink(self):
        for rec in self:
            counter=0
            for line in rec.invoice_line:
                line.unlink()

                '''customer_invoice_line = self.env['account.invoice.line'].search([('vendor_invoice_line_id', '=', line.id)])
                num_of_lines_in_account_inv=self.env['account.invoice.line'].search([('invoice_id', '=', customer_invoice_line.invoice_id.id)])
                for num_of_lines in num_of_lines_in_account_inv:
                    counter=counter+1
                if counter <= 1:
                    customer_invoice_line.invoice_id.write({'state': 'draft'})
                    customer_invoice_line.invoice_id.write({'move_name': ''})
                    customer_invoice_line.invoice_id.unlink()
                '''
            rec.account_move_id.unlink()
        res = super(VendorsInvoice, self).unlink()
        return res

    @api.constrains('custody_lines')
    def _check_custody_lines(self):
        for line in self.custody_lines:
            if (line.credit_amount > 0 and not line.credit_account) or (
                    line.cash_amount > 0 and not line.cash_account):
                raise ValidationError(_('Please select account for custody ') + line.custody_name)

    @api.constrains('invoice_line')
    def _check_invoice_line(self):
        customers_grouped = {}
        for line in self.invoice_line:
            if line.customer_id.id not in customers_grouped:
                customers_grouped[line.customer_id.id] = {'customer_id': line.customer_id,
                                                          'amount': line.product_qty * line.unit_price}
            else:
                customers_grouped[line.customer_id.id]['amount'] += line.product_qty * line.unit_price

        for customer_line in customers_grouped:
            if customers_grouped[customer_line]['customer_id'].credit + customers_grouped[customer_line]['amount'] >\
                    customers_grouped[customer_line]['customer_id'].credit_limit:

                raise ValidationError(customers_grouped[customer_line]['customer_id'].name +
                                      _(' has exceeded the limit of credential..! '))

    @api.one
    def post(self):
        for rec in self:
            if rec.state == 'posted':
                raise ValidationError(_('There is no journal items in draft state to post.'))

            posted = rec.env['account.move'].sudo().search([(
                'ref','=', rec.name
            )])

            if not posted :

                transmission_number = ''
                driver_name = ''
                postal_number = ''
                transmission_date = ''

                if rec.transmission_number :
                    transmission_number = str(transmission_number)
                if rec.driver_name :
                    driver_name = str(driver_name)
                if rec.postal_number :
                    postal_number = str(postal_number)
                if rec.transmission_date :
                    transmission_date = str(transmission_date)



                journal_id = rec.env['account.journal'].sudo().search(
                    [('id', '=', rec.env.ref('vegetables.vegetable_vendor_journal').id)])
                if not rec.sales_man_id.custody_account:
                    raise ValidationError(_('You should link custody account in sales man page '))
                for custody_line in rec.custody_lines:
                    if (custody_line.cash_amount > 0 and not custody_line.cash_account) or (
                            custody_line.credit_amount > 0 and not custody_line.credit_account):
                        raise ValidationError(_('You should select Account in ' + custody_line.custody_name))

                for product_line in rec.invoice_line:
                    if product_line.unit_price <= 0:
                        raise ValidationError(_(product_line.product_id.name + ' has no sale price!'))

                sum_custody = 0
                for custody_line in rec.custody_lines:
                    sum_custody += custody_line.credit_amount + custody_line.cash_amount

                vendor_balance = rec.amount_total  # - sum_custody - rec.commision_total_amount - rec.total_taxed_amount

                # Generate Account Move Line #vh
                move_line = []

                for custody_line in rec.custody_lines:

                    if custody_line.credit_account and custody_line.credit_amount > 0:
                        move_line.append((0, 0, {
                            'account_id': custody_line.credit_account.id,
                            'name': custody_line.custody_name,
                            'credit': custody_line.credit_amount,
                            'partner_id': rec.vendor_id.id
                        }))

                    if custody_line.cash_account and custody_line.cash_amount > 0:
                        if rec.sales_man_id.linked_customer.id:
                            move_line.append((0, 0, {
                                'account_id': rec.sales_man_id.linked_customer.property_account_receivable_id.id,
                                'name': custody_line.custody_name,
                                'credit': custody_line.cash_amount,
                                'partner_id': rec.sales_man_id.linked_customer.id

                            }))
                        else:
                            move_line.append((0, 0, {
                                'account_id': custody_line.cash_account.id,
                                'name': custody_line.custody_name,
                                'credit': custody_line.cash_amount,
                                'partner_id': rec.vendor_id.id
                            }))

                if rec.vendor_id.has_taxes:
                    select_vender=rec.id
                    for line in self.get_taxes_accounts_values(select_vender).values():
                        if line['taxes_id'].account_id and float(line['amount']) > 0 and line[
                            'taxes_id'].account_id and float(line['purchase_taxes']) > 0:
                            vendor_balance += line['purchase_taxes']

                if vendor_balance > 0:

                    move_line.append((0, 0, {
                        'account_id': rec.vendor_id.property_account_payable_id.id,
                        'name':  _('Transmission Number: ') + transmission_number\
                                + _(' Driver: ') + driver_name \
                                + _(' Postal Number: ') + postal_number \
                                + _(' Transmission Date: ') + transmission_date,
                        'credit': vendor_balance,
                        'partner_id': rec.vendor_id.id
                    }))
                elif vendor_balance < 0:
                    move_line.append((0, 0, {
                        'account_id': rec.vendor_id.property_account_payable_id.id,
                        'name':  _('Transmission Number: ') + transmission_number\
                                + _(' Driver: ') + driver_name \
                                + _(' Postal Number: ') + postal_number \
                                + _(' Transmission Date: ') + transmission_date,
                        'debit': vendor_balance * -1,
                        'partner_id': rec.vendor_id.id
                    }))
                #vh
                tax_ids = []
                for line in rec.invoice_line:
                    tax_ids = []
                    for tax in line.taxes_id:
                        tax_ids.append((4, tax.id, None))
                        for child in tax.children_tax_ids:
                            if child.type_tax_use != 'none':
                                tax_ids.append((4, child.id, None))

                move_line.append((0, 0, {
                    'account_id': rec.sales_man_id.custody_account.id,
                    'name': "sales man  custody account",
                    'debit': rec.amount_total + sum_custody + rec.commision_total_amount,
                    'partner_id': rec.vendor_id.id,
                    'tax_ids': tax_ids
                }))
                if rec.vendor_id.has_taxes:
                    select_vender=rec.id
                    for line in self.get_taxes_accounts_values(select_vender).values():

                        if rec.total_taxed_amount > 0:
                            move_line.append((0, 0, {
                                'account_id': line['taxes_id'].refund_account_id.id,
                                'name': "Taxes",
                                'debit': line['purchase_taxes'],
                                'partner_id': rec.vendor_id.id,
                                'tax_line_id':line['taxes_id'].id
                            }))
                move_line.append((0, 0, {
                    'account_id': rec.sales_man_id.commission_account.id,
                    'name': _('Commission'),
                    'credit': rec.commision_total_amount,
                    'partner_id': rec.vendor_id.id
                }))

                # Generate Account Move object
                account_move_id = rec.env['account.move'].sudo().create({
                    'date': rec.invoice_date,
                    'journal_id': journal_id.id,
                    'name': '/',
                    'ref': rec.name,
                    'state': 'draft',
                    'line_ids': move_line
                })

                # post journal entry
                account_move_id.post()
                if account_move_id.state == 'posted':
                    rec.write({'state': 'posted', 'edited': False, 'account_move_id': account_move_id.id})
                    rec.vendor_id.write({'vendor_invoice_sequence': rec.vendor_id.vendor_invoice_sequence + 1})

    @api.multi
    def update_info(self):
        if len(self) > 10:
            raise UserError(_('Can\'t update more than 10 records...!'))
        for rec in self:

            transmission_number = ''
            driver_name = ''
            postal_number = ''
            transmission_date = ''

            if rec.transmission_number:
                transmission_number = str(transmission_number)
            if rec.driver_name:
                driver_name = str(driver_name)
            if rec.postal_number:
                postal_number = str(postal_number)
            if rec.transmission_date:
                transmission_date = str(transmission_date)

            # if rec.edited:
            if not rec.sales_man_id.custody_account:
                raise ValidationError(_('You should link custody account in sales man page '))
            for custody_line in rec.custody_lines:
                if (custody_line.cash_amount > 0 and not custody_line.cash_account) or (
                        custody_line.credit_amount > 0 and not custody_line.credit_account):
                    raise ValidationError(_('You should select Account in ' + custody_line.custody_name))

            for product_line in rec.invoice_line:
                if product_line.unit_price <= 0:
                    raise ValidationError(_(product_line.product_id.name + ' has no sale price!'))

            sum_custody = 0
            for custody_line in rec.custody_lines:
                sum_custody += custody_line.credit_amount + custody_line.cash_amount

            vendor_balance = rec.amount_total  # - sum_custody - rec.commision_total_amount - rec.total_taxed_amount


            # ************************************* Edit post Entry ****************************************************
            # Generate Account Move Line
            move_line = []

            for custody_line in rec.custody_lines:

                if custody_line.credit_account and custody_line.credit_amount > 0:
                    move_line.append((0, 0, {
                        'company_id': custody_line.credit_account.company_id.id,
                        'account_id': custody_line.credit_account.id,
                        'name': custody_line.custody_name,
                        'credit': custody_line.credit_amount,
                        'partner_id': rec.vendor_id.id
                    }))

                if custody_line.cash_account and custody_line.cash_amount > 0:
                    if rec.sales_man_id.linked_customer.id:
                        move_line.append((0, 0, {
                            'company_id': custody_line.cash_account.company_id.id,
                            'account_id': rec.sales_man_id.linked_customer.property_account_receivable_id.id,
                            'name': custody_line.custody_name,
                            'credit': custody_line.cash_amount,
                            'partner_id': rec.sales_man_id.linked_customer.id

                        }))
                    else:
                        move_line.append((0, 0, {
                            'company_id': custody_line.cash_account.company_id.id,
                            'account_id': custody_line.cash_account.id,
                            'name': custody_line.custody_name,
                            'credit': custody_line.cash_amount,
                            'partner_id': rec.vendor_id.id
                        }))


            if rec.vendor_id.has_taxes:
                select_vender=rec.id
                for line in self.get_taxes_accounts_values(select_vender).values():
                    if line['taxes_id'].account_id and float(line['amount']) > 0 and line[
                        'taxes_id'].account_id and float(line['purchase_taxes']) > 0:
                        vendor_balance += line['purchase_taxes']

            if vendor_balance > 0:
                move_line.append((0, 0, {
                    'company_id': rec.vendor_id.property_account_payable_id.company_id.id,
                    'account_id': rec.vendor_id.property_account_payable_id.id,
                    'name':  _('Transmission Number: ') + transmission_number\
                            + _(' Driver: ') + driver_name \
                            + _(' Postal Number: ') + postal_number \
                            + _(' Transmission Date: ') + transmission_date,
                    'credit': vendor_balance,
                    'partner_id': rec.vendor_id.id
                }))
            elif vendor_balance < 0:
                move_line.append((0, 0, {
                    'company_id': rec.vendor_id.property_account_payable_id.company_id.id,
                    'account_id': rec.vendor_id.property_account_payable_id.id,
                    'name': _('Transmission Number: ') + transmission_number\
                            + _(' Driver: ') + driver_name \
                            + _(' Postal Number: ') + postal_number \
                            + _(' Transmission Date: ') + transmission_date,
                    'debit': vendor_balance * -1,
                    'partner_id': rec.vendor_id.id
                }))
                raise Warning(_('Invoice value is less than expenses'))

            #vh
            tax_ids = []
            for line in rec.invoice_line:
                tax_ids = []
                for tax in line.taxes_id:
                    tax_ids.append((4, tax.id, None))
                    for child in tax.children_tax_ids:
                        if child.type_tax_use != 'none':
                            tax_ids.append((4, child.id, None))

                            
            move_line.append((0, 0, {
                'company_id': rec.sales_man_id.custody_account.company_id.id,
                'account_id': rec.sales_man_id.custody_account.id,
                'name': "sales man  custody account",
                'debit': rec.amount_total + sum_custody + rec.commision_total_amount,
                'partner_id': rec.vendor_id.id,
                'tax_ids': tax_ids
            }))

            if rec.vendor_id.has_taxes:
                select_vender=rec.id
                for line in self.get_taxes_accounts_values(select_vender).values():
                    if line['taxes_id']:
                        move_line.append((0, 0, {
                            'company_id': rec.sales_man_id.custody_account.company_id.id,
                            'account_id': line['taxes_id'].refund_account_id.id,
                            'name': "Taxes",
                            'debit': line['purchase_taxes'],
                            'partner_id': rec.vendor_id.id,
                            'tax_line_id':line['taxes_id'].id
                        }))

            move_line.append((0, 0, {
                'company_id': rec.sales_man_id.commission_account.company_id.id,
                'account_id': rec.sales_man_id.commission_account.id,
                'name': "commision",
                'credit': rec.commision_total_amount,
                'partner_id': rec.vendor_id.id
            }))

            rec.account_move_id.line_ids.unlink()
            rec.account_move_id.update({'line_ids': move_line})

            # update customers invoices
            for line in rec.invoice_line:
                if line.product_id.taxes_id and rec.vendor_id.has_taxes:
                    line.invoice_line_tax_ids = line.product_id.taxes_id[0]
                else:
                    line.invoice_line_tax_ids = [(5,)]
                    #v8
                customer_invoice_line = rec.env['account.invoice.line'].search(
                    [('vendor_invoice_line_id', '=', line.id)])
                if customer_invoice_line:#v81
                    if customer_invoice_line.invoice_id.partner_id.id == line.customer_id.id:#v82
                        # reset taxes
                        if line.product_id.taxes_id and line.customer_id.has_taxes:
                            taxes = line.product_id.taxes_id[0]
                            tax_amt = taxes.amount

                        else:
                            taxes = [(5,)]
                            tax_amt = 0

                        if(line.product_id.id <> customer_invoice_line.product_id.id  or
                                   line.uom_id.id <> customer_invoice_line.uom_id.id or
                               line.product_qty <> customer_invoice_line.quantity or
                               line.unit_price <> customer_invoice_line.price_unit or
                               line.vendor_invoice_id.driver_name <> customer_invoice_line.vendor_invoice_id.driver_name or
                               line.vendor_invoice_id.transmission_number <> customer_invoice_line.vendor_invoice_id.transmission_number or
                               line.vendor_invoice_id.postal_number <> customer_invoice_line.vendor_invoice_id.postal_number or
                               line.vendor_invoice_id.transmission_date <> customer_invoice_line.vendor_invoice_id.transmission_date or
                               line.vendor_invoice_id.sales_man_id.id <> customer_invoice_line.vendor_invoice_id.sales_man_id.id or
                               line.vendor_invoice_id.sales_man_id.custody_account.id <> customer_invoice_line.account_id.id or
                               line.vendor_invoice_id.department_id.id <> customer_invoice_line.vendor_invoice_id.department_id.id
                           ):

                                customer_invoice_line.update({
                                    'product_id': line.product_id.id,
                                    'uom_id': line.uom_id.id,
                                    'quantity': line.product_qty,
                                    'price_unit': line.unit_price,
                                    'invoice_line_tax_ids': taxes,
                                    'tax_amount': ((line.product_qty * line.unit_price) * tax_amt) / 100,
                                    'driver_name': line.vendor_invoice_id.driver_name,
                                    'postal_number': line.vendor_invoice_id.postal_number,
                                    'transmission_number': line.vendor_invoice_id.transmission_number,
                                    'transmission_date': line.vendor_invoice_id.transmission_date,
                                    'sales_man_id': line.vendor_invoice_id.sales_man_id.id,
                                    'account_id': line.vendor_invoice_id.sales_man_id.custody_account.id,
                                    'department_id': line.vendor_invoice_id.department_id.id
                                })

                                customer_invoice_line.invoice_id.update({'edited': True})


                    else:#v83
                        # if not same customer we should delete this line and create new record with new customer
                        # customer_invoice_line.invoice_id.update_info()

                        # remove taxes from account invoice line
                        customer_invoice_line.invoice_line_tax_ids = [(5,)]

                        # save account invoice object to make update after delete the line
                        customer_invoice = customer_invoice_line.invoice_id
                        customer_invoice_line.unlink()
                        customer_invoice[0].write({'state': 'open'})
                        
                        #line.unlink()
                        #customer_invoice.unlink()
                        #customer_invoice_line = self.env['account.invoice.line'].search([('vendor_invoice_line_id', '=', line.id)])
                        # deleted_amount=line.unit_price*line.product_qty
                        # ref_value=customer_invoice.sequence_number
                         #update account.move
                        # account_move = self.env['account.move'].search([('ref', '=', ref_value)])
                        #account_move.update({'credit_amount':account_move.credit_amount-deleted_amount,
                        #                    'partner_id':account_move.partner_id.id,
                        #                    'amount':account_move.amount-deleted_amount})

                        #update account move line
                        # move_lineee=[]

                        # account_move_line = self.env['account.move.line'].search([('ref', '=', ref_value)])
                        # for line_move in account_move_line:
                        #     move_line_dic=[]
                        #     if line_move.debit:
                        #
                        #         move_lineee.append((0, 0,{
                        #
                        #
                        #
                        #                                 'company_id': line_move.company_id.id,
                        #                                 'account_id':line_move.account_id.id,
                        #                                 'partner_id':line_move.partner_id.id,
                        #                                 'debit':line_move.debit-deleted_amount,
                        #                               'debit_cash_basis':line_move.debit_cash_basis-deleted_amount,
                        #                               'balance_cash_basis':line_move.balance_cash_basis-deleted_amount,
                        #                               'balance':line_move.balance-deleted_amount,
                        #                                     }))
                        #
                        #     if line_move.credit:
                        #         move_lineee.append((0, 0,{
                        #
                        #zz
                        #
                        #                             'company_id': line_move.company_id.id,
                        #                             'account_id':line_move.account_id.id,
                        #                             'partner_id':line_move.partner_id.id,
                        #                             'credit':line_move.credit-deleted_amount,
                        #                               'credit_cash_basis':line_move.credit_cash_basis-deleted_amount,
                        #                               'balance_cash_basis':line_move.balance_cash_basis+deleted_amount,
                        #                               'balance':line_move.balance+deleted_amount,
                        #                                     }))
                        #customer_invoice.move_id.line_ids.unlink()
                        #customer_invoice.move_id.update({'line_ids': move_lineee})
                        #if account_move.credit_amount-deleted_amount==0:
                        #    account_move.unlink()
                        # after remove line from this customer add't to other customer
                        rec._create_customer_invoice(line)
                else:
                    rec._create_customer_invoice(line)
                    #if line.executed_line:
                    #    rec._create_customer_invoice(line)

            rec.write({'edited': False})
            rec.version_number=rec.version_number+1

    @api.multi
    def _create_customer_invoice(self, invoice_data):
        customer_invoice_lines = []
        customer_invoice = self.env['account.invoice'].search(
            [('partner_id', '=', invoice_data.customer_id.id),
             ('date_invoice', '=', invoice_data.vendor_invoice_id.invoice_date)])
        if customer_invoice:
            customer_invoice[0].invoice_line_ids.create({
                'invoice_id': customer_invoice[0].id,
                'vendor_invoice_id': invoice_data.vendor_invoice_id.id,
                'vendor_invoice_line_id': invoice_data.id,
                'product_id': invoice_data.product_id.id,
                'uom_id': invoice_data.uom_id.id,
                'currency_id': invoice_data.currency_id.id,
                'quantity': invoice_data.product_qty - invoice_data.executed_quantity,
                'executed_quantity': invoice_data.product_qty - invoice_data.executed_quantity,
                'price_unit': invoice_data.unit_price,
                'invoice_line_tax_ids': [
                    (4, invoice_data.product_id.taxes_id.id)] if invoice_data.product_id.taxes_id.id and invoice_data.customer_id.has_taxes else False,
                'executed_line': invoice_data.executed_line,
                'driver_name': invoice_data.vendor_invoice_id.driver_name,
                'postal_number': invoice_data.vendor_invoice_id.postal_number,
                'transmission_number': invoice_data.vendor_invoice_id.transmission_number,
                'transmission_date': invoice_data.vendor_invoice_id.transmission_date,
                'sales_man_id': invoice_data.vendor_invoice_id.sales_man_id.id,
                'account_id': invoice_data.vendor_invoice_id.sales_man_id.custody_account.id,
                'department_id': invoice_data.vendor_invoice_id.department_id.id
            })
            customer_invoice[0]._onchange_invoice_line_ids()
            #customer_invoice[0].update_info()
            customer_invoice[0].write({'edited': True})
            customer_invoice[0].write({'state': 'open'})
            customer_invoice[0].update_info()
        else:
            customer_invoice_lines.append((0, 0, {
                'vendor_invoice_id': invoice_data.vendor_invoice_id.id,
                'vendor_invoice_line_id': invoice_data.id,
                'product_id': invoice_data.product_id.id,
                'uom_id': invoice_data.uom_id.id,
                'currency_id': invoice_data.currency_id.id,
                'quantity': invoice_data.product_qty - invoice_data.executed_quantity,
                'executed_quantity': invoice_data.product_qty - invoice_data.executed_quantity,
                'price_unit': invoice_data.unit_price,
                'invoice_line_tax_ids': [
                    (4, invoice_data.product_id.taxes_id.id)] if invoice_data.product_id.taxes_id.id and invoice_data.customer_id.has_taxes else False,
                'executed_line': invoice_data.executed_line,
                'driver_name': invoice_data.vendor_invoice_id.driver_name,
                'postal_number': invoice_data.vendor_invoice_id.postal_number,
                'transmission_number': invoice_data.vendor_invoice_id.transmission_number,
                'transmission_date': invoice_data.vendor_invoice_id.transmission_date,
                'sales_man_id': invoice_data.vendor_invoice_id.sales_man_id.id,
                'account_id': invoice_data.vendor_invoice_id.sales_man_id.custody_account.id,
                'department_id': invoice_data.vendor_invoice_id.department_id.id
            }))
            customer_invoice_id = self.env['account.invoice'].sudo().create({
                'partner_id': invoice_data.customer_id.id,
                'account_id': invoice_data.customer_id.property_account_receivable_id.id,
                'date_invoice': invoice_data.vendor_invoice_id.invoice_date,
                'date_due': invoice_data.vendor_invoice_id.invoice_date,
                'invoice_line_ids': customer_invoice_lines
            })
            customer_invoice_id._onchange_invoice_line_ids()
            customer_invoice_id.action_invoice_open()

    @api.multi
    def get_taxes_accounts_values(self,select_vender):
        taxes_grouped = {}

        getObj= self.env['vendors.invoice']
        getAllIds = getObj.search([('id','=',select_vender)])
        for line in getAllIds.invoice_line:
            if line.taxes_id.id not in taxes_grouped:
                taxes_grouped[line.taxes_id.id] = {'taxes_id': line.taxes_id,
                                                   'amount': line.taxes_amount, 'purchase_taxes': line.purchase_taxes}
            else:
                taxes_grouped[line.taxes_id.id]['amount'] += line.taxes_amount
                taxes_grouped[line.taxes_id.id]['purchase_taxes'] += line.purchase_taxes

        return taxes_grouped

        # def _print_report(self):
        #     return self.env['report'].get_action(self, 'vegetables.print_vendor_invoice')


# Invoice Line Class


class VegetablesVendorsInvoiceLine(models.Model):
    _name = 'vendors.invoice.line'

    @api.depends('unit_price', 'product_qty')
    def _get_total_rice(self):
        for rec in self:
            if rec.product_id:
                if rec.taxes_id:
                    rec.taxes_amount = (rec.taxes_id.amount * (rec.unit_price * rec.product_qty)) / 100
                rec.total_price = rec.taxes_amount + (rec.unit_price * rec.product_qty)

    @api.depends('unit_price', 'product_qty', 'vendor_invoice_id.custody_lines', 'vendor_invoice_id.invoice_line',
                 'vendor_invoice_id.commision_total_amount')
    def _get_purchase_price(self):
        for rec in self:
            custody_total = 0
            total_qty = 0
            purchase_taxes_total = 0
            for line in rec.vendor_invoice_id.invoice_line:
                total_qty += line.product_qty


            # update product purchase price
            for line in rec.vendor_invoice_id.invoice_line:
                commission = ((line.product_qty * line.unit_price) * rec.vendor_invoice_id.commision) / 100
                line.purchase_price = line.unit_price - (commission / line.product_qty) - (rec.vendor_invoice_id.custody_total / total_qty)


                if rec.vendor_invoice_id.vendor_id.has_taxes:
                    line.purchase_taxes = (line.taxes_id.amount * (line.purchase_price * line.product_qty)) / 100
                    purchase_taxes_total += line.purchase_taxes

            rec.vendor_invoice_id.update({'purchase_taxes_total': purchase_taxes_total})

    @api.depends('taxes_id')
    def _get_taxes_amout(self):
        for rec in self:
            if rec.taxes_id:
                rec.taxes_amount = (rec.taxes_id.amount * (rec.unit_price * rec.product_qty)) / 100
            rec.total_price = rec.taxes_amount + (rec.unit_price * rec.product_qty)

    @api.depends('product_id')
    def _get_taxes(self):
        for rec in self:
            if rec.product_id.supplier_taxes_id:
                rec.taxes_id = rec.product_id.supplier_taxes_id[0].id

    _description = "Vegetables Vendors Invoice Line"

    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Invoice Reference', required=True,
                                        ondelete='cascade', index=True, copy=False)
    customer_id = fields.Many2one('res.partner', string='Customer Name', required=True,
                                  domain=[('customer', '=', True)])
    customer_code = fields.Char('C-Code')
    product_id = fields.Many2one('product.product', string='Product', required=True)
    product_code = fields.Char('P-Code')
    # uom_categ_id = fields.Many2one('product.uom.categ', string="Unit Category", related='product_id.uom_categ_id')
    uom_id = fields.Many2one('product.uom', string='Unit of Measure',
                             ondelete='set null', index=True, oldname='uos_id')
    product_qty = fields.Integer(string='Quantity', required=True, default=1.0)
    unit_price = fields.Monetary('Unit Price')
    taxes_id = fields.Many2one('account.tax', string='Taxes', compute='_get_taxes')
    taxes_amount = fields.Monetary('Tax Amount', compute='_get_taxes_amout')
    total_price = fields.Monetary('Sub Total', compute='_get_total_rice')
    purchase_price = fields.Monetary('Purchase Price', compute='_get_purchase_price')
    purchase_taxes = fields.Monetary('Purchase Taxes', compute='_get_purchase_price')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id.id)
    executed_quantity = fields.Float("Executed Quantity", default=0, copy=False)
    executed_line = fields.Boolean("Executed", default=False, copy=False)
    edit_product_id = fields.Boolean(default=False, copy=False)
    state = fields.Selection([('unpost', 'Unpost'), ('posted', 'Posted')], string='Status',
                             related='vendor_invoice_id.state', default='unpost')

    # @api.onchange('uom_id')
    # def onchange_uom_id(self):
    #     if self.uom_id and self.product_id:
    #         self.unit_price = self.product_id.uom_id._compute_price(self.product_id.list_price, self.uom_id)


    @api.multi
    def unlink(self):
        counter=0
        for line in self:
            #deleted record is ==> customer_invoice_line
            customer_invoice_line = self.env['account.invoice.line'].search([('vendor_invoice_line_id', '=', line.id)])
            if customer_invoice_line:
                account_invoice=customer_invoice_line.invoice_id
                ref_value=account_invoice.sequence_number
                account_move = self.env['account.move'].search([('ref', '=', ref_value)])
                '''deleted_amount=line.total_price#line.unit_price*line.product_qty

                
                #update account.invoice
                
                #update account.move
                account_move = self.env['account.move'].search([('ref', '=', ref_value)])
                account_move.update({'credit_amount':account_move.credit_amount-deleted_amount,
                                    'partner_id':account_invoice.partner_id.id,
                                    'amount':account_move.amount-deleted_amount})
                #update account move line
                move_lineee = depit_list = credit_list = tax_list =[]

                account_move_line = self.env['account.move.line'].search([('ref', '=', ref_value)])
                ch=0
                for move_line in account_move_line:
                    ch=ch+1
                if ch == 2:#means 2 line in account move line
                    for move_line in account_move_line:
                        if move_line.debit:
                            move_lineee.append((0, 0,{



                                                    'company_id': move_line.company_id.id,
                                                    'account_id':move_line.account_id.id,
                                                    'partner_id':move_line.partner_id.id,
                                                    'debit':move_line.debit-deleted_amount,
                                                    'debit_cash_basis':move_line.debit_cash_basis-deleted_amount,
                                                    'balance_cash_basis':move_line.balance_cash_basis-deleted_amount,
                                                'balance':move_line.balance-deleted_amount,
                                                        }))
                            
                        if move_line.credit :
                            move_lineee.append((0, 0,{



                                                'company_id': move_line.company_id.id,
                                                'account_id':move_line.account_id.id,
                                                'partner_id':move_line.partner_id.id,
                                                'credit':move_line.credit-deleted_amount,
                                                'credit_cash_basis':move_line.credit_cash_basis-deleted_amount,
                                                'balance_cash_basis':move_line.balance_cash_basis+deleted_amount,
                                                'balance':move_line.balance+deleted_amount,
                                                        }))
                else: # there is 3 line in account move line
                    ch=0
                    for move_line in account_move_line:
                        ch=ch+1
                        #print'hhh...................................',move_line
                        if move_line.tax_line_id:#ch == 1:
                            move_lineee.append((0, 0,{



                                                    'company_id': move_line.company_id.id,
                                                    'account_id':move_line.account_id.id,
                                                    'partner_id':move_line.partner_id.id,
                                                    'credit':move_line.credit-line.taxes_amount,
                                                    'balance':move_line.balance+line.taxes_amount,
                                                    'tax_line_id':move_line.tax_line_id.id
                                                        }))
                            
                        move_line_dic=[]
                        if move_line.debit:
                            move_lineee.append((0, 0,{
                                                    'company_id': move_line.company_id.id,
                                                    'account_id':move_line.account_id.id,
                                                    'partner_id':move_line.partner_id.id,
                                                    'debit':move_line.debit-deleted_amount,
                                                    'amount_residual':move_line.amount_residual-deleted_amount,
                                                    'balance':move_line.balance-deleted_amount,
                                                        }))
                            
                        if move_line.credit and not move_line.tax_line_id:
                            move_lineee.append((0, 0,{
                                                'company_id': move_line.company_id.id,
                                                'account_id':move_line.account_id.id,
                                                'partner_id':move_line.partner_id.id,
                                                'credit':move_line.credit-line.unit_price*line.product_qty,
                                                'balance':move_line.balance+line.unit_price*line.product_qty,
                                                        }))
                #print'm11........................................'
                account_invoice.move_id.line_ids.unlink() 
                #print'm222...........................................'
                '''
                account_invoice[0].write({'edited': True})                             
                num_of_lines_in_account_inv=self.env['account.invoice.line'].search([('invoice_id', '=', customer_invoice_line.invoice_id.id)])
                
                for num_of_lines in num_of_lines_in_account_inv:
                    counter=counter+1
                #if counter > 1:
                #    #print'm33....................................',move_lineee
                #    account_invoice.move_id.update({'line_ids': move_lineee})
                #    #print'm444..........................................'

                if counter <= 1:#delete acccount invoice
                    account_invoice.write({'state': 'draft'})
                    account_invoice.write({'move_name': ''})
                    account_invoice.unlink()            

                    account_move.unlink()
                

        return super(VegetablesVendorsInvoiceLine, self).unlink()

    @api.onchange('product_code')
    def onchange_product_code(self):
        if self.product_code:
            product = self.env['product.product'].sudo().search([('default_code', '=', self.product_code)])
            if product:
                self.product_id = product.id

    @api.onchange('product_id')
    def _onchange_product_id(self):
        domain = {}

        self.customer_id = self.vendor_invoice_id.sales_man_id.linked_customer
        if self.product_id:
            self.product_code = self.product_id.default_code
            if self.product_id.supplier_taxes_id:
                self.taxes_id = self.product_id.supplier_taxes_id[0].id
                self.taxes_amount = (self.taxes_id.amount * (self.unit_price * self.product_qty)) / 100
            else:
                self.taxes_amount = 0
            self.uom_id = self.product_id.uom_id.id
            domain['uom_id'] = [('category_id', '=', self.product_id.uom_categ_id.id)]

        return {'domain': domain}

    @api.onchange('customer_code')
    def onchange_customer_code(self):
        if self.customer_code:
            self.customer_id = []
            customer = self.env['res.partner'].sudo().search(
                [('code', '=', self.customer_code), ('customer', '=', True)])
            if customer:
                self.customer_id = customer.id

    @api.onchange('customer_id')
    def onchange_customer_id(self):
        if self.customer_id:
            self.customer_code = self.customer_id.code


# Custody Line Class


class VegetablesCustodyLine(models.Model):
    _name = 'vendors.custody.line'
    _description = "Vegetables Custody Line"

    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Invoice Reference', required=True,
                                        ondelete='cascade', index=True, copy=False)
    custody_name = fields.Char("Custody Name", required=True)
    custody_type = fields.Selection([('is_cash', 'IS Cash'), ('is_credit', 'IS Credit')],
                                    string='Custody Type', default='is_cash')
    sales_man = fields.Selection([('salesman', 'Sales Man'), ('supervisor', 'Supervisor')],
                                 string='Sales Man Type', default='salesman')
    account = fields.Many2one('account.account', string='Account')
    cash_account = fields.Many2one('account.account', string='Cash Account')
    credit_account = fields.Many2one('account.account', string='Credit Account')
    cash_amount = fields.Integer('Cash Amount')
    credit_amount = fields.Integer('Credit Amount')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id.id)
    is_rental = fields.Boolean('IS Rental')
    is_complex = fields.Boolean('Is Complex')

    @api.onchange('cash_amount')
    def _onchange_cash_amount(self):
        if self.cash_amount > 0:
            self.credit_amount = 0
            self.credit_account = []
            if self.sales_man == 'salesman':
                self.cash_account = self.vendor_invoice_id.sales_man_id.cash_account.id
            else:
                self.cash_account = self.vendor_invoice_id.supervisor_id.cash_account.id

    @api.onchange('credit_amount')
    def _onchange_credit_amount(self):
        if self.credit_amount > 0:
            self.cash_amount = 0
            self.cash_account = []
            if self.is_rental:
                self.credit_account = self.vendor_invoice_id.vendor_id.expense_account.id
            else:
                self.credit_account = self.account.id

