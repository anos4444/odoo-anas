import time
import datetime
from dateutil.relativedelta import relativedelta

import odoo
from odoo import SUPERUSER_ID
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class company_inhert(models.Model):
    _inherit = 'res.company'

    sale_customer_rel = fields.Boolean(string='Allow the sales man to link with the customer')

class vegetabelsConfigSettings(models.TransientModel):
    _name = 'veg.config.settings'
    _inherit = 'res.config.settings'

    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id)
    sale_customer_rel = fields.Boolean(string='Allow the sales man to link with the customer',default=lambda self: self.env.user.company_id.sale_customer_rel)
    allow_custom_commision = fields.Boolean(string="Allow Custom Commission",default=lambda self: self.env.user.company_id.allow_custom_commision)

    # @api.onchange('sale_customer_rel')
    # def change_allow_rel(self):
    #     print "change companyyyyyyyyyyy",self.sale_customer_rel
    #     self.company_id.write({'sale_customer_rel':self.sale_customer_rel,'allow_custom_commision':self.allow_custom_commision})
    #     return {}

    @api.model
    def create(self, vals):
        if 'company_id' in vals and 'sale_customer_rel' in vals:
            self.env.user.company_id.write({'sale_customer_rel':vals['sale_customer_rel'],'allow_custom_commision':vals['allow_custom_commision']})
            #print "in comp",self.env.user.company_id.sale_customer_rel
            #print "sallllllll",vals['sale_customer_rel']
        else:
            self.env.user.company_id.write({'sale_customer_rel':self.sale_customer_rel,'allow_custom_commision':self.allow_custom_commision})
            #print "out comp"
        res = super(vegetabelsConfigSettings, self).create(vals)
        res.company_id.write({'sale_customer_rel':vals['sale_customer_rel'],'allow_custom_commision':vals['allow_custom_commision']})
        #print "sl",res.company_id.sale_customer_rel
        #print 'all',res.company_id.allow_custom_commision
        return res