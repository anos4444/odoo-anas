# -*- coding: utf-8 -*-

from odoo import api, fields, models
from dateutil.parser import parse
from datetime import datetime


class daily_inv_group_date_reportWizard(models.TransientModel):
    _name = "daily_inv_group_date_report.wizard"
    _description = "daily_inv_group_date_report wizard"

    date_from = fields.Date(string='Start Date',default=fields.Date.context_today)
    date_to = fields.Date(string='End Date',default=fields.Date.context_today)

    partner_id = fields.Many2one('res.partner', string='Partner',domain=[('is_market', '=', True)])

    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['date_from', 'date_to', 'partner_id'])[0]
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['date_from', 'date_to', 'partner_id'])[0])
        return self.env['report'].get_action(self, 'vegetables.report_daily_inv_group_date_report', data=data)

def get_date(str):
    return datetime.strptime(str, "%Y-%m-%d").date()
