# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import api, fields, models


class update_vendor_invoice_sequance(models.TransientModel):
    _name = "update.invoices.vendor.sequance.wizard"


    vendor_id = fields.Many2one('res.partner', string='Vendor Name', domain=[('supplier', '=', True)])

    @api.multi
    def set(self):

        if self.vendor_id:
            vendor_invoice = self.env['vendors.invoice'].search([('vendor_id', '=', self.vendor_id.id)], order='invoice_date desc')
            count=1
            for invoice in vendor_invoice:
                invoice.update({'vendor_sequence':count})
                count=count+1

