# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import api, fields, models

class print_customers_invoices_report(models.TransientModel):

    _name = "customers.invoices.rep.wizard"
    
    date_from = fields.Date(string='Start Date',default=fields.Date.context_today)
    date_to = fields.Date(string='End Date',default=fields.Date.context_today)
     

    customers = fields.Boolean('Customers')
    salesperson_id = fields.Boolean('Salesperson')
    suppliers = fields.Boolean('Suppliers')
    product = fields.Boolean('Product')

    partners_ids = fields.Many2many('res.partner', 'customers_inv_rep', string='Partners',domain=[('is_market', '=', True)])
    
    product_ids = fields.Many2many('product.product', 'product_inv_rep', string='Product')

    salesperson_ids = fields.Many2many('hr.employee', 'for_cust_inv_rep', string='Salepersons',domain=[('is_market_employee', '=', True)])

    def get_data(self, data):

        domain=[]

        if self.product_ids and self.product: # customer filter
            domain = [('product_id', 'in', [x.id for x in self.product_ids])]
        if self.partners_ids and self.customers: # customer filter
            domain = [('partner_id', 'in', [x.id for x in self.partners_ids])]
        if self.salesperson_ids and self.salesperson_id: # saleperson filter
            domain = [('sales_man_id', 'in', [x.id for x in self.salesperson_ids])]+domain
        if self.partners_ids and self.suppliers: # suppliers filter
            domain = [('vendor_invoice_id.vendor_id','in',[x.id for x in self.partners_ids])]+domain
        if self.date_from:
            domain = [('invoice_id.date', '>=', self.date_from )]+domain
        if self.date_to:
            domain = [('invoice_id.date', '<=', self.date_to )]+domain

        customers_invoices = self.env['account.invoice.line'].search(domain)
        customers_invoices_ids = [x.id for x in customers_invoices]

        view = self.env.ref('vegetables.customer_invoice_report_tree')
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.invoice.line',
            'view_mode': 'tree',
            'view_type': 'form',
            'domain': [('id','in',customers_invoices_ids)],
            'view_id': view.id,
            'target': 'current',
        }
