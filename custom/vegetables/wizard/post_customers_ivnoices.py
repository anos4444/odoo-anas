# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import datetime


class PostCustomersInvoices(models.TransientModel):
    _name = "all.customers.invoices.wizard"

    transmission_number = fields.Char('Invoice Number')
    sales_man_id = fields.Many2one('hr.employee', string='Sales Man', required=False,domain=[('is_sales_man', '=', True)])
    invoice_date = fields.Date(string='Invoice Date', required=True, default=fields.Date.context_today)


    def _get_partner_balance(self, partner_id):
        amount = 0
	amount = 0.0
	account = False
	account = partner_id and partner_id.property_account_receivable_id.id or False
	where = ' m.account_id = %s' % str(account) + ' and m.partner_id = %s' % str(partner_id.id)
	if account :
		self._cr.execute(''' select round(sum(m.debit),2) as debit, 
		                round(sum(m.credit),2) as credit, 
		                round(sum(m.debit) - sum(m.credit) ,2) as balance 
		            from account_move_line m , account_move v
		        where m.move_id = v.id and v.state = 'posted' and ''' + where )
	res =  self._cr.dictfetchall()
	print res
	if res :
	    amount = res[0]['balance']
	return amount or 0.0



    @api.multi
    def post_all_customers(self):
	#print " AAAAAAAA  TEST   TEST    TEST"
	p_obj = self.env['res.partner']
	il_obj = self.env['vendors.invoice.line']
	ci_obj = self.env['account.invoice']
	v_lines= []
	action_lst_ids = []
        #new_name222222 = self.journal_id #self.env['ir.sequence'].next_by_code('customer.invoice')
        #print '*--*-*-*-*-*-*---*-*-*-*---* ', new_name222222
        if self.invoice_date:
	    vendors_invoices_list= []
            #all_customers_ids = p_obj.search([('customer', '=', True)])
            domain_v = [('executed_line', '=', False),('state', '=', 'posted'),('customer_id.customer', '=', True),('vendor_invoice_id.invoice_date', '=', self.invoice_date)]
            if self.sales_man_id :
		domain_v.append(('vendor_invoice_id.sales_man_id', '=', self.sales_man_id.id))
            if self.transmission_number :
		domain_v.append(('vendor_invoice_id.transmission_number', '=', self.transmission_number))
                #print "domain ",domain_v
            vendors_lines1 = il_obj.search(domain_v)
	    c_lst = []
	    for v in vendors_lines1:
		if v.customer_id.id not in c_lst:
			c_lst.append(v.customer_id.id)

            #for customer in all_customers_ids:
            for customer in p_obj.browse(c_lst):

                #domain_v = [('executed_line', '=', False),('state', '=', 'posted'),('customer_id', '=', customer.id),('vendor_invoice_id.invoice_date', '=', self.invoice_date)]
                #if self.sales_man_id :
                    #domain_v.append(('vendor_invoice_id.sales_man_id', '=', self.sales_man_id.id))
                #if self.transmission_number :
                    #domain_v.append(('vendor_invoice_id.transmission_number', '=', self.transmission_number))
                #print "domain ",domain_v
		
		if not customer.active_credit_limit:
		        domain_v = [('id', 'in', vendors_lines1 and vendors_lines1.ids or []),('customer_id', '=', customer.id)]
		        vendors_lines = il_obj.search(domain_v)

		        customer_invoice = ci_obj.search(
		            [('partner_id', '=', customer.id),
		             ('date_invoice', '=', self.invoice_date)])
		        if customer_invoice:
		            for line in vendors_lines:
				if line.vendor_invoice_id and (line.vendor_invoice_id.id not in vendors_invoices_list):
					vendors_invoices_list.append(line.vendor_invoice_id.id)
		                if customer_invoice:
		                    #print '*************************',customer_invoice
		                    #print '*************************',line
		                    customer_invoice[0].invoice_line_ids.create({
		                        'invoice_id': customer_invoice[0].id,
		                        'vendor_invoice_id': line.vendor_invoice_id.id,
		                        'product_id': line.product_id.id,
		                        'uom_id': line.uom_id.id,
		                        'currency_id': line.currency_id.id,
		                        'quantity': line.product_qty - line.executed_quantity,
		                        'executed_quantity': line.product_qty - line.executed_quantity,
		                        'price_unit': line.unit_price,
		                        'invoice_line_tax_ids': [
		                                    (4, line.product_id.taxes_id.id)] if line.product_id.taxes_id.id and line.customer_id.has_taxes else False,
		                        'total_after_discount': line.total_price,
		                        'executed_line': line.executed_line,
		                        'driver_name': line.vendor_invoice_id.driver_name,
		                        'postal_number': line.vendor_invoice_id.postal_number,
		                        'transmission_number': line.vendor_invoice_id.transmission_number,
		                        'transmission_date': line.vendor_invoice_id.transmission_date,
		                        'sales_man_id': line.vendor_invoice_id.sales_man_id.id,
		                        'account_id': line.vendor_invoice_id.sales_man_id.custody_account.id,
		                        'vendor_invoice_line_id': line.id,
		                        'department_id': line.vendor_invoice_id.department_id.id,
		                    })
		                    line.executed_line = True
				    v_lines.append(line.id)
				    print "  YYYYYYYYYSSSSSSSSSS      11111111111111 "
		                    customer_invoice[0]._onchange_invoice_line_ids()
		                    # customer_invoice[0].update_info()
		                    customer_invoice[0].write({'edited': True,'vegetables': True,})
		        
		        else :
		            invoice_lines = []
		            for line in vendors_lines:
				if line.vendor_invoice_id and (line.vendor_invoice_id.id not in vendors_invoices_list):
					vendors_invoices_list.append(line.vendor_invoice_id.id)
		                # if line.vendor_invoice_id.invoice_date == self.invoice_date:
		                invoice_lines.append((0, 0,
		                                      {
		                                          'vendor_invoice_id': line.vendor_invoice_id.id,
		                                          'product_id': line.product_id.id,
		                                          'uom_id': line.uom_id.id,
		                                          'currency_id': line.currency_id.id,
		                                          'quantity': line.product_qty - line.executed_quantity,
		                                          'executed_quantity': line.product_qty - line.executed_quantity,
		                                          'price_unit': line.unit_price,
		                                          'invoice_line_tax_ids': [
		                                              (4,
		                                               line.product_id.taxes_id.id)] if line.product_id.taxes_id.id and line.customer_id.has_taxes else False,
		                                          'total_after_discount': line.total_price,
		                                          'executed_line': line.executed_line,
		                                          'driver_name': line.vendor_invoice_id.driver_name,
		                                          'postal_number': line.vendor_invoice_id.postal_number,
		                                          'transmission_number': line.vendor_invoice_id.transmission_number,
		                                          'transmission_date': line.vendor_invoice_id.transmission_date,
		                                          'sales_man_id': line.vendor_invoice_id.sales_man_id.id,
		                                          'account_id': line.vendor_invoice_id.sales_man_id.custody_account.id,
		                                          'vendor_invoice_line_id': line.id,
		                                          'department_id': line.vendor_invoice_id.department_id.id

		                                      }))
				print "  YYYYYYYYYSSSSSSSSSS      222222 "
		                line.executed_line = True
				v_lines.append(line.id)
		            '''
		            now_date = datetime.datetime.now().strftime("%Y-%m-%d")
		            self._cr.execute("insert into account_invoice "\
		                             "( date_due, create_date, journal_id, amount_total_company_signed, residual, "\
		                             "  partner_id, create_uid, amount_untaxed, residual_company_signed, amount_total_signed, "\
		                             "  company_id, state, type, sent, account_id, "\
		                             "  reconciled, reference_type, date_invoice, write_date, residual_signed, date, "\
		                             "  user_id, write_uid, amount_total, amount_untaxed_signed, currency_id, "\
		                             "  sequence_number, team_id, partner_code, legacy_amount, version_number, "\
		                             "  email_status, vegetables, invoice_type, send_by_email) "\
		                             "  value ('%s', '%s', %s, %s, 0.0, "\
		                             "  %s, '%s', %s, 0.0, %s, "\
		                             "  %s, 'paid', 'out_invoice', 'f', %s, "\
		                             "  't', 'none', '%s', '%s', 0.0, '%s', "\
		                             "  %s, %s, %s, %s, %s, "\
		                             "  '%s', %s, %s, 'f', 1, "\
		                             "  'not_sent', 't', 'vegetables', 'f') ;"\
		                                %(self.invoice_date, now_date, rec.name, self.company_id.id, journal_id.id,
		                                self.currency_id.id, rec.untaxes_total + rec.purchase_taxes_total, 0.0, datetime.datetime.now(), rec.invoice_date,
		                                datetime.datetime.now(), self.user_id.id, 'f', 'default', rec.untaxes_total + rec.purchase_taxes_total,
		                                rec.id))
		            for ll in invoice_lines:
		                print '***********************************************', ll
		            '''
		            if invoice_lines:
		                customer_invoice = ci_obj.sudo().create({
		                    "invoice_type": "vegetables",
		                    "partner_id": customer.id,
		                    "partner_code": customer.code,
		                    "account_id": customer.property_account_receivable_id.id,
		                    "date_invoice": self.invoice_date,
		                    "date_due": self.invoice_date,
		                    "invoice_line_ids": invoice_lines,
		                    'vegetables': True,

		                })
		                print "*/////////////////////////////*************************"
		                record = ci_obj.browse(customer_invoice.id)

		                if record.state != 'draft':
		                    raise UserError(_(
		                        "Selected invoice(s) cannot be confirmed as they are not in 'Draft' state."))
		                record._onchange_invoice_line_ids()
		                record.action_invoice_open()

		else:


		                    # customer_invoice.action_invoice_open()
			customer_balance= self._get_partner_balance(customer)
			customer_credit_limit = customer.credit_limit or 0.00
		        domain_v = [('id', 'in', vendors_lines1 and vendors_lines1.ids or []),('customer_id', '=', customer.id)]
		        vendors_lines = il_obj.search(domain_v)

		        customer_invoice = ci_obj.search(
		            [('partner_id', '=', customer.id),
		             ('date_invoice', '=', self.invoice_date)])
		        if customer_invoice:
		            for line in vendors_lines:
				if line.vendor_invoice_id and (line.vendor_invoice_id.id not in vendors_invoices_list):
					vendors_invoices_list.append(line.vendor_invoice_id.id)
		                customer_balance += (line.total_price or 0.00)
				if customer_balance > customer_credit_limit:
					break




		                if customer_invoice:
		                    #print '*************************',customer_invoice
		                    #print '*************************',line
		                    customer_invoice[0].invoice_line_ids.create({
		                        'invoice_id': customer_invoice[0].id,
		                        'vendor_invoice_id': line.vendor_invoice_id.id,
		                        'product_id': line.product_id.id,
		                        'uom_id': line.uom_id.id,
		                        'currency_id': line.currency_id.id,
		                        'quantity': line.product_qty - line.executed_quantity,
		                        'executed_quantity': line.product_qty - line.executed_quantity,
		                        'price_unit': line.unit_price,
		                        'invoice_line_tax_ids': [
		                                    (4, line.product_id.taxes_id.id)] if line.product_id.taxes_id.id and line.customer_id.has_taxes else False,
		                        'total_after_discount': line.total_price,
		                        'executed_line': line.executed_line,
		                        'driver_name': line.vendor_invoice_id.driver_name,
		                        'postal_number': line.vendor_invoice_id.postal_number,
		                        'transmission_number': line.vendor_invoice_id.transmission_number,
		                        'transmission_date': line.vendor_invoice_id.transmission_date,
		                        'sales_man_id': line.vendor_invoice_id.sales_man_id.id,
		                        'account_id': line.vendor_invoice_id.sales_man_id.custody_account.id,
		                        'vendor_invoice_line_id': line.id,
		                        'department_id': line.vendor_invoice_id.department_id.id,
		                    })
		                    line.executed_line = True
				    v_lines.append(line.id)
		                    customer_invoice[0]._onchange_invoice_line_ids()
		                    # customer_invoice[0].update_info()
		                    customer_invoice[0].write({'edited': True,'vegetables': True,})
		        
		        else :
		            invoice_lines = []
		            for line in vendors_lines:
				if line.vendor_invoice_id and (line.vendor_invoice_id.id not in vendors_invoices_list):
					vendors_invoices_list.append(line.vendor_invoice_id.id)
					print '************************',line.total_price
		                customer_balance += (line.total_price or 0.00)
		                if customer_balance > customer_credit_limit:
					break

		                # if line.vendor_invoice_id.invoice_date == self.invoice_date:
		                invoice_lines.append((0, 0,
		                                      {
		                                          'vendor_invoice_id': line.vendor_invoice_id.id,
		                                          'product_id': line.product_id.id,
		                                          'uom_id': line.uom_id.id,
		                                          'currency_id': line.currency_id.id,
		                                          'quantity': line.product_qty - line.executed_quantity,
		                                          'executed_quantity': line.product_qty - line.executed_quantity,
		                                          'price_unit': line.unit_price,
		                                          'invoice_line_tax_ids': [
		                                              (4,
		                                               line.product_id.taxes_id.id)] if line.product_id.taxes_id.id and line.customer_id.has_taxes else False,
		                                          'total_after_discount': line.total_price,
		                                          'executed_line': line.executed_line,
		                                          'driver_name': line.vendor_invoice_id.driver_name,
		                                          'postal_number': line.vendor_invoice_id.postal_number,
		                                          'transmission_number': line.vendor_invoice_id.transmission_number,
		                                          'transmission_date': line.vendor_invoice_id.transmission_date,
		                                          'sales_man_id': line.vendor_invoice_id.sales_man_id.id,
		                                          'account_id': line.vendor_invoice_id.sales_man_id.custody_account.id,
		                                          'vendor_invoice_line_id': line.id,
		                                          'department_id': line.vendor_invoice_id.department_id.id

		                                      }))
		                line.executed_line = True
				v_lines.append(line.id)
		            '''
		            now_date = datetime.datetime.now().strftime("%Y-%m-%d")
		            self._cr.execute("insert into account_invoice "\
		                             "( date_due, create_date, journal_id, amount_total_company_signed, residual, "\
		                             "  partner_id, create_uid, amount_untaxed, residual_company_signed, amount_total_signed, "\
		                             "  company_id, state, type, sent, account_id, "\
		                             "  reconciled, reference_type, date_invoice, write_date, residual_signed, date, "\
		                             "  user_id, write_uid, amount_total, amount_untaxed_signed, currency_id, "\
		                             "  sequence_number, team_id, partner_code, legacy_amount, version_number, "\
		                             "  email_status, vegetables, invoice_type, send_by_email) "\
		                             "  value ('%s', '%s', %s, %s, 0.0, "\
		                             "  %s, '%s', %s, 0.0, %s, "\
		                             "  %s, 'paid', 'out_invoice', 'f', %s, "\
		                             "  't', 'none', '%s', '%s', 0.0, '%s', "\
		                             "  %s, %s, %s, %s, %s, "\
		                             "  '%s', %s, %s, 'f', 1, "\
		                             "  'not_sent', 't', 'vegetables', 'f') ;"\
		                                %(self.invoice_date, now_date, rec.name, self.company_id.id, journal_id.id,
		                                self.currency_id.id, rec.untaxes_total + rec.purchase_taxes_total, 0.0, datetime.datetime.now(), rec.invoice_date,
		                                datetime.datetime.now(), self.user_id.id, 'f', 'default', rec.untaxes_total + rec.purchase_taxes_total,
		                                rec.id))
		            for ll in invoice_lines:
		                print '***********************************************', ll
		            '''
		            if invoice_lines:
		                customer_invoice = ci_obj.sudo().create({
		                    "invoice_type": "vegetables",
		                    "partner_id": customer.id,
		                    "partner_code": customer.code,
		                    "account_id": customer.property_account_receivable_id.id,
		                    "date_invoice": self.invoice_date,
		                    "date_due": self.invoice_date,
		                    "invoice_line_ids": invoice_lines,
		                    'vegetables': True,

		                })
		                record = ci_obj.browse(customer_invoice.id)

		                if record.state != 'draft':
		                    raise UserError(_(
		                        "Selected invoice(s) cannot be confirmed as they are not in 'Draft' state."))
		                record._onchange_invoice_line_ids()
		                record.action_invoice_open()

		                    # customer_invoice.action_invoice_open()
		
		print '7777',vendors_invoices_list
	    cr = self.env.cr
	    for v_invoice in vendors_invoices_list:

		    sql= (" select vil.vendor_invoice_id as vid from vendors_invoice_line vil "\
				 "WHERE vil.vendor_invoice_id = %s and executed_line = 'f' and vil.id not in %s "\
				)
		    params = (v_invoice,v_lines and tuple(v_lines) or tuple([0]),)
		    cr.execute(sql, params)
		    f= cr.dictfetchall()
 		    print '***********************************',f
		    if not ( f and f[0] or False):
			    print '88888888888888888888888888888888888888888888'
			    sql= (" update vendors_invoice set executed_lines = 1\
					 WHERE id = %s")
			    params = (v_invoice,)
			    cr.execute(sql, params)
		    else:
			    print '44444444444444444444444444444444444444'
			    action_lst_ids.append(v_invoice)
			    sql= (" update vendors_invoice set executed_lines = 0\
					 WHERE id = %s")
			    params = (v_invoice,)
			    cr.execute(sql, params)

	    sql= (" select vil.id as vid from vendors_invoice vil \
	    WHERE vil.invoice_date = %s and executed_lines = 0\
		")
	    params = (self.invoice_date,)
	    cr.execute(sql, params)
	    f= cr.dictfetchall()
	    if ( f and f[0] or False):
	            action = self.env.ref('vegetables.vegetables_vendor_invoice_action').read()[0]
		    action.update({'domain':[('invoice_date','=',self.invoice_date),('executed_lines','=',0)]})
	    else:
            	action = self.env.ref('vegetables.action_vegetables_invoice_tree1').read()[0]
            return action
