# -*- coding: utf-8 -*-

from odoo import api, fields, models
from dateutil.parser import parse
from datetime import datetime


class detailed_sales_disclosure_reportWizard(models.TransientModel):
    _name = "detailed_sales_disclosure_report.wizard"
    _description = "detailed_sales_disclosure_report wizard"

    customer_id = fields.Many2one('res.partner', string='Customer Name', required=True,
                                 domain=['&',('customer', '=', True),('is_market', '=', True)])
    vendor_id = fields.Many2one('res.partner', string='Vendor Name', 
            domain=['&',('supplier', '=', True),('is_market', '=', True)])

    date_from = fields.Date(string='Start Date',default=fields.Date.context_today)
    date_to = fields.Date(string='End Date',default=fields.Date.context_today)


    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['customer_id','vendor_id','date_from', 'date_to'])[0]
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['customer_id','vendor_id','date_from', 'date_to'])[0])
        return self.env['report'].get_action(self, 'vegetables.detailed_sales_disclosure', data=data)

