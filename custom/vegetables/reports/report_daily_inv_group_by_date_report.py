# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from operator import itemgetter
from datetime import timedelta

class Reportall_payment(models.AbstractModel):
    _name = 'report.vegetables.report_daily_inv_group_date_report'

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))

        date_from_o = parse(docs.date_from).date()#original
        date_to_o = parse(docs.date_to).date()#original
        wanted_partner_id = docs.partner_id
        partners_static = self.env['res.partner'].search(['|',('vendors_invoice_line_ids', '!=', []),('customer_account_invoice_ids', '!=', [])])
        if wanted_partner_id:
            partners_static = [wanted_partner_id]
        partners = {}
        #all_vendor_total_amount = all_customer_total_amount = all_difference =all_vendor_total_tax = all_customer_total_tax = all_difference_tax =0.0

        aaaaa = 0 
        for partner in partners_static:
            #print "*****************", partner.id
            aaaaa += 1
            start = date_from_o
            end = date_to_o
            while start <= end:
                
                vendor_total_amount = vendor_total_tax = 0.0
                self._cr.execute("select "\
                                 "vendors_invoice.invoice_date,  "\
                                 "res_partner.name as name , "\
                                 "sum(round((vendors_invoice_line.unit_price * vendors_invoice_line.product_qty),4)) as ven_total , "\
                                 "sum(round(vendors_invoice_line.total_price - round((vendors_invoice_line.unit_price * vendors_invoice_line.product_qty),4) ,4)) as ven_tax "\
                                 "from "\
                                 "vendors_invoice "\
                                 "INNER JOIN vendors_invoice_line on vendors_invoice_line.vendor_invoice_id = vendors_invoice.id "
                                 "INNER JOIN res_partner on res_partner.id = vendors_invoice_line.customer_id "\
                                 "where "\
                                 "vendors_invoice_line.customer_id in (%s) "\
                                 "and vendors_invoice.invoice_date = '%s' "\
                                 "GROUP BY "\
                                 "vendors_invoice.invoice_date, "\
                                 "res_partner.name,"\
                                 "vendors_invoice_line.unit_price, "\
                                 "vendors_invoice_line.product_qty, "\
                                 "vendors_invoice_line.total_price "\
                                 "ORDER By "\
                                 "vendors_invoice.invoice_date;" %(partner.id,start))
                res = self._cr.dictfetchall()
                self._cr.execute("select "\
                             "round(sum(amount_untaxed),4) as cus_total, "\
                             "round(sum(amount_total - amount_untaxed),4) as cus_tax "\
                             "from "\
                             "account_invoice "\
                             "where "\
                             "partner_id in (%s) "\
                             "and date = '%s';" %(partner.id,start))
                res1 = self._cr.dictfetchall()

                if res and res1:
                    #print "*****************", partner.id, "      **************  ", start, " ***************** ", res1[0]['cus_total']
                    if res1[0]['cus_total'] is not None and res1[0]['cus_tax'] is not None:
                        customer_total_amount = res1[0]['cus_total']
                        customer_total_tax = res1[0]['cus_tax']
                    else:
                        #print "@@@@@@@@@@@"
                        customer_total_amount = 0.0
                        customer_total_tax = 0.0

                    for n in range(len(res)):
                        vendor_total_amount += res[n]['ven_total']
                        vendor_total_tax += res[n]['ven_tax']
                    
                    print "******************--------------------------*********************",customer_total_tax
                    partners[partner] = partners.get(partner,{'data':[],'all_vendor_total_amount':0.0,'all_customer_total_amount':0.0,'all_difference':0.0,'all_vendor_total_tax':0.0,'all_customer_total_tax':0.0,'all_difference_tax':0.0,})
                    partners[partner]['data'].append({
                            'date':start,
                            'vendor_total_amount':vendor_total_amount,
                            'customer_total_amount':customer_total_amount,
                            'difference':abs(vendor_total_amount - customer_total_amount),
                            'vendor_taxes':vendor_total_tax,
                            'customer_taxes':customer_total_tax,
                            'difference_tax':abs(vendor_total_tax - customer_total_tax)
                        })

                    partners[partner]['all_vendor_total_amount']+= vendor_total_amount
                    partners[partner]['all_customer_total_amount']+= customer_total_amount
                    partners[partner]['all_difference']+= abs(vendor_total_amount - customer_total_amount)
                    partners[partner]['all_vendor_total_tax']+= vendor_total_tax
                    partners[partner]['all_customer_total_tax']+= customer_total_tax
                    partners[partner]['all_difference_tax']+= abs(vendor_total_tax - customer_total_tax)

                start = start + timedelta(days=1)
                #print start

        #print " -**-*-**-*--**-*-*-*-*-* " , all_vendor_total_amount , partner.name
        #print " -**-*-**-*--**-*-*-*-*-* " , all_vendor_total_tax


        # for p in partners_static:
        #     start = date_from_o
        #     end = date_to_o
        #     while start <= end:
        #         sum = vendor_taxes = 0.0
        #         for inv in p.vendors_invoice_line_ids:
        #             if parse(inv.vendor_invoice_id.invoice_date).date() == start:
        #                 sum += inv.total_price
		#         vendor_taxes += inv.taxes_amount
        #         vendor_total_amount = sum - vendor_taxes

        #         sum = customer_taxes = 0.0
        #         for inv in p.customer_account_invoice_ids:
        #             if parse(inv.date_invoice).date() == start:
        #                 if inv.type == 'out_invoice':
        #                     sum += inv.amount_total
		# 	    customer_taxes += inv.amount_tax
        #                 if inv.type == 'out_refund':
        #                     sum -= inv.amount_total
		# 	    customer_taxes -= inv.amount_tax
        #         customer_total_amount = sum - customer_taxes
        #         # print "AAAAAAAAAAAAA  ******* ",customer_taxes
        #         # print "]]]]]]]]]]]]]]]]]]]]]]]]]  ******* ",customer_total_amount

        #         if vendor_total_amount != 0.0 or customer_total_amount != 0.0:
        #             vendor_total_amount = round(vendor_total_amount, 2)
        #             customer_total_amount = round(customer_total_amount, 2)
        #             vendor_taxes = round(vendor_taxes, 2)
        #             customer_taxes = round(customer_taxes, 2)
        #             partners[p] = partners.get(p,{'data':[],'all_vendor_total_amount':0.0,'all_customer_total_amount':0.0,'all_difference':0.0,'all_vendor_total_tax':0.0,'all_customer_total_tax':0.0,'all_difference_tax':0.0,})
        #             partners[p]['data'].append({
        #                 'date':start,
        #                 'vendor_total_amount':vendor_total_amount,
        #                 'customer_total_amount':customer_total_amount,
        #                 'difference':abs(vendor_total_amount - customer_total_amount),
        #                 'vendor_taxes':vendor_taxes,
        #                 'customer_taxes':customer_taxes,
        #                 'difference_tax':abs(vendor_taxes - customer_taxes)
        #             })
        #             partners[p]['all_vendor_total_amount']+= vendor_total_amount
        #             partners[p]['all_customer_total_amount']+= customer_total_amount
        #             partners[p]['all_difference']+= abs(vendor_total_amount - customer_total_amount)
        #             partners[p]['all_vendor_total_tax']+= vendor_taxes
        #             partners[p]['all_customer_total_tax']+= customer_taxes
        #             partners[p]['all_difference_tax']+= abs(vendor_taxes - customer_taxes)
        #         vendor_total_amount = customer_total_amount = 0.0
        #         start = start + timedelta(days=1)

        print aaaaa
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'all_data': partners,
            # 'all_vendor_total_amount':all_vendor_total_amount,
	        # 'all_vendor_taxes':all_vendor_taxes,
	        # 'all_customer_taxes':all_customer_taxes,
            # 'all_customer_total_amount':all_customer_total_amount,
            # 'all_difference':all_difference,
	        # 'all_taxes_difference':all_taxes_difference
        }
        return self.env['report'].render('vegetables.report_daily_inv_group_date_report', docargs)
