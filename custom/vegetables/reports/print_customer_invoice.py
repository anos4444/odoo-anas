# -*- coding: utf-8 -*-

from odoo import api, models
from operator import itemgetter


class PrintVCustomerInvoice(models.AbstractModel):
    _name = 'report.vegetables.print_customer_invoice_main_template'

    @api.model
    def render_html(self, docids, data=None):
        _count = 1
        docs = self.env['account.invoice'].browse(docids)
        print '*************** ', docs

        #docs.sort(key=lambda k: k.partner_code) 
        docs = docs.sorted(key=lambda r: int(r.partner_id.code or 0))
        print docs




        invoices = []
        for invoice in docs:
            _invoice_line = []
            _invoice_data = []
            _invoice = []
            _loop = 0
            _page_num = 1
            _loop_last_paper = 0
            __page_num_total = 1
            for line in invoice.invoice_line_ids:

                _invoice_line.append({
                    '_count': str(_count)+'-'+str(line.sales_man_id.code),
                    '_item': line.product_id.name,
                    '_quantity': line.quantity,
                    '_price': line.price_unit,
                    '_tax': line.tax_amount,
                    '_total_price': line.price_unit * line.quantity + line.tax_amount
                })
                _count += 1

            if (len(_invoice_line) == 12):
                _loop = 11
                _loop_last_paper = 14
            elif (len(_invoice_line) < 12):
                _loop = 8
                _loop_last_paper = 8
            elif (len(_invoice_line) > 12):
                    _loop = 12
                    if(len(_invoice_line)%12 ==0):
                        _loop_last_paper = 11
                    else:
                        _loop_last_paper = 8

            _page_num_total = len(_invoice_line) / _loop

            if len(_invoice_line) % _loop != 0:
                _page_num_total +=1

            count = 0
            for data in range(len(_invoice_line)):
                count += 1
                if count <= _loop:
                    _invoice.append(_invoice_line[data])
                else:
                    _invoice_data.append({
                        '_last_page': '-1',
                        '_invoice': _invoice,
                        '_page_num': _page_num
                    })
                    _page_num = _page_num + 1
                    __page_num_total += 1

                    if (__page_num_total == _page_num_total):
                        _loop = _loop_last_paper
                    _invoice = []
                    _invoice.append(_invoice_line[data])
                    count = 1

            # __page_num_total += 1
            _invoice_data.append({
                '_last_page': '1',
                '_invoice': _invoice,
                '_page_num': _page_num
            })

            docargs = {
                'doc_ids': docids,
                'doc_model': 'vendors.invoice',
                'docs': invoice,
                '_data': _invoice_data,
                '_page_num_total': __page_num_total
            }
            invoices.append(docargs)
            _count = 1

        return self.env['report'].render('vegetables.print_customer_invoice_main_template', {'all_data':invoices})
